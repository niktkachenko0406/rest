<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\Blog;

/**
 * This is the model class for table "comments".
 *
 * @property integer $comment_id
 * @property integer $user_id
 * @property integer $entity_id
 * @property string $comment_text
 * @property integer $comment_kitchen_rating
 * @property integer $comment_interier_rating
 * @property integer $comment_service_rating
 * @property integer $comment_ambience_rating
 * @property integer $comment_entity
 * @property integer $comment_status
 * @property integer $created_at
 */
class Comments extends \yii\db\ActiveRecord
{
    const ENTITY_REST = 1;
    const ENTITY_NEWS = 2;
    const ENTITY_BLOG = 3;

    const COMMENT_INACTIVE = 0;
    const COMMENT_ACTIVE   = 1;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'entity_id', 'comment_text', 'comment_entity'], 'required'],
            [['user_id', 'entity_id', 'comment_kitchen_rating', 'comment_interier_rating', 'comment_service_rating', 'comment_ambience_rating', 'comment_entity', 'comment_status'], 'integer'],
            [['comment_text'], 'string'],
            [['comment_text'], 'trim'],
            [['comment_text'], 'filter', 'filter' => 'strip_tags'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment_id'              => 'Comment ID',
            'user_id'                 => 'User ID',
            'entity_id'               => 'Entity ID',
            'comment_text'            => 'Текст коментария',
            'comment_kitchen_rating'  => 'Кухня',
            'comment_interier_rating' => 'Интерьер',
            'comment_service_rating'  => 'Обслуживание',
            'comment_ambience_rating' => 'Атмосфера',
            'comment_entity'          => 'Comment Entity',
            'created_at'              => 'Дата комментария',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors              = parent::behaviors();
        $behaviors['timestamp'] = [
            'class'      => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
            ],
            'value' => function() { return date('U');}
        ];

        return $behaviors;
    }

    public function getRatingPercent($column)
    {
        return ($this->{$column}) / 5 * 100;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Is entity have a not approved comments
     * @param int $commentEntity type of comment(constants)
     * @param int $entityId
     * @return string
     */
    public static function isApproved($commentEntity, $entityId) {
        $action = self::getAction($commentEntity);

        if($count = count(Comments::find()->where([
            'entity_id'      => $entityId,
            'comment_entity' => $commentEntity,
            'comment_status' => self::COMMENT_INACTIVE
        ])->all())) {
            return Html::a('Есть: ' . $count, ['comments/' . $action, 'id' => $entityId], ['class' => 'btn-warning']);
        } else {
            return Html::tag('p', 'Нет', ['class' => 'btn-success']);
        }
    }

    public function getStatus($status = null) {
        $data = [
            0 => 'В ожидании',
            1 => 'Одобрено'
        ];

        return $status !== null ? $data[$status] : $data;
    }

    public function getType($type = null) {
        $data = [
            self::ENTITY_REST => 'Ресторан',
            self::ENTITY_NEWS => 'Новости',
            self::ENTITY_BLOG => 'Блог'
        ];

        return $type !== null ? $data[$type] : $data;
    }

    public static function getAction($commentEntity = null) {
        $data = [
            self::ENTITY_REST => 'index',
            self::ENTITY_NEWS => 'news-comments',
            self::ENTITY_BLOG => 'blog-comments'
        ];

        return $commentEntity ? $data[$commentEntity] : $data;
    }

    public static function isApprovedBlogComment($commentEntity, $entityId) {
        $count = 0;

        if($posts = Blog::find()->where(['restaurant_id' => $entityId])->all()) {
            foreach ($posts as $post) {
                $countComments = count(Comments::find()->where([
                    'entity_id'      => $post->blog_id,
                    'comment_entity' => $commentEntity,
                    'comment_status' => self::COMMENT_INACTIVE
                ])->all());

                if($countComments) {
                    $count = $count + $countComments;
                }
            }
        }

        if($count) {
            return Html::a('Есть: ' . $count, ['blog/index', 'restId' => $entityId], ['class' => 'btn-warning']);
        } else {
            return Html::tag('p', 'Нет', ['class' => 'btn-success']);
        }
    }

    public function getRatingList() {
        return [
            0 => 0,
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5
        ];
    }

    public static function countComments($restId) {
        return self::find()->where(['entity_id' => $restId, 'comment_entity' => self::ENTITY_REST, 'comment_status' => self::COMMENT_ACTIVE])->count();
    }
}
