<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "reasons".
 *
 * @property string $reason_id
 * @property string $reason_name
 *
 * @property RestaurantReasons[] $restaurantReasons
 */
class Reasons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reasons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reason_name'], 'required'],
            [['reason_name'], 'string', 'max' => 50],
            [['reason_name'], 'trim'],
            [['reason_name'], 'filter', 'filter' => 'strip_tags'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reason_id'   => 'ID',
            'reason_name' => 'Повод',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('re-' . $event->sender->reason_name);
            },
        ];

        return $behaviors;
    }

    public static function getReasons()
    {
        return static::find()->all();
    }

    public static function getReasonNames($names) {
        $names = explode(' ', $names);
        $names = static::find()
            ->select('reason_name')
            ->where(['in', 'uri', $names])
            ->asArray()
            ->all();

        $reasonNames = [];

        foreach($names as $name) {
            $reasonNames[] = $name['reason_name'];
        }

        return implode(',', $reasonNames);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantReasons()
    {
        return $this->hasMany(RestaurantReasons::className(), ['reason_id' => 'reason_id']);
    }
}
