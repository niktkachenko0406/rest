<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "restaurant_metro".
 *
 * @property integer $id
 * @property integer $restaurant_id
 * @property integer $station_id
 *
 * @property Restaurants $restaurant
 * @property Metro $station
 */
class RestaurantMetro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_metro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'station_id'], 'required'],
            [['restaurant_id', 'station_id'], 'integer'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurants::className(), 'targetAttribute' => ['restaurant_id' => 'restaurant_id']],
            [['station_id'], 'exist', 'skipOnError' => true, 'targetClass' => Metro::className(), 'targetAttribute' => ['station_id' => 'station_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'restaurant_id' => 'Restaurant ID',
            'station_id' => 'Station ID',
        ];
    }

    public static function multiInsert($data, $restId)
    {
        static::deleteAll('restaurant_id = ' . $restId);

        Yii::$app->db->createCommand()->batchInsert(static::tableName(), ['restaurant_id', 'station_id'],
            $data
        )->execute();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurants::className(), ['restaurant_id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Metro::className(), ['station_id' => 'station_id']);
    }
}
