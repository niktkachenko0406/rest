<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "restaurant_features".
 *
 * @property integer $feature_id
 * @property integer $restaurant_id
 * @property string $feature_name
 *
 * @property Restaurants $restaurant
 */
class RestaurantFeatures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_features';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'feature_id'], 'required'],
            [['restaurant_id', 'feature_id'], 'integer'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurants::className(), 'targetAttribute' => ['restaurant_id' => 'restaurant_id']],
            [['feature_id'], 'exist', 'skipOnError' => true, 'targetClass' => Features::className(), 'targetAttribute' => ['feature_id' => 'feature_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'feature_id' => 'Feature ID',
            'restaurant_id' => 'Restaurant ID',
        ];
    }

    public function getId()
    {
        return $this->feature_id;
    }

    public static function multiInsert($data, $restId)
    {
        static::deleteAll('restaurant_id = ' . $restId);

        Yii::$app->db->createCommand()->batchInsert(static::tableName(), ['restaurant_id', 'feature_id'],
            $data
        )->execute();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurants::className(), ['restaurant_id' => 'restaurant_id']);
    }
}
