<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "favorites".
 *
 * @property string $favorites_id
 * @property integer $user_id
 * @property string $restaurant_id
 *
 * @property Restaurants $restaurant
 * @property User $user
 */
class Favorites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'favorites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'restaurant_id'], 'required'],
            [['user_id', 'restaurant_id'], 'integer'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurants::className(), 'targetAttribute' => ['restaurant_id' => 'restaurant_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'favorites_id' => 'Favorites ID',
            'user_id' => 'User ID',
            'restaurant_id' => 'Restaurant ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurants::className(), ['restaurant_id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Get ids favorites restaurants of user
     */
    public static function getFavoritesRestaurants($userId) {
        $restaurantsIds = [];

        if($userId) {
            $result = self::find()->select('restaurant_id')->where(['user_id' => $userId])->asArray()->all();

            foreach ($result as $value) {
                $restaurantsIds[] = (int)$value['restaurant_id'];
            }
        }

        return $restaurantsIds;
    }
}
