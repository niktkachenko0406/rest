<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "providers_products".
 *
 * @property string $product_id
 * @property string $provider_id
 * @property string $product_title
 * @property string $product_image
 * @property string $uri
 *
 * @property Providers $provider
 */
class ProvidersProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uri', 'provider_id', 'product_title'], 'required'],
            [['provider_id'], 'integer'],
            [['product_title'], 'string', 'max' => 255],
            [['product_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Providers::className(), 'targetAttribute' => ['provider_id' => 'provider_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id'    => 'Product ID',
            'provider_id'   => 'Провайдер',
            'product_title' => 'Продукт',
            'product_image' => 'Изображение',
            'uri'           => 'uri',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('prp-' . $event->sender->product_title);
            },
        ];

        return $behaviors;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Providers::className(), ['provider_id' => 'provider_id']);
    }

    public function getImg($module = null)
    {
        if($this->product_image) {
            if($module == 'admin') {
                return Yii::$app->params['uploadUrl'] . '/provider/product/' . $this->product_image;
            }

            return '/upload/provider/product/' . $this->product_image;
        } else {
            return '/img/dish1.jpg';
        }
    }

    public static function findProducts($providerId)
    {
        return static::find()->where(['provider_id' => $providerId])->all();
    }
}
