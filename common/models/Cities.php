<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * This is the model class for table "cities".
 *
 * @property integer $city_id
 * @property string $city_name
 * @property string $of_city
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_name'], 'trim'],
            [['city_name'], 'filter', 'filter' => 'strip_tags'],
            [['city_name'], 'string', 'max' => 80],
            [['city_name'], 'required', 'message' => 'Поле не может быть пустым'],
            [['city_name'], 'unique', 'targetClass' => '\common\models\Cities', 'message' => 'Данный Город уже добавлен'],
            [['of_city'], 'trim'],
            [['of_city'], 'filter', 'filter' => 'strip_tags'],
            [['of_city'], 'string', 'max' => 80],
            [['of_city'], 'required', 'message' => 'Поле не может быть пустым'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('c-' . $event->sender->city_name);
            },
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id'   => 'ID',
            'city_name' => 'Город',
            'of_city'   => 'Город(а)'
        ];
    }

    public static function getCityName($cityId = null, $uri = null)
    {
        $city = static::find()->select('city_name');

        if($cityId) {
            $city->where(['city_id' => $cityId]);
        }

        if($uri) {
            $city->where(['uri' => $uri]);
        }

        $result = $city->one();

        return $result->city_name;
    }

    public static function getCities($byUri = false) {
        $cities = static::find()
                ->select(['city_id', 'city_name', 'uri'])
                ->asArray()
                ->all();

        if(!$byUri) {
            return ArrayHelper::map($cities, 'city_id', 'city_name');
        } else {
            return ArrayHelper::map($cities, 'uri', 'city_name');
        }
    }

    public static function getCityId($uri) {
        $city = static::findOne(['uri' => $uri]);

        return $city ? $city->city_id : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(Districts::className(), ['city_id' => 'city_id']);
    }
}
