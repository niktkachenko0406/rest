<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "types".
 *
 * @property integer $type_id
 * @property string $type_name
 *
 * @property RestaurantTypes[] $restaurantTypes
 */
class Types extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_name'], 'required'],
            [['type_name'], 'string', 'max' => 50],
            [['type_name'], 'trim'],
            [['type_name'], 'filter', 'filter' => 'strip_tags'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'type_name' => 'Type Name',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('t-' . $event->sender->type_name);
            },
        ];

        return $behaviors;
    }

    public static function getTypes()
    {
        return static::find()->all();
    }

    public static function getTypeNames($names) {
        $names = explode(' ', $names);
        $names = static::find()
               ->select('type_name')
               ->where(['in', 'uri', $names])
               ->asArray()
               ->all();

        $typeNames = [];

        foreach($names as $name) {
            $typeNames[] = $name['type_name'];
        }

        return implode(',', $typeNames);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantTypes()
    {
        return $this->hasMany(RestaurantTypes::className(), ['type_id' => 'type_id']);
    }
}
