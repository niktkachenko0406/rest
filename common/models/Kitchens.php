<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "kitchens".
 *
 * @property integer $kitchen_id
 * @property string $kitchen_name
 *
 * @property RestaurantKitchen[] $restaurantKitchens
 */
class Kitchens extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kitchens';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kitchen_name'], 'required'],
            [['kitchen_name'], 'string', 'max' => 50],
            [['kitchen_name'], 'trim'],
            [['kitchen_name'], 'filter', 'filter' => 'strip_tags'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kitchen_id' => 'Kitchen ID',
            'kitchen_name' => 'Kitchen Name',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('k-' . $event->sender->kitchen_name);
            },
        ];

        return $behaviors;
    }

    public static function getKitchens()
    {
        return static::find()->all();
    }

    public static function getKitchenNames($names) {
        $names = explode(' ', $names);
        $names = static::find()
            ->select('kitchen_name')
            ->where(['in', 'uri', $names])
            ->asArray()
            ->all();

        $typeNames = [];

        foreach($names as $name) {
            $typeNames[] = $name['kitchen_name'];
        }

        return implode(',', $typeNames);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantKitchens()
    {
        return $this->hasMany(RestaurantKitchen::className(), ['kitchen_id' => 'kitchen_id']);
    }
}
