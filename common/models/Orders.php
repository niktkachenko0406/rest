<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $order_id
 * @property integer $order_person
 * @property integer $user_id
 * @property string $restaurant_id
 * @property string $order_phone
 * @property string $order_date
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_person', 'user_id', 'restaurant_id', 'order_phone', 'order_date'], 'required'],
            [['order_person', 'user_id', 'restaurant_id'], 'integer'],
            [['order_phone', 'order_date'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'      => 'ID',
            'order_person'  => 'Количество',
            'user_id'       => 'Пользователь',
            'restaurant_id' => 'Ресторан',
            'order_phone'   => 'Телефон',
            'order_date'    => 'Дата',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getRestaurant()
    {
        return $this->hasOne(Restaurants::className(), ['restaurant_id' => 'restaurant_id']);
    }

    public static function getOrderRestaurants($userId)
    {
        return static::find()->where(['user_id' => $userId])->with('restaurant')->all();
    }

    public static function countOrders() {
        return static::find()->select('order_id')->count();
    }
}
