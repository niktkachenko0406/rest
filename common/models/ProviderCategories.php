<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "provider_categories".
 *
 * @property string $category_id
 * @property string $category_name
 * @property string $uri
 *
 * @property ProviderSubcategories[] $providerSubcategories
 */
class ProviderCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'provider_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uri', 'category_name'], 'required'],
            [['category_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id'   => 'ID',
            'category_name' => 'Название',
            'uri'           => 'uri',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('prc-' . $event->sender->category_name);
            },
        ];

        return $behaviors;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderSubcategories()
    {
        return $this->hasMany(ProviderSubcategories::className(), ['category_id' => 'category_id']);
    }

    public function getServices() {
        $categories = $this->find()->joinWith('providerSubcategories')->asArray()->all();
        $services   = [];

        foreach ($categories as $category) {
            foreach ($category['providerSubcategories'] as $subcategory) {
                $services[$category['category_name']][$subcategory['subcategory_id']] = $subcategory['subcategory_name'];
            }
        }

        return $services;
    }

    public static function getCategories() {
        return static::find()->with('providerSubcategories')->all();
    }
}
