<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "provider_subcategories".
 *
 * @property string $subcategory_id
 * @property string $subcategory_name
 * @property string $category_id
 * @property string $uri
 *
 * @property ProviderCategories $category
 * @property ProvidersServices[] $providersServices
 */
class ProviderSubcategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'provider_subcategories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uri', 'subcategory_name', 'category_id'], 'required'],
            [['category_id'], 'integer'],
            [['subcategory_name'], 'string', 'max' => 100],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderCategories::className(), 'targetAttribute' => ['category_id' => 'category_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subcategory_id'   => 'ID',
            'subcategory_name' => 'Название',
            'category_id'      => 'Категория',
            'uri'              => 'uri',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('prs-' . $event->sender->subcategory_name);
            },
        ];

        return $behaviors;
    }

    public static function getSubCategoriesByUri($params) {
        $uris = explode(' ', $params['filter']);

        return static::find()->where(['in', 'uri', $uris])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProviderCategories::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvidersServices()
    {
        return $this->hasMany(ProvidersServices::className(), ['subcategory_id' => 'subcategory_id']);
    }
}
