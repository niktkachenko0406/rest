<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * This is the model class for table "districts".
 *
 * @property integer $district_id
 * @property string $district_name
 * @property integer $city_id
 *
 * @property Cities $city
 */
class Districts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'districts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['district_name', 'city_id'], 'required'],
            [['city_id'], 'integer'],
            [['district_name'], 'string', 'max' => 100],
            [['district_name'], 'trim'],
            [['district_name'], 'filter', 'filter' => 'strip_tags'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'city_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'district_id'   => 'ID',
            'district_name' => 'Район',
            'city_id'       => 'Город',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('d-' . $event->sender->district_name);
            },
        ];

        return $behaviors;
    }

    public static function getDistricts($cityId = null) {
        $query = static::find()
               ->select(['district_id', 'district_name', 'uri']);

        if($cityId) {
            $query->where(['city_id' => $cityId]);
        }

        return $query->all();
    }

    public static function getDistrictName($districtId = null, $uri = null)
    {
        $district = static::find()
                  ->select('district_name');

        if($districtId) {
            $district->where(['district_id' => $districtId]);
        }

        if($uri) {
            $district->where(['uri' => $uri]);
        }

        $district = $district->one();

        return $district->district_name;
    }

    public static function getDistrictId($uri) {
        $district = static::findOne(['uri' => $uri]);

        return $district ? $district->district_id : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['city_id' => 'city_id']);
    }
}
