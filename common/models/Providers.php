<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "providers".
 *
 * @property string $provider_id
 * @property string $provider_name
 * @property string $provider_short_desc
 * @property string $provider_desc
 * @property string $provider_email
 * @property string $provider_phone
 * @property string $provider_site
 * @property string $city_id
 * @property string $district_id
 * @property string $provider_address
 * @property string $provider_avatar
 * @property string $uri
 *
 * @property ProvidersProducts[] $providersProducts
 * @property ProvidersServices[] $providersServices
 */
class Providers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_desc'], 'string'],
            [['city_id', 'district_id'], 'integer'],
            [['uri', 'provider_name', 'provider_email', 'provider_short_desc', 'provider_site', 'provider_phone'], 'required'],
            [['provider_name', 'provider_email', 'provider_site'], 'string', 'max' => 100],
            [['provider_short_desc'], 'string', 'max' => 1024],
            [['provider_phone'], 'string', 'max' => 20],
            [['provider_address', 'uri'], 'string', 'max' => 255],
            [['provider_avatar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'provider_id'         => 'ID',
            'provider_name'       => 'Название',
            'provider_short_desc' => 'Краткое описание',
            'provider_desc'       => 'Описание',
            'provider_email'      => 'Почта',
            'provider_phone'      => 'Телефон',
            'provider_site'       => 'Сайт',
            'city_id'             => 'Город',
            'district_id'         => 'Район',
            'provider_address'    => 'Адресс',
            'provider_avatar'     => 'Аватар',
            'uri'                 => 'uri',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('pr-' . $event->sender->provider_name);
            },
        ];

        return $behaviors;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvidersProducts()
    {
        return $this->hasMany(ProvidersProducts::className(), ['provider_id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvidersServices()
    {
        return $this->hasMany(ProvidersServices::className(), ['provider_id' => 'provider_id']);
    }

    public function getSubCategories() {
        return $this->hasMany(ProviderSubcategories::className(), ['subcategory_id' => 'subcategory_id'])
                    ->viaTable('providers_services', ['provider_id' => 'provider_id']);
    }

    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(Districts::className(), ['district_id' => 'district_id']);
    }

    public function getImg($module = null)
    {
        if($this->provider_avatar) {
            if($module == 'admin') {
                return Yii::$app->params['uploadUrl'] . '/provider/avatars/' . $this->provider_avatar;
            }

            return '/upload/provider/avatars/' . $this->provider_avatar;
        } else {
            return '/img/dish1.jpg';
        }
    }

    public static function getModels($params)
    {
        $sql = static::find()->joinWith('subCategories')->distinct();

        if($params['filter']) {
            $filters = explode(' ', $params['filter']);

            $sql->andWhere(['in', 'provider_subcategories.uri', $filters]);
        }

        return $sql;
    }

    /**
     * Return provider
     * @param $uri
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findModelByUri($uri)
    {
        return static::find()->where(['uri' => $uri])->with('providersProducts', 'city')->one();
    }

    public static function countProviders() {
        return static::find()->select('provider_id')->count();
    }
}
