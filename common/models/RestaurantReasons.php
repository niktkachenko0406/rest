<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "restaurant_reasons".
 *
 * @property string $id
 * @property string $restaurant_id
 * @property string $reason_id
 *
 * @property Reasons $reason
 * @property Restaurants $restaurant
 */
class RestaurantReasons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_reasons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'reason_id'], 'required'],
            [['restaurant_id', 'reason_id'], 'integer'],
            [['reason_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reasons::className(), 'targetAttribute' => ['reason_id' => 'reason_id']],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurants::className(), 'targetAttribute' => ['restaurant_id' => 'restaurant_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'restaurant_id' => 'Ресторан ID',
            'reason_id'     => 'Повод ID',
        ];
    }

    public static function multiInsert($data, $restId)
    {
        static::deleteAll('restaurant_id = ' . $restId);

        Yii::$app->db->createCommand()->batchInsert(static::tableName(), ['restaurant_id', 'reason_id'],
            $data
        )->execute();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReason()
    {
        return $this->hasOne(Reasons::className(), ['reason_id' => 'reason_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurants::className(), ['restaurant_id' => 'restaurant_id']);
    }
}
