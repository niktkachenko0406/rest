<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "restaurant_kitchen".
 *
 * @property integer $kitchen_id
 * @property integer $restaurant_id
 * @property string $kitchen_name
 *
 * @property Restaurants $restaurant
 */
class RestaurantKitchen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_kitchen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'kitchen_id'], 'required'],
            [['restaurant_id', 'kitchen_id'], 'integer'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurants::className(), 'targetAttribute' => ['restaurant_id' => 'restaurant_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kitchen_id' => 'ID',
            'restaurant_id' => 'Ресторан',
            'kitchen_name' => 'Название',
        ];
    }

    public function getId()
    {
        return $this->kitchen_id;
    }

    public static function multiInsert($data, $restId)
    {
        static::deleteAll('restaurant_id = ' . $restId);

        Yii::$app->db->createCommand()->batchInsert(static::tableName(), ['restaurant_id', 'kitchen_id'],
            $data
        )->execute();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurants::className(), ['restaurant_id' => 'restaurant_id']);
    }
}
