<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "providers_services".
 *
 * @property string $id
 * @property string $subcategory_id
 * @property string $provider_id
 *
 * @property Providers $provider
 * @property ProviderSubcategories $subcategory
 */
class ProvidersServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subcategory_id', 'provider_id'], 'required'],
            [['subcategory_id', 'provider_id'], 'integer'],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Providers::className(), 'targetAttribute' => ['provider_id' => 'provider_id']],
            [['subcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderSubcategories::className(), 'targetAttribute' => ['subcategory_id' => 'subcategory_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subcategory_id' => 'Subcategory ID',
            'provider_id' => 'Provider ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Providers::className(), ['provider_id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(ProviderSubcategories::className(), ['subcategory_id' => 'subcategory_id']);
    }

    public static function multiInsert($data, $provaiderId)
    {
        static::deleteAll('provider_id = ' . $provaiderId);

        Yii::$app->db->createCommand()->batchInsert(static::tableName(), ['subcategory_id', 'provider_id'],
            $data
        )->execute();
    }

    public function getProviderServices($providerId) {
        return self::find()->where(['provider_id' => $providerId])->with('subcategory.category')->asArray()->all();
    }
}
