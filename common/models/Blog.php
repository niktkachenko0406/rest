<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\helpers\Html;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Inflector;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "blog".
 *
 * @property string $blog_id
 * @property string $restaurant_id
 * @property string $blog_title
 * @property string $blog_text
 * @property string $blog_image
 * @property integer $blog_status
 * @property string $created_at
 *
 * @property Restaurants $restaurant
 */
class Blog extends \yii\db\ActiveRecord
{
    const POST_STATUS_INACTIVE = 0;
    const POST_STATUS_ACTIVE   = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'blog_title', 'blog_text'], 'required'],
            [['blog_title', 'blog_text'], 'trim'],
            [['restaurant_id', 'blog_status'], 'integer'],
            [['blog_status'], 'default', 'value' => self::POST_STATUS_INACTIVE],
            [['blog_text'],  'string'],
            [['blog_title'], 'string', 'max' => 255],
            [['blog_title'], 'filter', 'filter' => 'strip_tags'],
            [['blog_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurants::className(), 'targetAttribute' => ['restaurant_id' => 'restaurant_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'blog_id'       => 'Blog ID',
            'restaurant_id' => 'Автор',
            'blog_title'    => 'Названия статьи',
            'blog_text'     => 'Текстовый редактор',
            'blog_image'    => 'Изображение',
            'blog_status'   => 'Статус',
            'created_at'    => 'Создано',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('b-' . $event->sender->blog_title);
            },
        ];
        $behaviors['timestamp'] = [
            'class'      => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
            ],
            'value' => function() { return date('U');}
        ];

        return $behaviors;
    }

    /**
     * Return post
     * @param $id
     * @return array|ActiveRecord[]
     */
    public static function findModel($id)
    {
        return static::find()->where([
            'blog_id' => $id,
        ])->one();
    }

    public static function getModels()
    {
        return static::find()->where(['blog_status' => self::POST_STATUS_ACTIVE]);
    }

    /**
     * @return string
     */
    public function getAvatar() {
        if($this->blog_image) {
            $path = '/upload/blog/' . $this->blog_image;
        } else {
            $path = '/img/dish1.jpg';
        }

        return $path;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurants::className(), ['restaurant_id' => 'restaurant_id']);
    }

    public function getStatus($status = null) {
        $data = [
            0 => 'В ожидании',
            1 => 'Одобрено'
        ];

        return $status !== null ? $data[$status] : $data;
    }

    /**
     * Is restaurant have a not approved posts
     * @param int $restId
     * @return string
     */
    public static function isApproved($restId) {
        if($count = count(Blog::find()->where([
            'restaurant_id' => $restId,
            'blog_status'   => self::POST_STATUS_INACTIVE
        ])->all())) {
            return Html::a('Есть: ' . $count, ['blog/index', 'restId' => $restId], ['class' => 'btn-warning']);
        } else {
            return Html::tag('p', 'Нет', ['class' => 'btn-success']);
        }
    }

    public function getImg($module = null)
    {
        if($this->blog_image) {
            if($module == 'admin') {
                return Yii::$app->params['uploadUrl'] . '/blog/' . $this->blog_image;
            }

            return '/upload/blog/' . $this->blog_image;
        } else {
            return '/img/dish1.jpg';
        }
    }

    /**
     * Return posts
     * @param $uri
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findModelByUri($uri)
    {
        return static::find()->where(['uri' => $uri])->with('comments.user.userProfile')->one();
    }

    public static function getLastPosts($limit = 3)
    {
        return static::find()->limit($limit)
                             ->orderBy('created_at DESC')
                             ->where(['blog_status' => self::POST_STATUS_ACTIVE])
                             ->all();
    }

    public static function getClasses() {
        return [
            '',
            'grid-item--width2',
            'grid-item--width3'
        ];
    }

    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['entity_id' => 'blog_id'])
            ->where([
                'comment_entity' => Comments::ENTITY_BLOG,
                'comment_status' => Comments::COMMENT_ACTIVE
            ]);
    }

    public static function getPostsForSearch($search) {
        return static::find()->filterWhere(['like', 'blog_title', $search])->all();
    }
}
