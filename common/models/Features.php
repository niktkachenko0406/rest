<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "features".
 *
 * @property integer $feature_id
 * @property string $feature_name
 *
 * @property RestaurantFeatures[] $restaurantFeatures
 */
class Features extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'features';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feature_name'], 'required'],
            [['feature_name'], 'string', 'max' => 50],
            [['feature_name'], 'trim'],
            [['feature_name'], 'filter', 'filter' => 'strip_tags'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'feature_id' => 'Feature ID',
            'feature_name' => 'Feature Name',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('f-' . $event->sender->feature_name);
            },
        ];

        return $behaviors;
    }

    public static function getFeatures()
    {
        return static::find()->all();
    }

    public static function getFeatureNames($names) {
        $names = explode(' ', $names);
        $names = static::find()
            ->select('feature_name')
            ->where(['in', 'uri', $names])
            ->asArray()
            ->all();

        $featureNames = [];

        foreach($names as $name) {
            $featureNames[] = $name['feature_name'];
        }

        return implode(',', $featureNames);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantFeatures()
    {
        return $this->hasMany(RestaurantFeatures::className(), ['feature_id' => 'feature_id']);
    }
}
