<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\Inflector;
use yii\helpers\Json;
use common\models\RestaurantKitchen;

/**
 * This is the model class for table "restaurants".
 *
 * @property integer $restaurant_id
 * @property integer $user_id
 * @property string $restaurant_name
 * @property string $restaurant_short_desc
 * @property string $restaurant_desc
 * @property integer $restaurant_average_check
 * @property string $restaurant_schedule
 * @property string $restaurant_address
 * @property string $restaurant_avatar
 * @property string $restaurant_phone
 * @property integer $restaurant_status
 * @property integer $city_id
 * @property integer $district_id
 * @property string $restaurant_days
 * @property string $uri
 * @property string $restaurant_rating
 *
 * @property User $user
 */
class Restaurants extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const DEFAULT_URI     = 'restaurant-name';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_name', 'restaurant_short_desc', 'restaurant_desc', 'city_id', 'restaurant_phone'], 'required'],
            [['user_id', 'city_id', 'district_id', 'restaurant_average_check', 'restaurant_status'], 'integer'],
            [['restaurant_desc', 'restaurant_short_desc', 'restaurant_phone'], 'string'],
            [['restaurant_desc', 'restaurant_short_desc', 'restaurant_phone', 'restaurant_name'], 'trim'],
            [['restaurant_name'],       'filter', 'filter' => 'strip_tags'],
            [['restaurant_short_desc'], 'filter', 'filter' => 'strip_tags'],
            [['restaurant_phone'],      'filter', 'filter' => 'strip_tags'],
            [['restaurant_name'],       'string', 'max' => 50],
            [['restaurant_phone'],      'string', 'max' => 20],
            [['restaurant_short_desc'], 'string', 'max' => 1024],
            [['restaurant_address'],    'string', 'max' => 255],
            [['restaurant_schedule', 'restaurant_days'], 'safe'],
            [['restaurant_avatar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_CREATE] = ['user_id'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'restaurant_id'            => 'ID',
            'user_id'                  => 'User ID',
            'restaurant_name'          => 'Название',
            'restaurant_short_desc'    => 'Краткое описание',
            'restaurant_desc'          => 'Подробное описание',
            'restaurant_average_check' => 'Средний чек (гривен)',
            'restaurant_schedule'      => 'Режим работы',
            'restaurant_address'       => 'Адрес',
            'restaurant_avatar'        => 'Фото',
            'restaurant_rating'        => 'Рейтинг',
            'city_id'                  => 'Город',
            'district_id'              => 'Район',
            'restaurant_status'        => 'Статус',
            'restaurant_phone'         => 'Телефон'
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->restaurant_name) && !empty($event->sender->uri) && $event->sender->uri != self::DEFAULT_URI) {
                    return $event->sender->uri;
                }

                if(empty($event->sender->uri) && empty($event->sender->restaurant_name)) {
                    return Inflector::slug('r-' . self::DEFAULT_URI);
                } else {
                    return Inflector::slug('r-' . $event->sender->restaurant_name);
                }
            },
        ];

        return $behaviors;
    }

    /**
     * @param null $uri
     * @param null $userId
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findModel($userId = null, $uri = null)
    {
        $query = static::find();

        if($uri) {
            $query->where(['uri' => $uri]);
        } elseif ($userId) {
            $query->where(['user_id' => $userId]);
        }

        $query->with(
            'types', 'kitchen', 'features',
            'gallery', 'city', 'district',
            'metro', 'comments.user.userProfile',
            'reasons', 'blog'
        );

        return $query ->one();
    }

    public static function getRestaurant($restId) {
        return static::findOne($restId);
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->restaurant_schedule = Json::decode($this->restaurant_schedule);
        $this->restaurant_days     = Json::decode($this->restaurant_days);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getModels($params, $select = [])
    {
        $sql = static::find();

        if($select) {
            $sql->select($select);
        }

        $sql->where(['restaurants.restaurant_status' => self::STATUS_ACTIVE])
            ->joinWith('user')
            ->joinWith('types')
            ->joinWith('kitchen')
            ->joinWith('features')
            ->joinWith('reasons')
            ->joinWith('metro')
            ->joinWith('city')
            ->distinct();

        if(isset($params['search'])) {
            $sql->andWhere(['like', 'restaurants.restaurant_name', $params['search']]);
            $sql->orWhere(['like', 'kitchens.kitchen_name', $params['search']]);
            $sql->orWhere(['like', 'features.feature_name', $params['search']]);
            $sql->orWhere(['like', 'types.type_name', $params['search']]);
        }

        if($params['city']) {
            $sql->andWhere(['restaurants.city_id' => Cities::getCityId($params['city'])]);
        }

        if($params['district']) {
            $sql->andWhere(['restaurants.district_id' => Districts::getDistrictId($params['district'])]);
        }

        if($params['metro']) {
            $stations = explode(' ', $params['metro']);

            $sql->andWhere(['in', 'metro.uri', $stations]);
        }

        if($params['type']) {
            $types = explode(' ', $params['type']);

            $sql->andWhere(['in', 'types.uri', $types]);
        }

        if($params['kitchen']) {
            $kitchens = explode(' ', $params['kitchen']);

            $sql->andWhere(['in', 'kitchens.uri', $kitchens]);
        }

        if($params['feature']) {
            $features = explode(' ', $params['feature']);

            $sql->andWhere(['in', 'features.uri', $features]);
        }

        if($params['reason']) {
            $reasons = explode(' ', $params['reason']);

            $sql->andWhere(['in', 'reasons.uri', $reasons]);
        }

        return $sql;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function createRest($userId)
    {
        $this->user_id = $userId;

        return $this->save() ? true : false;
    }

    /**
     * @param $type
     * @return RestaurantTypes|null
     */
    public static function getModelByType($type)
    {
        $model = null;

        switch ($type) {
            case 'type':
                $model = new RestaurantTypes();
            break;
            case 'kitchen':
                $model = new RestaurantKitchen();
            break;
            case 'feature':
                $model = new RestaurantFeatures();
            break;
        }

        return $model;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->restaurant_schedule = Json::encode($this->restaurant_schedule);
        $this->restaurant_days     = Json::encode($this->restaurant_days);

        return true;
    }

    /**
     * Return path avatar
     * @return string
     */
    public function getAvatar($module = null) {
        if($this->restaurant_avatar) {
            if($module == 'admin') {
                return Yii::$app->params['uploadUrl'] . '/restaurant/avatars/' . $this->restaurant_avatar;
            }

            $path = '/upload/restaurant/avatars/' . $this->restaurant_avatar;
        } else {
            $path = '/img/dish1.jpg';
        }

        return $path;
    }

    public function getRating()
    {
        $rating = 0;

        if($this->comments) {
            foreach ($this->comments as $comment) {
                $rating += (
                        $comment['comment_kitchen_rating'] +
                        $comment['comment_interier_rating'] +
                        $comment['comment_service_rating'] +
                        $comment['comment_ambience_rating']
                    ) / 4;
            }

            if (count($this->comments) > 0) {
                $rating = round($rating / count($this->comments), 1, PHP_ROUND_HALF_UP);

                return $rating == '5' ? '5.0' : $rating;
            }
        }

        return $rating;
    }

    public function getRatingPercent()
    {
        return ($this->restaurant_rating) / 5 * 100;
    }

    /**
     * Return restaurants on main page
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getMainRestaurants() {
        return static::find()->where(['restaurant_status' => self::STATUS_ACTIVE])
                             ->limit(6)
                             ->joinWith('user')
                             ->with('comments')
                             ->orderBy('user.created_at DESC')
                             ->all();
    }

    /**
     * @return array
     */
    public function getSelectedMetro()
    {
        return ArrayHelper::map($this->getMetro()->asArray()->all(), 'station_id', 'station_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypes()
    {
        return $this->hasMany(Types::className(), ['type_id' => 'type_id'])
                    ->viaTable('restaurant_types', ['restaurant_id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKitchen()
    {
        return $this->hasMany(Kitchens::className(), ['kitchen_id' => 'kitchen_id'])
                    ->viaTable('restaurant_kitchen', ['restaurant_id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatures()
    {
        return $this->hasMany(Features::className(), ['feature_id' => 'feature_id'])
                    ->viaTable('restaurant_features', ['restaurant_id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetro()
    {
        return $this->hasMany(Metro::className(), ['station_id' => 'station_id'])
            ->viaTable('restaurant_metro', ['restaurant_id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReasons()
    {
        return $this->hasMany(Reasons::className(), ['reason_id' => 'reason_id'])
            ->viaTable('restaurant_reasons', ['restaurant_id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasMany(RestaurantGallery::className(), ['restaurant_id' => 'restaurant_id']);
    }

    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(Districts::className(), ['district_id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['entity_id' => 'restaurant_id'])
                    ->where([
                        'comment_entity' => Comments::ENTITY_REST,
                        'comment_status' => Comments::COMMENT_ACTIVE
                    ]);
    }

    public function getBlog()
    {
        return $this->hasMany(Blog::className(), ['restaurant_id' => 'restaurant_id']);
    }

    /**
     * @param null $status
     * @return array|mixed
     */
    public static function getStatus($status = null) {
        $data = [
            0 => 'В ожидании',
            1 => 'Одобрено'
        ];

        return $status !== null ? $data[$status] : $data;
    }

    public static function getDay($day) {
//        $week = [
//            'monday'    => 'Понедельник',
//            'tuesday'   => 'Вторник',
//            'wednesday' => 'Среда',
//            'thursday'  => 'Четверг',
//            'friday'    => 'Пятница',
//            'saturday'  => 'Суббота',
//            'sunday'    => 'Воскресенье',
//        ];

        $week = [
            'monday'    => 'Пн',
            'tuesday'   => 'Вт',
            'wednesday' => 'Ср',
            'thursday'  => 'Чт',
            'friday'    => 'Пт',
            'saturday'  => 'Сб',
            'sunday'    => 'Вс',
        ];

        return $week[$day];
    }

    /**
     * Return favorite restaurants of user
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getFavoriteRests($restIds) {
        return static::find()->where(['restaurant_id' => $restIds])->all();
    }

    /**
     * Return similar restaurants
     * @return array
     */
    public static function getSimilarRestaurants($restId, $kitchens) {
        return static::find()
                     ->joinWith('kitchen')
                     ->where(['restaurants.restaurant_status' => self::STATUS_ACTIVE])
                     ->andWhere(['!=', 'restaurants.restaurant_id', $restId])
                     ->andWhere(['in', 'kitchens.uri', $kitchens])
                     ->limit(4)
                     ->all();
    }

    public static function countRestaurants() {
        return static::find()->select('restaurant_id')->count();
    }
}
