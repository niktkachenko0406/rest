<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "restaurant_types".
 *
 * @property integer $type_id
 * @property integer $restaurant_id
 * @property string $type_name
 *
 * @property Restaurants $restaurant
 */
class RestaurantTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'type_id'], 'required'],
            [['restaurant_id', 'type_id'], 'integer'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurants::className(), 'targetAttribute' => ['restaurant_id' => 'restaurant_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id'       => 'ID',
            'restaurant_id' => 'Ресторан',
            'type_name'     => 'Тип заведения',
        ];
    }

    public function getId()
    {
        return $this->type_id;
    }

    public static function multiInsert($data, $restId)
    {
        static::deleteAll('restaurant_id = ' . $restId);

        Yii::$app->db->createCommand()->batchInsert(static::tableName(), ['restaurant_id', 'type_id'],
            $data
        )->execute();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurants::className(), ['restaurant_id' => 'restaurant_id']);
    }
}
