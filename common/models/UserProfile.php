<?php

namespace common\models;

use Yii;
/**
 * Class UserProfile
 * @package common\models
 *
 * @property integer user_id
 * @property string profile_name
 * @property string profile_surname
 * @property string profile_avatar
 * @property string profile_phone
 */
class UserProfile extends User {

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_name'], 'required'],
            [['profile_name', 'profile_surname'], 'string'],
            [['profile_name', 'profile_surname', 'profile_phone'], 'trim'],
            [['profile_phone'], 'string', 'max' => 20],
            [['profile_name'],    'filter', 'filter' => 'strip_tags'],
            [['profile_surname'], 'filter', 'filter' => 'strip_tags'],
            [['profile_phone'],   'filter', 'filter' => 'strip_tags'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'profile_name'    => 'Имя',
            'profile_surname' => 'Фамилия',
            'profile_phone'   => 'Телефон'
        ];
    }

    public function getAvatar($module = null)
    {
        if($this->profile_avatar) {
            if($module == 'admin') {
                return Yii::$app->params['uploadUrl'] . '/profile/avatars/' . $this->profile_avatar;
            }

            $path = '/upload/profile/avatars/' . $this->profile_avatar;
        } else {
            $path = '/img/avatar-default.jpg';
        }

        return $path;
    }

    /**
     * Return user full name
     * @return string
     */
    public function getFullName()
    {
        return $this->profile_name . ' ' . $this->profile_surname;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Finds user by userId
     * @param string $userId
     * @return static|null
     */
    public static function findByUserId($userId)
    {
        return static::findOne(['user_id' => $userId]);
    }
}