<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\User;
use yii\db\Expression;
use yii\helpers\BaseInflector;
use yii\helpers\Inflector;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "news".
 *
 * @property string $news_id
 * @property integer $user_id
 * @property string $news_title
 * @property string $news_text
 * @property string $news_image
 * @property string $uri
 * @property string $created_at
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['news_text', 'news_title'], 'required'],
            [['news_text', 'news_title'], 'trim'],
            [['news_text'],  'string'],
            [['news_title'], 'string', 'max' => 255],
            [['news_title'], 'filter', 'filter' => 'strip_tags'],
            [['news_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'news_id'    => 'ID',
            'user_id'    => 'Автор',
            'news_title' => 'Заголовок',
            'news_text'  => 'Текст',
            'news_image' => 'Изображение',
            'created_at' => 'Создано',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('n-' . $event->sender->news_title);
            },
        ];
        $behaviors['timestamp'] = [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
            ],
            'value' => function() { return date('U');}
        ];

        return $behaviors;
    }

    public function getImg($module = null)
    {
        if($this->news_image) {
            if($module == 'admin') {
                return Yii::$app->params['uploadUrl'] . '/news/' . $this->news_image;
            }

            return '/upload/news/' . $this->news_image;
        } else {
            return '/img/dish1.jpg';
        }
    }

    public static function getModels()
    {
        return static::find()->orderBy('created_at DESC');
    }

    /**
     * Return news
     * @param $uri
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findModelByUri($uri)
    {
        return static::find()->where(['uri' => $uri])->with('comments.user.userProfile')->one();
    }

    public static function getLastNews($limit = 3)
    {
        return static::find()->limit($limit)->orderBy('created_at DESC')->all();
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile() {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['entity_id' => 'news_id'])
            ->where([
                'comment_entity' => Comments::ENTITY_NEWS,
                'comment_status' => Comments::COMMENT_ACTIVE
            ]);
    }

    public static function getNewsForSearch($search) {
        return static::find()->filterWhere(['like', 'news_title', $search])->all();
    }
}
