<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "restaurant_gallery".
 *
 * @property integer $gallery_id
 * @property integer $restaurant_id
 * @property string $gallery_image
 *
 * @property Restaurants $restaurant
 */
class RestaurantGallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'gallery_image'], 'required'],
            [['restaurant_id'], 'integer'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurants::className(), 'targetAttribute' => ['restaurant_id' => 'restaurant_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gallery_id' => 'Gallery ID',
            'restaurant_id' => 'Restaurant ID',
            'gallery_image' => 'Gallery Image',
        ];
    }

    public static function findModel($id)
    {
        return static::findOne($id);
    }

    public function getImg($thumb)
    {
        if($this->gallery_image) {
            $path = '/upload/restaurant/gallery/' . $thumb . '/' . $this->gallery_image;
        } else {
            $path = '/img/dish1.jpg';
        }

        return $path;
    }

    public function getGalleryImage($thumb)
    {
        return Yii::$app->params['uploadUrl'] . '/restaurant/gallery/preview/' . $thumb;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurants::className(), ['restaurant_id' => 'restaurant_id']);
    }

    /**
     * Return images
     * @param $restId
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findImages($restId)
    {
        return static::find()->where(['restaurant_id' => $restId])->all();
    }
}
