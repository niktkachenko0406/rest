<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\BaseInflector;
use yii\helpers\Inflector;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "partners".
 *
 * @property string $partner_id
 * @property integer $user_id
 * @property string $partner_title
 * @property string $partner_text
 * @property string $partner_image
 * @property string $uri
 * @property string $created_at
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_text', 'partner_title'], 'required'],
            [['partner_text', 'partner_title'], 'trim'],
            [['partner_text'], 'string'],
            [['created_at'], 'safe'],
            [['partner_title'], 'string', 'max' => 255],
            [['partner_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'partner_id'    => 'Partner ID',
            'user_id'       => 'Автор',
            'partner_title' => 'Заголовок',
            'partner_text'  => 'Текст',
            'partner_image' => 'Изображение',
            'created_at'    => 'Создано',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('p-' . $event->sender->partner_title);
            },
        ];
        $behaviors['timestamp'] = [
            'class'      => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
            ],
            'value' => function() { return date('U');}
        ];

        return $behaviors;
    }

    public function getImg($module = null)
    {
        if($this->partner_image) {
            if($module == 'admin') {
                return Yii::$app->params['uploadUrl'] . '/partners/' . $this->partner_image;
            }

            return '/upload/partners/' . $this->partner_image;
        } else {
            return '/img/dish1.jpg';
        }
    }

    public static function getModels()
    {
        return static::find()->orderBy('created_at DESC');
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Return news
     * @param $uri
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findModelByUri($uri)
    {
        return static::find()->where(['uri' => $uri])->one();
    }

    public static function getLastPosts($limit = 3)
    {
        return static::find()->limit($limit)->orderBy('created_at DESC')->all();
    }
}
