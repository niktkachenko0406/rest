<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subscribers".
 *
 * @property string $subscriber_id
 * @property string $email
 */
class Subscribers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscribers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'string', 'max' => 255],
            ['email', 'unique', 'message' => 'Данный Email уже зарегистрирован'],
            ['email', 'email', 'message' => 'Невалидный email'],
            [['email'], 'trim'],
            [['email'], 'filter', 'filter' => 'strip_tags'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subscriber_id' => 'Subscriber ID',
            'email' => 'Email',
        ];
    }
}
