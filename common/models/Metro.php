<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "metro".
 *
 * @property string $station_id
 * @property string $station_name
 * @property string $branch_name
 * @property string $city_id
 * @property string $district_id
 *
 * @property Cities $city
 * @property Districts $district
 */
class Metro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['station_name'], 'trim'],
            [['station_name'], 'filter', 'filter' => 'strip_tags'],
            [['station_name', 'branch_name', 'city_id'], 'required', 'message' => 'Поле не может быть пустым'],
            [['city_id', 'district_id'], 'integer'],
            [['station_name', 'branch_name'], 'string', 'max' => 80],
            [['city_id'],     'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'city_id']],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => Districts::className(), 'targetAttribute' => ['district_id' => 'district_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'station_id'   => 'ID',
            'station_name' => 'Название',
            'branch_name'  => 'Ветка',
            'city_id'      => 'Город',
            'district_id'  => 'Район',
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        $behaviors         = parent::behaviors();
        $behaviors['slug'] = [
            'class'         => SluggableBehavior::className(),
            'slugAttribute' => 'uri',
            'ensureUnique'  => true,
            'value'         => function ($event) {
                if (!empty($event->sender->uri)) {
                    return $event->sender->uri;
                }

                return Inflector::slug('m-' . $event->sender->station_name);
            },
        ];

        return $behaviors;
    }

    public static function getMetroByDistrict($districtId)
    {
        return static::find()->select('station_id, station_name')
                             ->where(['district_id' => $districtId])
                             ->asArray()
                             ->all();
    }

    public static function getMetroByCity($cityId)
    {
        return static::find()->select('station_id, station_name, uri')
                             ->where(['city_id' => $cityId])
                             ->all();
    }

    public static function getMetroNames($stations) {
        $stations = explode(' ', $stations);
        $stations = static::find()
                  ->select('station_name')
                  ->where(['in', 'uri', $stations])
                  ->asArray()
                  ->all();

        $stationNames = [];

        foreach($stations as $station) {
            $stationNames[] = $station['station_name'];
        }

        return implode(',', $stationNames);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(Districts::className(), ['district_id' => 'district_id']);
    }
}
