<?php

namespace common\lybrary\upload;

/**
 * Class Help Info
 *
 *          ->setFile()                      // Set upload file
 *          ->setFileExtensions()            // Allowed Extensions
 *          ->setFileRename()                // Rename file true/false
 *          ->setMaxSize()                   // Max size MB
 *          ->setMaxWidth()                  // Max Width PX
 *          ->setMaxHeight()                 // Max Height PX
 *          ->setMinWidth()                  // Min Width PX
 *          ->setMinHeight()                 // Min Height PX
 *          ->saveOriginal('path', 'suffix') // Save original file  (suffix if need)
 *
 *          // Add thumbnails
 *          // Max width and Max Height (suffix if need) and type adaptive/crop
 *          ->addThumbnail('path', width, height, 'suffix', 'type')
 *          ->addThumbnails(array(array(array('path', width, height, 'suffix', 'type') ... array N)
 *
 *          To set document root (without '/' slash in the end)
 *          ->setDocumentRoot()
 *
 *          ->isErrorExists()     // Return is error exists
 *          ->getErrors()         // Return all errors
 *          ->getFileName()       // Return file name
 *          ->getFileExtension()  // Return file extension
 *          ->upload();           // Upload file
 */

/*----------------------------------------------------------------------------------------------------------------------------------*/

/**
 * Class PhpUpload
 * @author Alexandr Malenko (Bitrop) <bitrop@mail.ru>
 * @copyright Alexandr Malenko (Bitrop)
 * 2013
 * Version 3.0
 * Date 20.05.2014
 */
Class Upload
{
    protected $file;
    protected $maxSize        = 0;          // Max file size in MB. 0 - unlimited
    protected $maxWidth       = 0;          // Max file width.      0 - unlimited
    protected $maxHeight      = 0;          // Max file height.     0 - unlimited
    protected $minWidth       = 0;          // Min file width.      0 - unlimited
    protected $minHeight      = 0;          // Min file height.     0 - unlimited
    protected $fileRename     = false;      // Is file rename
    protected $fileExtensions = [];         // Allowed file extensions. Empty array all allowed extensions
    protected $thumbnails     = [];         // Image thumbnails
    protected $documentRoot   = '/';        // document root
    protected $originalPath   = '';         // Original Path
    protected $originalSuffix;              // Original Suffix
    protected $errors;                      // Errors
    protected $fileName;                    // File name
    protected $fileExtension;               // File extension
    protected $fileProportions;             // File width and height
    protected $chunkErrors     = [];
    protected $quality         = 50;
    protected $compress        = false;
    protected $minSizeCompress = 500;
    protected $thumbnailsCount;

    const FILE_UPLOAD_ERROR     = 'Ошибка загрузки, попробуйте позже';
    const FILE_SIZE_ERROR       = 'Файл превышает допустимый размер';
    const FILE_EXTS_ERROR       = 'Загружать можно только изображения: jpg, jpg, gif, png';
    const FILE_MAX_WIDTH_ERROR  = 'Изображение превышает допустимую максимальную ширину';
    const FILE_MAX_HEIGHT_ERROR = 'Изображение превышает допустимую максимальную высоту';
    const FILE_MIN_WIDTH_ERROR  = 'Минимальная допустимая ширина привышает ширину изображения';
    const FILE_MIN_HEIGHT_ERROR = 'Минимальная допустимая ширина привышает высоту изображения';
    const FILE_COPY_ERROR       = 'Ошибка копирования, попробуйте позже';
    const FILE_SAVE_ERROR       = 'Ошибка сохранения, попробуйте позже';

    const UNLIMITED   = 0;
    const FILE_WIDTH  = 0;
    const FILE_HEIGHT = 1;

    const THUMBNAIL_PATH   = 0;
    const THUMBNAIL_WIDTH  = 1;
    const THUMBNAIL_HEIGHT = 2;
    const THUMBNAIL_SUFFIX = 3;
    const THUMBNAIL_TYPE   = 4;

    const ADAPTIVE_RESIZE  = 'adaptive';
    const CROP_RESIZE      = 'crop';
    const CROP_CUSTOM      = 'custom';

    const LANDSCAPE = 0;
    const PORTRAIT  = 1;

    const FOLDER_PERMISSION = 0755;

    const DEFAULT_COMPRESS_QUALITY = 70;

    /**
     * Set document root
     */
    public function __construct() {
        $this->documentRoot = \Yii::$app->params['uploadPath'] . '/';
    }

    /**
     * @param $method
     * @param $params
     * @return bool
     */
    public function __call($method, $params) {
        if(substr($method, 0, 2) != 'is') {
            return false;
        }

        return $this->getFileExtension() == strtolower(substr($method, 2));
    }

    /**
     * @param string $documentRoot
     * @return $this
     */
    public function setDocumentRoot($documentRoot) {
        $this->documentRoot = $documentRoot;

        return $this;
    }

    /**
     * @param mixed $file
     * @return $this
     */
    public function setFile($file) {
        $this->clearCache();
        $this->file = (object) $file;

        return $this;
    }

    public function setQuality($quality = self::DEFAULT_COMPRESS_QUALITY) {
        $this->quality = $quality;

        return $this;
    }

    public function getQuality() {
        if($this->fileExtension == 'png') {
            $this->quality = 9 / 100 * $this->quality ;
        }

        return $this->quality;
    }

    /**
     * In KB
     * @param $size
     * @return $this
     */
    public function setMinCompressSize($size) {
        $this->minSizeCompress = $size;

        return $this;
    }

    /**
     * Now it's if file less then max width/height
     * @param bool $isCompress
     * @return $this
     */
    public function setFileCompress($isCompress = false) {
        $this->compress = $isCompress;

        return $this;
    }

    /**
     * Clear cache
     */
    public function clearCache() {
        $this->fileExtension = null;
    }

    /**
     * @param array $fileExtensions
     * @return $this
     */
    public function setFileExtensions($fileExtensions) {
        $this->fileExtensions = $fileExtensions;

        return $this;
    }

    /**
     * @param boolean $fileRename
     * @return $this
     */
    public function setFileRename($fileRename) {
        $this->fileRename = $fileRename;

        return $this;
    }

    /**
     * @param int $maxHeight
     * @return $this
     */
    public function setMaxHeight($maxHeight) {
        $this->maxHeight = $maxHeight;

        return $this;
    }

    /**
     * @param int $maxSize
     * @return $this
     */
    public function setMaxSize($maxSize) {
        $this->maxSize = $maxSize;

        return $this;
    }

    /**
     * @param int $maxWidth
     * @return $this
     */
    public function setMaxWidth($maxWidth) {
        $this->maxWidth = $maxWidth;

        return $this;
    }

    /**
     * @param int $minHeight
     * @return $this
     */
    public function setMinHeight($minHeight) {
        $this->minHeight = $minHeight;

        return $this;
    }

    /**
     * @param int $minWidth
     * @return $this
     */
    public function setMinWidth($minWidth) {
        $this->minWidth = $minWidth;

        return $this;
    }

    /**
     * @param string $filePath
     * @param string $fileSuffix
     * @return $this
     */
    public function saveOriginal($filePath, $fileSuffix = '') {
        $this->originalPath    = $filePath;
        $this->originalSuffix = ($fileSuffix != '') ? $fileSuffix . '_' : '';

        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function addThumbnail($data) {
        $this->thumbnails[] = $data;

        return $this;
    }

    /**
     * @param $thumbnails
     * @return $this
     */
    public function addThumbnails($thumbnails) {
        $this->thumbnails = $thumbnails;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileExtension() {
        if(empty($this->fileExtension)) {
            $this->fileExtension = substr($this->file->name, strrpos($this->file->name, '.') + 1);
        }

        return $this->fileExtension;
    }

    /**
     * @return mixed
     */
    public function getFileName() {
        return $this->fileName;
    }

    /**
     * @param $error
     */
    public function addError($error) {
        $this->errors[] = $error;
    }

    /**
     * @return mixed
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * @return int
     */
    public function isErrorExists() {
        return count($this->errors) > 0;
    }

    /**
     * @return string
     */
    public function fileRename() {
        if($this->fileRename) {
            $fileHash       = substr(md5(md5(microtime(true)) . md5(0, 100)), 0, 10);
            $this->fileName = $fileHash . '.' . $this->fileExtension;
        } else {
            $this->fileName = $this->file->name;
        }
    }

    /**
     * File allow extensions check
     */
    public function checkAllowedExtension() {
        $this->fileExtension = $this->getFileExtension();

        if(!empty($this->fileExtensions) AND !in_array(strtolower($this->fileExtension), $this->fileExtensions)) {
            $this->addError(self::FILE_EXTS_ERROR);
        }
    }

    /**
     * File size check
     */
    public function checkFileSize() {
        if($this->maxSize != self::UNLIMITED AND $this->file->size > $this->maxSize * 1024 * 1024) {
            $this->addError(self::FILE_SIZE_ERROR);
        }
    }

    /**
     * File check proportions
     */
    public function fileCheckProportions() {
        if($fileSize = getimagesize($this->file->tempName)) {
            $this->fileProportions = $fileSize;
        }

        if($this->maxWidth != self::UNLIMITED AND $this->maxWidth < $fileSize[self::FILE_WIDTH]) {
            $this->addError(self::FILE_MAX_WIDTH_ERROR);
        }

        if($this->maxHeight != self::UNLIMITED AND $this->maxHeight < $fileSize[self::FILE_HEIGHT]) {
            $this->addError(self::FILE_MAX_HEIGHT_ERROR);
        }

        if($this->minWidth != self::UNLIMITED AND $this->minWidth > $fileSize[self::FILE_WIDTH]) {
            $this->addError(self::FILE_MIN_WIDTH_ERROR);
        }

        if($this->minHeight != self::UNLIMITED AND $this->minHeight > $fileSize[self::FILE_HEIGHT]) {
            $this->addError(self::FILE_MIN_HEIGHT_ERROR);
        }
    }

    /**
     * Save original file
     * !!! We don't use move_uploaded file for uploading by parts
     */
    public function saveOriginalFile() {
        if(!empty($this->originalPath)) {
            if(!copy($this->file->tempName, $this->documentRoot . $this->originalPath. '/' . $this->originalSuffix . $this->fileName)) {
                $this->addError(self::FILE_UPLOAD_ERROR);
            }

            if(!$this->thumbnailsCount) {
                unlink($this->file->tempName);
            }
        }
    }

    /**
     * Function for creating thumbnails
     */
    public function createThumbnails() {
        if(count($this->thumbnails) > 0) {
            foreach ($this->thumbnails as $thumbnail) {
                $path = $this->documentRoot . current($thumbnail);

                if(!is_dir($path)){
                    mkdir($path, self::FOLDER_PERMISSION, true);
                }

                $this->fileResize(
                    $thumbnail['path'],
                    isset($data['prefix'])      ? $data['prefix']      : '',
                    isset($thumbnail['width'])  ? $thumbnail['width']  : 0,
                    isset($thumbnail['height']) ? $thumbnail['height'] : 0,
                    isset($thumbnail['type'])   ? $thumbnail['type']   : self::ADAPTIVE_RESIZE,
                    isset($thumbnail['crop'])   ? $thumbnail['crop']   : []
                );
            }
        }
    }

    /**
     * Just compress image without resize
     */
    public function fileCompress($thumbnailPath, $thumbnailSuffix = '') {
        $extension = (strtolower($this->fileExtension) == 'jpg') ? 'jpeg' : strtolower($this->fileExtension);
        $functionImageFrom = 'imagecreatefrom' . $extension;
        $functionImageSave = 'image' . $extension;
        $image = $functionImageFrom($this->file->tempName);

        $imagePath = $this->documentRoot
            . $thumbnailPath . '/'
            . (($thumbnailSuffix != '') ? $thumbnailSuffix . '_' : '')
            . $this->fileName;

        $functionImageSave($image, $imagePath, $this->getQuality());
    }

    /**
     * @param $thumbnailPath
     * @param $thumbnailSuffix
     * @param $thumbnailMaxWidth
     * @param $thumbnailMaxHeight
     * @param $thumbnailType
     * @return bool
     */
    public function fileResize($thumbnailPath, $thumbnailSuffix, $thumbnailMaxWidth, $thumbnailMaxHeight, $thumbnailType, $data = []) {
        $originalWidth  = $this->fileProportions[self::FILE_WIDTH];
        $originalHeight = $this->fileProportions[self::FILE_HEIGHT];
        $imageType      = ($originalWidth >= $originalHeight) ? self::LANDSCAPE : self::PORTRAIT;

        $this->thumbnailsCount--;

        if((!$thumbnailMaxWidth && !$thumbnailMaxHeight) || ($originalWidth <= $thumbnailMaxWidth && $originalHeight <= $thumbnailMaxHeight)) {
            $imageSize = round(filesize($this->file->tempName) / 1024, 2);

            if($this->compress && (!$this->minSizeCompress || $imageSize > $this->minSizeCompress)) {
                $this->fileCompress($thumbnailPath);
            } else {
                $this->saveOriginal($thumbnailPath, $thumbnailSuffix);
                $this->saveOriginalFile();
            }

            return true;
        } elseif($thumbnailType == self::CROP_CUSTOM) {
            $thumbnailWidth  = $data['width'];
            $thumbnailHeight = $data['height'];
        }
        elseif(($thumbnailType == self::ADAPTIVE_RESIZE AND $imageType == self::LANDSCAPE) OR
            ($thumbnailType == self::CROP_RESIZE     AND $imageType == self::PORTRAIT)) {
            $thumbnailHeight = round($originalHeight / (round(($originalWidth / $thumbnailMaxWidth), 2)));
            $thumbnailWidth  = $thumbnailMaxWidth;
        } else {
            $thumbnailWidth   = round($originalWidth / (round(($originalHeight / $thumbnailMaxHeight), 2)));
            $thumbnailHeight  = $thumbnailMaxHeight;
        }

        $extension         = (strtolower($this->fileExtension) == 'jpg') ? 'jpeg' : strtolower($this->fileExtension);
        $functionImageFrom = 'imagecreatefrom' . $extension;
        $functionImageSave = 'image' . $extension;

        $newImagePath  = $this->documentRoot . $thumbnailPath .  '/' . (($thumbnailSuffix != '') ? $thumbnailSuffix . '_' : '') . $this->fileName;
        $resourceImage = $functionImageFrom(empty($this->originalPath) ? $this->file->tempName :
            $this->documentRoot . $this->originalPath. '/' . $this->originalSuffix . $this->fileName);

        if($extension == 'png') {
            imagealphablending($resourceImage, false);
            imagesavealpha($resourceImage, true);
        }

        //@todo: $this->thumbnailsCount--; fix
        if($thumbnailType == self::CROP_CUSTOM) {
            $this->customCrop(
                $newImagePath, $functionImageFrom($this->file->tempName),
                $data['width'], $data['height'],
                $data['x'], $data['y'], $functionImageSave
            );

            $this->originalPath = $thumbnailPath;

            $this->fileProportions[self::FILE_WIDTH]  = $data['width'];
            $this->fileProportions[self::FILE_HEIGHT] = $data['height'];

            return $this->fileResize($thumbnailPath, $thumbnailSuffix, $thumbnailMaxWidth, $thumbnailMaxHeight, self::ADAPTIVE_RESIZE);
        }

        $newImage          = imagecreatetruecolor($thumbnailWidth, $thumbnailHeight);
        $backgroundColor   = imagecolorallocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $backgroundColor);

        if(!imagecopyresampled($newImage, $resourceImage, 0, 0, 0, 0, $thumbnailWidth, $thumbnailHeight, $originalWidth, $originalHeight)) {
            $this->addError(self::FILE_COPY_ERROR);
        } else {
            if(!$functionImageSave($newImage, $newImagePath)) {
                $this->addError(self::FILE_SAVE_ERROR);
            } else {
                if ($thumbnailType == self::CROP_RESIZE AND $imageType == self::PORTRAIT) {
                    $thumbnailMarginLeft = 0;
                    $thumbnailMarginTop = round(($thumbnailHeight - $thumbnailMaxHeight) / 2);
                    $this->imageCrop($newImagePath, $functionImageFrom($newImagePath), $functionImageSave, $thumbnailMarginLeft, $thumbnailMarginTop, $thumbnailMaxWidth, $thumbnailMaxHeight);
                }

                if ($thumbnailType == self::CROP_RESIZE AND $imageType == self::LANDSCAPE) {
                    $thumbnailMarginTop = 0;
                    $thumbnailMarginLeft = round(($thumbnailWidth - $thumbnailMaxWidth) / 2);
                    $this->imageCrop($newImagePath, $functionImageFrom($newImagePath), $functionImageSave, $thumbnailMarginLeft, $thumbnailMarginTop, $thumbnailMaxWidth, $thumbnailMaxHeight);
                }
            }
        }
    }

    /**
     * @param $newImagePath
     * @param $resourceImage
     * @param $functionImageSave
     * @param $thumbnailMarginLeft
     * @param $thumbnailMarginTop
     * @param $thumbnailMaxWidth
     * @param $thumbnailMaxHeight
     */
    public function imageCrop($newImagePath, $resourceImage, $functionImageSave, $thumbnailMarginLeft, $thumbnailMarginTop, $thumbnailMaxWidth, $thumbnailMaxHeight) {
        $newImage          = imagecreatetruecolor($thumbnailMaxWidth, $thumbnailMaxHeight);
        $backgroundColor   = imagecolorallocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $backgroundColor);

        if(!imagecopyresampled($newImage, $resourceImage, 0, 0, $thumbnailMarginLeft, $thumbnailMarginTop, $thumbnailMaxWidth, $thumbnailMaxHeight, $thumbnailMaxWidth, $thumbnailMaxHeight)) {
            $this->addError(self::FILE_COPY_ERROR);
        } else {
            if(!$functionImageSave($newImage, $newImagePath)) {
                $this->addError(self::FILE_SAVE_ERROR);
            }
        }
    }

    /**
     * Custom image crop can use directly or after file upload
     * @param $newImagePath
     * @param $resourceImage string | resource
     * @param $width int
     * @param $height int
     * @param $imageX int
     * @param $imageY int
     * @param $functionImageSave string | null
     * @param bool $rename
     * @return bool|string
     */
    public function customCrop($newImagePath, $resourceImage, $width, $height, $imageX, $imageY, $functionImageSave = null, $rename = false) {
        $isResource = is_resource($resourceImage);

        if(!$isResource) {
            $fileExtension     = substr($resourceImage, strrpos($resourceImage, '.') + 1);
            $extension         = (strtolower($fileExtension) == 'jpg') ? 'jpeg' : strtolower($fileExtension);
            $functionImageFrom = 'imagecreatefrom' . $extension;
            $functionImageSave = 'image'           . $extension;
            $resourceImage     = $functionImageFrom($this->documentRoot . $resourceImage);
            $newImagePath      = $this->documentRoot . $newImagePath;
        }

        $this->imageCrop(
            $newImagePath, $resourceImage,
            $functionImageSave,
            $imageX, $imageY, $width, $height
        );

        if(!$isResource && $rename) {
            $fileName = substr(md5(md5(microtime(true)) . md5(0, 100)), 0, 10);
            $filePath = substr($newImagePath, 0, strrpos($newImagePath, '/'));
            rename($newImagePath, sprintf('%s/%s.%s', $filePath, $fileName, $fileExtension));

            return sprintf('%s.%s',$fileName, $fileExtension);
        }
    }

    public function isImage() {
        return (bool) strstr($this->file->type, 'image/');
    }

    /**
     * Uploading
     * @return $this
     */
    public function upload() {
        $this->checkFileSize();
        $this->checkAllowedExtension();
        $this->fileCheckProportions();
        $this->fileRename();

        $this->thumbnailsCount = count($this->thumbnails);

        if(count($this->getErrors()) == 0) {
            $this->saveOriginalFile();
        }

        if(count($this->getErrors()) == 0) {
            $this->createThumbnails();
        }

        return $this;
    }

    /**
     * Return chunk errors
     *
     * @return array
     */
    public function getChunkErrors() {
        return $this->chunkErrors;
    }

    /**
     * Assembly of the parts of the file
     *
     * @param $request
     * @return array|bool
     */
    public function isFileCompiled($request)
    {
        if (empty($this->file) || $this->file->error) {
            $this->chunkErrors[] = 'Failed to move uploaded file';
        }

        $chunk    = isset($request['chunk']) ? intval($request['chunk']) : 0;
        $chunks   = isset($request['chunks']) ? intval($request['chunks']) : 0;
        $fileName = isset($request['name']) ? $request['name'] : $this->file->name;
        $filePath = sys_get_temp_dir() . "/$fileName";
        $out      = fopen("{$filePath}.part", $chunk == 0 ? 'wb' : 'ab');

        if ($out) {
            $in = fopen($this->file->tempName, 'rb');

            if ($in) {
                while ($buff = fread($in, 4096)) {
                    fwrite($out, $buff);
                }
            } else {
                $this->chunkErrors[] = 'Failed to open input stream';

                return false;
            }

            fclose($in);
            fclose($out);

            unlink($this->file->tempName);
        } else {
            $this->chunkErrors[] = 'Failed to open output stream';

            return false;
        }

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            return [
                'media_image' => [
                    'name'     => $fileName,
                    'type'     => 'image/jpeg',
                    'tempName' => $filePath,
                    'error'    => 0,
                    'size'     => filesize($filePath)
                ]
            ];
        }

        return true;
    }

    /**
     * @param $file
     * @return array|bool
     */
    public function prepareFile($file)
    {
        if($file) {
            $filePath     = sys_get_temp_dir() . "/image.png";
            $file         = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file));
            $fileInfo     = finfo_open();
            $mimeType     = finfo_buffer($fileInfo, $file, FILEINFO_MIME_TYPE);
            $infoBuffer   = explode('/', $mimeType);
            $imgExtension = array_pop($infoBuffer);

            if(file_put_contents($filePath, $file)) {
                return [
                    'img_name' => [
                        'name'     => 'image.' . $imgExtension,
                        'type'     => 'image/' . $imgExtension,
                        'tempName' => $filePath,
                        'error'    => 0,
                        'size'     => filesize($filePath)
                    ]
                ];
            }
        }

        return false;
    }

    /**
     * @author bitrop
     * @param $entity
     * @param $fileName
     * @param array $folders
     * @param string $destination
     */
    public static function clearThumbnails($entity, $fileName, $folders = [], $destination = 'frontend') {
        $baseFolder = \Yii::$app->params['uploadPath'] . '/upload';
        $folders    = $folders ?: null;

        if($folders) {
            foreach ($folders as $folder) {
                $file = "{$baseFolder}/{$entity}/{$folder}/{$fileName}";

                if (file_exists($file)) {
                    unlink($file);
                }
            }
        } else {
            $file = "{$baseFolder}/{$entity}/{$fileName}";

            if (file_exists($file)) {
                unlink($file);
            }
        }
    }

    /**
     * Clear Live thumbnails
     * @param $folders
     * @param $fileName
     */
    public static function clearLiveThumbnails($folders, $fileName) {
        foreach ($folders as $folder) {
            $file = "{$folder}/{$fileName}";

            if (file_exists($file)) {
                unlink($file);
            }
        }
    }
}