<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Providers;

/**
 * ProvidersSearch represents the model behind the search form about `common\models\Providers`.
 */
class ProvidersSearch extends Providers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'city_id', 'district_id'], 'integer'],
            [['provider_name', 'provider_short_desc', 'provider_desc', 'provider_email', 'provider_phone', 'provider_site', 'provider_address', 'provider_avatar', 'uri'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Providers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'provider_id' => $this->provider_id,
            'city_id' => $this->city_id,
            'district_id' => $this->district_id,
        ]);

        $query->andFilterWhere(['like', 'provider_name', $this->provider_name])
            ->andFilterWhere(['like', 'provider_short_desc', $this->provider_short_desc])
            ->andFilterWhere(['like', 'provider_desc', $this->provider_desc])
            ->andFilterWhere(['like', 'provider_email', $this->provider_email])
            ->andFilterWhere(['like', 'provider_phone', $this->provider_phone])
            ->andFilterWhere(['like', 'provider_site', $this->provider_site])
            ->andFilterWhere(['like', 'provider_address', $this->provider_address])
            ->andFilterWhere(['like', 'provider_avatar', $this->provider_avatar])
            ->andFilterWhere(['like', 'uri', $this->uri]);

        return $dataProvider;
    }
}
