<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Blog as BlogModel;

/**
 * Blog represents the model behind the search form about `common\models\Blog`.
 */
class BlogSearch extends BlogModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blog_id', 'restaurant_id', 'blog_status'], 'integer'],
            [['blog_title', 'blog_text', 'blog_image', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BlogModel::find()->with('restaurant')->where(['restaurant_id' => $params['restId']]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'blog_id' => $this->blog_id,
            'restaurant_id' => $this->restaurant_id,
            'blog_status' => $this->blog_status,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'blog_title', $this->blog_title])
            ->andFilterWhere(['like', 'blog_text', $this->blog_text])
            ->andFilterWhere(['like', 'blog_image', $this->blog_image])
            ->andFilterWhere(['like', 'restaurant_name', $this->restaurant->restaurant_name]);

        return $dataProvider;
    }
}
