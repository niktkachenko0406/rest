<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Comments;

/**
 * CommentsSearch represents the model behind the search form about `common\models\Comments`.
 */
class CommentsBlogSearch extends Comments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment_id',
                'user_id',
                'entity_id',
                'comment_kitchen_rating',
                'comment_interier_rating',
                'comment_service_rating',
                'comment_ambience_rating',
                'comment_entity',
                'comment_status',
                'created_at'
            ], 'integer'],
            [['comment_text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comments::find()->where([
            'entity_id'      => $params['id'],
            'comment_entity' => Comments::ENTITY_BLOG
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'comment_id' => $this->comment_id,
            'user_id' => $this->user_id,
            'entity_id' => $this->entity_id,
            'comment_kitchen_rating' => $this->comment_kitchen_rating,
            'comment_interier_rating' => $this->comment_interier_rating,
            'comment_service_rating' => $this->comment_service_rating,
            'comment_ambience_rating' => $this->comment_ambience_rating,
            'comment_entity' => $this->comment_entity,
            'comment_status' => $this->comment_status,
        ]);

        $query->andFilterWhere(['like', 'comment_text', $this->comment_text]);

        return $dataProvider;
    }
}
