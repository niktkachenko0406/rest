<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Metro;
use common\models\Cities;

/**
 * MetroSearch represents the model behind the search form about `common\models\Metro`.
 */
class MetroSearch extends Metro
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['station_id', 'city_id', 'district_id'], 'integer'],
            [['station_name', 'branch_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Metro::find()->with(['city', 'district']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'station_id' => $this->station_id,
            'city_id' => $this->city_id,
            'district_id' => $this->district_id,
        ]);

        $query->andFilterWhere(['like', 'station_name', $this->station_name])
            ->andFilterWhere(['like', 'branch_name', $this->branch_name]);

        return $dataProvider;
    }
}
