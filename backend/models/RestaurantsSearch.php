<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Restaurants;

/**
 * RestaurantsSearch represents the model behind the search form about `common\models\Restaurants`.
 */
class RestaurantsSearch extends Restaurants
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'restaurant_id',
                'user_id',
                'restaurant_average_check',
                'city_id',
                'district_id',
            ], 'integer'],
            [[
                'restaurant_name',
                'restaurant_short_desc',
                'restaurant_desc',
                'restaurant_schedule',
                'restaurant_address',
                'restaurant_avatar',
                'restaurant_phone'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Restaurants::find()->with('user.userProfile', 'city', 'district');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'restaurant_id'            => $this->restaurant_id,
            'user_id'                  => $this->user_id,
            'restaurant_average_check' => $this->restaurant_average_check,
            'city_id'                  => $this->city_id,
            'district_id'              => $this->district_id,
        ]);

        $query->andFilterWhere(['like', 'restaurant_name', $this->restaurant_name])
            ->andFilterWhere(['like', 'restaurant_short_desc', $this->restaurant_short_desc])
            ->andFilterWhere(['like', 'restaurant_desc', $this->restaurant_desc])
            ->andFilterWhere(['like', 'restaurant_schedule', $this->restaurant_schedule])
            ->andFilterWhere(['like', 'restaurant_address', $this->restaurant_address])
            ->andFilterWhere(['like', 'restaurant_avatar', $this->restaurant_avatar])
            ->andFilterWhere(['like', 'restaurant_phone', $this->restaurant_phone]);

        return $dataProvider;
    }
}
