<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;

/**
 * Class SignupForm
 * @package backend\models
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $status;
    public $role;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required', 'message' => 'Поле не может быть пустым'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данный Email уже зарегистрирован'],

            ['password', 'required', 'message' => 'Поле не может быть пустым'],
            ['password', 'string', 'min' => 6, 'message' => 'Минимум 6 символов'],

            ['role', 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'    => 'Электронная почта *',
            'password' => 'Пароль *',
            'role'     => 'Роль'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup($type = null)
    {
        if (!$this->validate()) {
            return null;
        }

        $user            = new User();
        $user->email     = $this->email;
        $user->user_type = $type ? User::USER_TYPE_ADMIN : User::USER_TYPE_USER;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        $save = $user->save();

        if($type && $save) {
            $auth    = \Yii::$app->authManager;
            $getRole = $auth->getRole($this->role);
            $auth->assign($getRole, $user->id);
        }

        return $save ? $user : null;
    }
}
