<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProvidersServices;

/**
 * ProvidersServicesSearch represents the model behind the search form about `common\models\ProvidersServices`.
 */
class ProvidersServicesSearch extends ProvidersServices
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'subcategory_id', 'provider_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProvidersServices::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subcategory_id' => $this->subcategory_id,
            'provider_id' => $this->provider_id,
        ]);

        return $dataProvider;
    }
}
