<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\News;

/**
 * NewsSearch represents the model behind the search form about `common\models\News`.
 */
class NewsSearch extends News
{
    public $profile_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'user_id'], 'integer'],
            [['profile_name', 'news_title', 'news_text', 'news_image', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find()->with('user.userProfile')->orderBy('created_at DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['profile_name'] = [
            'asc'  => ['user_profile.profile_name' => SORT_ASC],
            'desc' => ['user_profile.profile_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'news_id'    => $this->news_id,
            'user_id'    => $this->user_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'news_title', $this->news_title])
              ->andFilterWhere(['like', 'news_text', $this->news_text])
              ->andFilterWhere(['like', 'news_image', $this->news_image])
              ->joinWith(['userProfile' => function ($q) {
                  $q->where('`user_profile`.profile_name LIKE "%' . $this->profile_name . '%"');}
              ]);

        return $dataProvider;
    }
}