<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProviderSubcategories;

/**
 * ProviderSubcategoriesSearch represents the model behind the search form about `common\models\ProviderSubcategories`.
 */
class ProviderSubcategoriesSearch extends ProviderSubcategories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subcategory_id', 'category_id'], 'integer'],
            [['subcategory_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProviderSubcategories::find()->with('category');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'subcategory_id' => $this->subcategory_id,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'subcategory_name', $this->subcategory_name]);

        return $dataProvider;
    }
}
