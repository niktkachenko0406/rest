<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    public $profile_name;
    public $profile_phone;
//    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            ['email', 'trim'],
//            ['email', 'required', 'message' => 'Поле не может быть пустым'],
//            ['email', 'email'],
//            ['email', 'string', 'max' => 255],
//            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данный Email уже зарегистрирован'],
//            ['password', 'required', 'message' => 'Поле не может быть пустым'],
//            ['password', 'string', 'min' => 6, 'message' => 'Минимум 6 символов'],
//            ['user_type', 'integer'],
            [['id', 'status', 'user_type', 'created_at', 'updated_at'], 'integer'],
            [['profile_phone', 'profile_name', 'auth_key', 'password_hash', 'password_reset_token', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->joinWith('userProfile')->where(['user_type' => !empty($params['type']) ? $params['type'] : User::USER_TYPE_USER]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['profile_name'] = [
            'asc'  => ['profile_name' => SORT_ASC],
            'desc' => ['profile_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['profile_phone'] = [
            'asc'  => ['profile_phone' => SORT_ASC],
            'desc' => ['profile_phone' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'status'     => $this->status,
            'user_type'  => $this->user_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'profile_name', $this->profile_name])
            ->andFilterWhere(['like', 'profile_phone', $this->profile_phone])
//            ->joinWith(['userProfile' => function ($q) {
//                $q->where('`user_profile`.profile_name LIKE "%' . $this->profile_name . '%"');}
//            ])
        ;

        return $dataProvider;
    }
}
