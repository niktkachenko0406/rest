<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RestaurantFeatures;

/**
 * RestaurantFeaturesSearch represents the model behind the search form about `common\models\RestaurantFeatures`.
 */
class RestaurantFeaturesSearch extends RestaurantFeatures
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feature_id', 'restaurant_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RestaurantFeatures::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'feature_id' => $this->feature_id,
            'restaurant_id' => $this->restaurant_id,
        ]);

        return $dataProvider;
    }
}
