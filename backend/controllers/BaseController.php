<?php

namespace backend\controllers;

use yii\web\Controller;

class BaseController extends Controller {

    public $user;

    public function init()
    {
        $this->user = \Yii::$app->user->identity;
    }

    /**
     * Return id authorisation user
     * @return mixed
     */
    public function getAuthId()
    {
        return \Yii::$app->user->identity->getId();
    }

    /**
     * Return user type
     * @return mixed
     */
    public function getAuthType()
    {
        return \Yii::$app->user->identity->getType();
    }
}