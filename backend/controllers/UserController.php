<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserProfile;
use common\lybrary\upload\Upload;
use backend\models\UserSearch;
use backend\models\PasswordChangeForm;
use backend\models\SignupForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\base\Exception;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex($type = null)
    {

        $searchModel  = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'type'         => $type
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $type = null)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'type'  => $type
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = null)
    {
        $request     = \Yii::$app->request;
        $post        = $request->post();
        $userModel   = new User();
        $userProfile = new UserProfile();
        $model       = new SignupForm();

        if($request->isPost && $model->load($post) && $userProfile->load($post) && $userProfile->validate() && $user = $model->signup($type)) {
            $userProfile->user_id = $user->id;
            $file                 = UploadedFile::getInstance($userProfile, 'profile_avatar');
            $upload               = new Upload();

            if($file) {
                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->addThumbnails([[
                        'path'   => '/upload/profile/avatars',
                        'width'  => 300,
                        'height' => 300,
                        'type'   => 'crop'
                    ]])
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $userProfile->profile_avatar = $upload->getFileName();
                }
            }

            $userProfile->save();

            return $this->redirect(['view', 'id' => $user->id, 'type' => User::USER_TYPE_ADMIN]);
        } else {
            return $this->render('create', [
                'user'    => $userModel,
                'profile' => $userProfile,
                'model'   => $model,
                'type'    => $type
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $type = null)
    {
        /** @var UserProfile $userProfile */
        $user           = $this->findModel($id);
        $request        = \Yii::$app->request;
        $post           = $request->post();
        $userProfile    = $user->userProfile ?: new UserProfile();
        $passwordChange = new PasswordChangeForm($user);

        if($request->isPost && $userProfile->load($post) && $user->load($post)) {
            $userProfile->user_id = $user->id;
            $file                 = UploadedFile::getInstance($userProfile, 'profile_avatar');
            $upload               = new Upload();

            if($file) {
                if($userProfile->profile_avatar) {
                    Upload::clearThumbnails('profile', $userProfile->profile_avatar, ['avatars']);
                }

                $upload->setFile($file)
                       ->setFileExtensions(['jpg', 'png', 'jpeg'])
                       ->setMaxSize(6)
                       ->setFileRename(true)
                       ->addThumbnails([[
                           'path'   => '/upload/profile/avatars',
                           'width'  => 300,
                           'height' => 300,
                           'type'   => 'crop'
                       ]])
                       ->upload();

                if (!$upload->isErrorExists()) {
                    $userProfile->profile_avatar = $upload->getFileName();
                }
            }

            if($userProfile->save() && $user->save()) {
                if($type) {
                    $auth        = Yii::$app->authManager;
                    $userNewRole = $post['User']['role'];
                    $userOldRole = current($auth->getRolesByUser($user->id))->name;

                    if($userNewRole != $userOldRole) {
                        $newRole = $auth->getRole($userNewRole);
                        $oldRole = $auth->getRole($userOldRole);
                        $auth->revoke($oldRole, $user->id);
                        $auth->assign($newRole, $user->id);
                    }
                }

                return $this->redirect(Url::to(['/user/view', 'id' => $userProfile->user_id, 'type' => $type]));
            }
        }

        return $this->render('update', [
            'user'           => $user,
            'profile'        => $userProfile,
            'passwordChange' => $passwordChange,
            'type'           => $type
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $type = null)
    {
        $this->findModel($id)->delete();

        if($type) {
            $auth     = Yii::$app->authManager;
            $userRole = current($auth->getRolesByUser($id))->name;

            if($userRole) {
                $role = $auth->getRole($userRole);
                $auth->revoke($role, $id);
            }
        }

        return $this->redirect(['index', 'type' => $type]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = User::find()->where(['id' => $id])->joinWith('userProfile')->one();

        if($model) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Approved user
     * @param int $id
     * @param int $status
     * @return mixed
     */
    public function actionApprovedUser($id, $status = User::STATUS_DELETED, $type = null) {
        $user         = $this->findModel($id);
        $user->status = $status;
        $user->save();

        return $this->redirect(['view', 'id' => $user->id, 'type' => $type]);
    }

    /**
     * Change user password
     */
    public function actionChangePass($id, $type = null) {
        $user           = $this->findModel($id);
        $passwordChange = new PasswordChangeForm($user);
        $request        = \Yii::$app->request;

        if($request->isPost && $passwordChange->load($request->post())) {
            try {
                $passwordChange->changePassword();

                return $this->redirect(['view', 'id' => $user->id, 'type' => $type]);
            }catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }

        return $this->render('change-pass', [
            'user'           => $user,
            'passwordChange' => $passwordChange,
            'type'           => $type
        ]);
    }
}
