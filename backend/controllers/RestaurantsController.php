<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\base\Exception;
use common\lybrary\upload\Upload;
use common\models\User;
use common\models\Restaurants;
use common\models\Districts;
use common\models\RestaurantFeatures;
use common\models\RestaurantGallery;
use common\models\RestaurantKitchen;
use common\models\RestaurantMetro;
use common\models\RestaurantReasons;
use common\models\RestaurantTypes;
use common\models\UserProfile;
use backend\models\RestaurantsSearch;
use backend\models\CreateRestForm;
use backend\models\PasswordChangeForm;

/**
 * RestaurantsController implements the CRUD actions for Restaurants model.
 */
class RestaurantsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Restaurants models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new RestaurantsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Restaurants model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Restaurants model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model     = new Restaurants();
        $userModel = new User();
        $userForm  = new CreateRestForm();
        $request   = \Yii::$app->request;
        $post      = $request->post();

        if($request->isPost && $model->load($post) && $model->validate() && $userForm->load($post) && $user = $userForm->signup()) {
//        if($request->isPost) {
//            if($model->load($post = $request->post()) && $model->validate()) {
//                if($userForm->load($post) && $user = $userForm->signup()) {
                    $types = [];
                    $kitchens = [];
                    $features = [];
                    $metroData = [];
                    $reasonData = [];
                    $file = UploadedFile::getInstance($model, 'restaurant_avatar');
                    $upload = new Upload();
                    $model->user_id = $user->id;

                    if ($file) {
                        $upload->setFile($file)
                            ->setFileExtensions(['jpg', 'png', 'jpeg'])
                            ->setMaxSize(6)
                            ->setFileRename(true)
                            ->addThumbnails([['path' => 'upload/restaurant/avatars']])
                            ->setQuality(60)
                            ->setMinCompressSize(0)
                            ->upload();

                        if (!$upload->isErrorExists()) {
                            $model->restaurant_avatar = $upload->getFileName();

                            if ($oldImage = $model->oldAttributes['restaurant_avatar']) {
                                Upload::clearThumbnails('restaurant', $oldImage, ['avatars']);
                            }
                        }
                    } else {
                        $model->restaurant_avatar = $model->oldAttributes['restaurant_avatar'];
                    }

                    if ($model->save()) {
                        if ($post['types']) {
                            $i = 0;
                            foreach ($post['types'] as $type) {
                                $types[$i][] = $model->restaurant_id;
                                $types[$i][] = $type;
                                $i++;
                            }

                            RestaurantTypes::multiInsert($types, $model->restaurant_id);
                        }

                        if ($post['kitchens']) {
                            $i = 0;
                            foreach ($post['kitchens'] as $kitchen) {
                                $kitchens[$i][] = $model->restaurant_id;
                                $kitchens[$i][] = $kitchen;
                                $i++;
                            }

                            RestaurantKitchen::multiInsert($kitchens, $model->restaurant_id);
                        }

                        if ($post['features']) {
                            $i = 0;
                            foreach ($post['features'] as $feature) {
                                $features[$i][] = $model->restaurant_id;
                                $features[$i][] = $feature;
                                $i++;
                            }

                            RestaurantFeatures::multiInsert($features, $model->restaurant_id);
                        }

                        if ($post['metroId']) {
                            $i = 0;
                            foreach ($post['metroId'] as $metro) {
                                $metroData[$i][] = $model->restaurant_id;
                                $metroData[$i][] = $metro;
                                $i++;
                            }

                            RestaurantMetro::multiInsert($metroData, $model->restaurant_id);
                        }

                        if ($post['reasons']) {
                            $i = 0;
                            foreach ($post['reasons'] as $reason) {
                                $reasonData[$i][] = $model->restaurant_id;
                                $reasonData[$i][] = $reason;
                                $i++;
                            }

                            RestaurantReasons::multiInsert($reasonData, $model->restaurant_id);
                        }

                        return $this->redirect(['view', 'id' => $model->restaurant_id]);
                    }
//                }
//            }
        } else {
            return $this->render('create', [
                'model'    => $model,
                'userForm' => $userForm
            ]);
        }
    }

    /**
     * Updates an existing Restaurants model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model   = $this->findModel($id);
        $request = \Yii::$app->request;

        if ($request->isPost) {
            $post       = $request->post();
            $types      = [];
            $kitchens   = [];
            $features   = [];
            $metroData  = [];
            $reasonData = [];

            if($model->load($post) && $model->validate()) {
                $file   = UploadedFile::getInstance($model, 'restaurant_avatar');
                $upload = new Upload();

                if ($file) {
                    $upload->setFile($file)
                           ->setFileExtensions(['jpg', 'png', 'jpeg'])
                           ->setMaxSize(6)
                           ->setFileRename(true)
                           ->addThumbnails([['path' => 'upload/restaurant/avatars']])
                           ->setQuality(60)
                           ->setMinCompressSize(0)
                           ->upload();

                    if (!$upload->isErrorExists()) {
                        $model->restaurant_avatar = $upload->getFileName();

                        if ($oldImage = $model->oldAttributes['restaurant_avatar']) {
                            Upload::clearThumbnails('restaurant', $oldImage, ['avatars']);
                        }
                    }
                } else {
                    $model->restaurant_avatar = $model->oldAttributes['restaurant_avatar'];
                }

                if ($model->save()) {
                    if ($post['types']) {
                        $i = 0;
                        foreach ($post['types'] as $type) {
                            $types[$i][] = $model->restaurant_id;
                            $types[$i][] = $type;
                            $i++;
                        }

                        RestaurantTypes::multiInsert($types, $model->restaurant_id);
                    }

                    if($post['kitchens']) {
                        $i = 0;
                        foreach ($post['kitchens'] as $kitchen) {
                            $kitchens[$i][] = $model->restaurant_id;
                            $kitchens[$i][] = $kitchen;
                            $i++;
                        }

                        RestaurantKitchen::multiInsert($kitchens,  $model->restaurant_id);
                    }

                    if($post['features']) {
                        $i = 0;
                        foreach ($post['features'] as $feature) {
                            $features[$i][] = $model->restaurant_id;
                            $features[$i][] = $feature;
                            $i++;
                        }

                        RestaurantFeatures::multiInsert($features,  $model->restaurant_id);
                    }

                    if($post['metroId']) {
                        $i = 0;
                        foreach ($post['metroId'] as $metro) {
                            $metroData[$i][] = $model->restaurant_id;
                            $metroData[$i][] = $metro;
                            $i++;
                        }

                        RestaurantMetro::multiInsert($metroData,  $model->restaurant_id);
                    }

                    if($post['reasons']) {
                        $i = 0;
                        foreach ($post['reasons'] as $reason) {
                            $reasonData[$i][] = $model->restaurant_id;
                            $reasonData[$i][] = $reason;
                            $i++;
                        }

                        RestaurantReasons::multiInsert($reasonData,  $model->restaurant_id);
                    }
                }

                return $this->redirect(['view', 'id' => $model->restaurant_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Restaurants model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $rest = $this->findModel($id);
        $user = User::find()->where(['id' => $rest->user_id])->one();
        $user->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Restaurants model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Restaurants the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $model = Restaurants::find()->where(['restaurant_id' => $id])->with('user.userProfile', 'city', 'district')->one();

        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Get list districts
     * @return array
     */
    public function actionGetDistricts() {
        /** @var Districts $district */
        $request = Yii::$app->request;

        if($request->isAjax && $request->isPost) {
            $out       = [];
            $i         = 0;
            $post      = $request->post('depdrop_parents');
            $districts = Districts::find()->where(['city_id' => $post[$i]])->all();

            foreach($districts as $district) {
                $out[$i]['id']   = $district->district_id;
                $out[$i]['name'] = $district->district_name;
                $i++;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['output' => $out, 'selected'=>''];
        }
    }

    /**
     * Approved restaurant
     * @param int $id
     * @param int $status
     * @return mixed
     */
    public function actionApprovedRest($id, $status = Restaurants::STATUS_INACTIVE) {
        $rest                    = Restaurants::findOne($id);
        $rest->restaurant_status = $status;

        $rest->save(false);

        return $this->redirect(['view', 'id' => $rest->restaurant_id]);
    }

    /**
     * Lists restaurants gallery.
     * @param int $id
     * @return mixed
     */
    public function actionGallery($id)
    {
        $galleryModel  = new RestaurantGallery();
        $images        = $galleryModel->findImages($id);

        return $this->render('gallery', [
            'images'       => $images,
            'galleryModel' => $galleryModel,
            'id'           => $id
        ]);
    }

    public function actionUploadGallery($id)
    {
        $request      = \Yii::$app->request;
        $galleryModel = new RestaurantGallery();

        if($request->isPost) {
            $file   = UploadedFile::getInstance($galleryModel, 'gallery_image');
            $upload = new Upload();

            if($file) {
                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->addThumbnails([
                        [
                            'path'   => '/upload/restaurant/gallery/preview',
                            'width'  => 190,
                            'height' => 97,
                            'type'   => 'crop'
                        ],
                        [
                            'path'   => '/upload/restaurant/gallery/big',
                            'width'  => 800,
                            'height' => 600,
                            'type'   => 'adaptive'
                        ]
                    ])
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $galleryModel->gallery_image = $upload->getFileName();
                    $galleryModel->restaurant_id = $id;
                    $galleryModel->save();

                    return $this->redirect(['gallery', 'id' => $id]);
                }
            }
        } else {
            return $this->render('upload-gallery', [
                'galleryModel' => $galleryModel,
                'id'           => $id
            ]);
        }
    }

    /**
     * Change rest password
     * @param int $userId
     * @param int $restId
     * @return mixed
     */
    public function actionChangePass($userId, $restId) {
        $user           = User::findOne($userId);;
        $passwordChange = new PasswordChangeForm($user);
        $request        = \Yii::$app->request;

        if($request->isPost && $passwordChange->load($request->post())) {
            try {
                $passwordChange->changePassword();

                return $this->redirect(['view', 'id' => $restId]);
            }catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }

        return $this->render('change-pass', [
            'user'           => $user,
            'restId'         => $restId,
            'passwordChange' => $passwordChange
        ]);
    }
}
