<?php

namespace backend\controllers;

use Yii;
use common\lybrary\upload\Upload;
use common\models\News;
use common\models\User;
use backend\models\NewsSearch;
use backend\controllers\BaseController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        /** @var News $modelNews */
        $modelNews = new News();
        $request   = \Yii::$app->request;

        if ($request->isPost && $modelNews->load($request->post())) {
            $modelNews->user_id = $this->getAuthId();
            $file               = UploadedFile::getInstance($modelNews, 'news_image');
            $upload             = new Upload();

            if($file) {
                $upload->setFile($file)
                       ->setFileExtensions(['jpg', 'png', 'jpeg'])
                       ->setMaxSize(6)
                       ->setFileRename(true)
                       ->setFileCompress(true)
                       ->addThumbnails([['path' => 'upload/news']])
                       ->setQuality(60)
                       ->setMinCompressSize(0)
                       ->upload();

                if (!$upload->isErrorExists()) {
                    $modelNews->news_image = $upload->getFileName();
                }
            }

            $modelNews->save();

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $modelNews,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $request = Yii::$app->request;

        if($request->isPost) {
            $post = $request->post();

            if(empty($post['News']['news_image']) && $model->news_image) {
                $post['News']['news_image'] = $model->news_image;
            }

            if($model->load($post)) {
                $file   = UploadedFile::getInstance($model, 'news_image');
                $upload = new Upload();

                if($file) {
                    if($oldImage = $model->oldAttributes['news_image']) {
                        Upload::clearThumbnails('news', $oldImage);
                    }

                    $upload->setFile($file)
                           ->setFileExtensions(['jpg', 'png', 'jpeg'])
                           ->setMaxSize(6)
                           ->setFileRename(true)
                           ->setFileCompress(true)
                           ->addThumbnails([['path' => 'upload/news']])
                           ->setQuality(60)
                           ->setMinCompressSize(0)
                           ->upload();

                    if (!$upload->isErrorExists()) {
                        $model->news_image = $upload->getFileName();
                    }
                }

                $model->save();

                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if(!empty($model->news_image)) {
            Upload::clearThumbnails('news', $model->news_image);
        }

        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $model = News::find()->where(['news_id' => $id])->with('user.userProfile')->one();

        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
