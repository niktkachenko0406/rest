<?php

namespace backend\controllers;

use Yii;
use common\models\Partners;
use backend\models\PartnersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\lybrary\upload\Upload;
use common\models\User;
use backend\controllers\BaseController;
use yii\web\UploadedFile;
use yii\db\Expression;

/**
 * PartnersController implements the CRUD actions for Partners model.
 */
class PartnersController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Partners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartnersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partners model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Partners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var Partners $modelPartners */
        $modelPartners = new Partners();
        $request       = \Yii::$app->request;

        if($request->isPost && $modelPartners->load($request->post())) {
            $modelPartners->user_id = $this->getAuthId();
            $file                   = UploadedFile::getInstance($modelPartners, 'partner_image');
            $upload                 = new Upload();

            if($file) {
                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->setFileCompress(true)
                    ->addThumbnails([['path' => 'upload/partners']])
                    ->setQuality(60)
                    ->setMinCompressSize(0)
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $modelPartners->partner_image = $upload->getFileName();
                }
            }

            $modelPartners->save();

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $modelPartners,
            ]);
        }
    }

    /**
     * Updates an existing Partners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model   = $this->findModel($id);
        $request = Yii::$app->request;

        if($request->isPost) {
            $post = $request->post();

            if(empty($post['Partners']['partner_image']) && $model->partner_image) {
                $post['Partners']['partner_image'] = $model->partner_image;
            }

            if($model->load($post)) {
                $file   = UploadedFile::getInstance($model, 'partner_image');
                $upload = new Upload();

                if($file) {
                    $upload->setFile($file)
                        ->setFileExtensions(['jpg', 'png', 'jpeg'])
                        ->setMaxSize(6)
                        ->setFileRename(true)
                        ->setFileCompress(true)
                        ->addThumbnails([['path' => 'upload/partners']])
                        ->setQuality(60)
                        ->setMinCompressSize(0)
                        ->upload();

                    if (!$upload->isErrorExists()) {
                        $model->partner_image = $upload->getFileName();

                        if($oldImage = $model->oldAttributes['partner_image']) {
                            Upload::clearThumbnails('partners', $oldImage);
                        }
                    }
                }

                $model->save();

                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Partners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if(!empty($model->partner_image)) {
            Upload::clearThumbnails('partners', $model->partner_image);
        }

        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Partners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Partners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Partners::find()->where(['partner_id' => $id])->with('user')->one();

        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
