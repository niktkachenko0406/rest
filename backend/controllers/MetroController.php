<?php

namespace backend\controllers;

use Yii;
use common\models\Metro;
use common\models\Cities;
use common\models\Districts;
use backend\models\MetroSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * MetroController implements the CRUD actions for Metro model.
 */
class MetroController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Metro models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new MetroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Metro model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Metro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model  = new Metro();
        $cities = Cities::getCities();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model'     => $model,
                'cities'    => $cities
            ]);
        }
    }

    /**
     * Updates an existing Metro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model  = $this->findModel($id);
        $cities = Cities::getCities();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->station_id]);
        } else {
            return $this->render('update', [
                'model'  => $model,
                'cities' => $cities
            ]);
        }
    }

    /**
     * Deletes an existing Metro model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Metro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Metro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Metro::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Get districts by city id
     * @return array
     */
    public function actionGetDistricts()
    {
        $request = \Yii::$app->request;
        $data    = [];

        if($request->isAjax && $request->isPost) {
            $post      = $request->post();
            $districts = Districts::getDistricts($post['depdrop_parents'][0]);
            $i         = 0;

            foreach($districts as $district) {
                $data[$i]['id']   = $district->district_id;
                $data[$i]['name'] = $district->district_name;
                $i++;
            }

            \Yii::$app->response->format = Response::FORMAT_JSON;

            return ['output' => $data, 'selected' => ''];
        }
    }
}
