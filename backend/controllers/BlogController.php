<?php

namespace backend\controllers;

use common\models\Restaurants;
use Yii;
use common\models\Blog;
use backend\models\BlogSearch as BlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\lybrary\upload\Upload;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blog models.
     * @param int $restId
     * @return mixed
     */
    public function actionIndex($restId)
    {
        $searchModel  = new BlogSearch();
        $rest         = Restaurants::findOne($restId);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'rest'         => $rest
        ]);
    }

    /**
     * Displays a single Blog model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $modelBlog = new Blog();
        $request   = \Yii::$app->request;

        if ($request->isPost && $modelBlog->load($request->post())) {
            $file   = UploadedFile::getInstance($modelBlog, 'blog_image');
            $upload = new Upload();

            if($file) {
                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->addThumbnails([['path' => 'upload/blog']])
                    ->setQuality(60)
                    ->setMinCompressSize(0)
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $modelBlog->blog_image = $upload->getFileName();
                }
            }
            $modelBlog->restaurant_id = $id;
            $modelBlog->save();

            return $this->redirect(['view', 'id' => $modelBlog->blog_id]);
        } else {
            return $this->render('create', [
                'model' => $modelBlog,
            ]);
        }
    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $blog    = $this->findModel($id);
        $request = \Yii::$app->request;

        if ($request->isPost && $blog->load($request->post())) {
            $file   = UploadedFile::getInstance($blog, 'blog_image');
            $upload = new Upload();

            if($file) {
                $upload->setFile($file)
                       ->setFileExtensions(['jpg', 'png', 'jpeg'])
                       ->setMaxSize(6)
                       ->setFileRename(true)
                       ->addThumbnails([['path' => 'upload/blog']])
                       ->setQuality(60)
                       ->setMinCompressSize(0)
                       ->upload();

                if (!$upload->isErrorExists()) {
                    $blog->blog_image = $upload->getFileName();

                    if($oldImage = $blog->oldAttributes['blog_image']) {
                        Upload::clearThumbnails('blog', $oldImage);
                    }
                }
            } else {
                $blog->blog_image = $blog->oldAttributes['blog_image'];
            }

            $blog->save();

            return $this->redirect(['view', 'id' => $blog->blog_id]);
        } else {
            return $this->render('update', [
                'model' => $blog,
            ]);
        }
    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Approved post
     * @param int $id
     * @return mixed
     */
    public function actionApprovedPost($id)
    {
        $blog              = $this->findModel($id);
        $blog->blog_status = Blog::POST_STATUS_ACTIVE;
        $blog->save();

        return $this->redirect(['index', 'restId' => $blog->restaurant_id]);
    }
}
