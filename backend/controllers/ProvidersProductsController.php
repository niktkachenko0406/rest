<?php

namespace backend\controllers;

use Yii;
use common\models\ProvidersProducts;
use backend\models\ProvidersProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\lybrary\upload\Upload;

/**
 * ProvidersProductsController implements the CRUD actions for ProvidersProducts model.
 */
class ProvidersProductsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProvidersProducts models.
     * @param int $providerId
     * @return mixed
     */
    public function actionIndex($providerId)
    {
        $searchModel  = new ProvidersProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'providerId'   => $providerId
        ]);
    }

    /**
     * Displays a single ProvidersProducts model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProvidersProducts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($providerId)
    {
        $model   = new ProvidersProducts();
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post())) {
            $file   = UploadedFile::getInstance($model, 'product_image');
            $upload = new Upload();

            if($file) {
                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->addThumbnails([[
                        'path'   => 'upload/provider/product',
                        'width'  => 170,
                        'height' => 100,
                        'type'   => 'crop'
                    ]])
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $model->product_image = $upload->getFileName();
                }
            }

            $model->provider_id = $providerId;
            $model->save();

            return $this->redirect(['index', 'providerId' => $model->provider_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProvidersProducts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model   = $this->findModel($id);
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post())) {
            $file   = UploadedFile::getInstance($model, 'product_image');
            $upload = new Upload();

            if($file) {
                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->addThumbnails([[
                        'path'   => 'upload/provider/product',
                        'width'  => 170,
                        'height' => 100,
                        'type'   => 'crop'
                    ]])
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $model->product_image = $upload->getFileName();

                    if($oldImage = $model->oldAttributes['product_image']) {
                        Upload::clearThumbnails('provider', $oldImage, ['product']);
                    }
                }
            }

            $model->save();

            return $this->redirect(['index', 'providerId' => $model->provider_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProvidersProducts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $product    = $this->findModel($id);
        $providerId = $product->provider_id;

        $product->delete();

        return $this->redirect(['index', 'providerId' => $providerId]);
    }

    /**
     * Finds the ProvidersProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ProvidersProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProvidersProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
