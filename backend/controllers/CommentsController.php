<?php

namespace backend\controllers;

use common\models\Blog;
use common\models\User;
use Yii;
use common\models\Comments;
use common\models\Restaurants;
use common\models\UserProfile;
use common\models\News;
use backend\models\CommentsSearch;
use backend\models\CommentsUserSearch;
use backend\models\CommentsNewsSearch;
use backend\models\CommentsBlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommentsController implements the CRUD actions for Comments model.
 */
class CommentsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Comments models.
     * @param int $restId
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel  = new CommentsSearch();
        $rest         = Restaurants::findOne($id);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'rest'         => $rest
        ]);
    }

    /**
     * Lists all Comments models.
     * @param int $id
     * @return mixed
     */
    public function actionUserComments($id, $type = null)
    {
        $searchModel  = new CommentsUserSearch();
        $user         = User::find()->where(['id' => $id])->joinWith('userProfile')->one();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('user-comments', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'user'         => $user,
            'type'         => $type
        ]);
    }

    /**
     * Lists all Comments models.
     * @param int $id
     * @return mixed
     */
    public function actionNewsComments($id)
    {
        $searchModel  = new CommentsNewsSearch();
        $news         = News::findOne($id);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('news-comments', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'news'         => $news
        ]);
    }

    /**
     * Lists all Comments models.
     * @param int $id
     * @return mixed
     */
    public function actionBlogComments($id)
    {
        $searchModel  = new CommentsBlogSearch();
        $blog         = Blog::findOne($id);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('blog-comments', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'blog'         => $blog
        ]);
    }

    /**
     * Displays a single Comments model.
     * @param string $id
     * @param bool $userView
     * @return mixed
     */
    public function actionView($id, $userView = false, $type = null)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model'    => $model,
            'userView' => $userView,
            'type'     => $type
        ]);
    }

    /**
     * Creates a new Comments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param int $entity_id
     * @param int $comment_entity
     * @return mixed
     */
    public function actionCreate($entity_id, $entity)
    {
        $model = new Comments();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id        = $this->getAuthId();
            $model->entity_id      = $entity_id;
            $model->comment_entity = $entity;

            $model->save();

            if($model->comment_entity == Comments::ENTITY_REST) {
                $this->actionChangeRating($model->entity_id);
            }

            return $this->redirect(['view', 'id' => $model->comment_id]);
        } else {
            return $this->render('create', [
                'model'  => $model,
                'entity' => $entity
            ]);
        }
    }

    /**
     * Updates an existing Comments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id, $userView = false, $type = null)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->comment_entity == Comments::ENTITY_REST) {
                $this->actionChangeRating($model->entity_id);
            }

            return $this->redirect(['view', 'id' => $model->comment_id, 'userView' => $userView, 'type' => $type]);
        } else {
            return $this->render('update', [
                'model'    => $model,
                'userView' => $userView,
                'type'     => $type,
                'entity'   => $model->comment_entity
            ]);
        }
    }

    /**
     * Deletes an existing Comments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id, $userView = false, $type = null)
    {
        $comment  = $this->findModel($id);
        $userId   = $comment->user_id;
        $entityId = $comment->entity_id;
        $entity   = $comment->comment_entity;
        $status   = $comment->comment_status;
        $action   = Comments::getAction($entity);

        $comment->delete();

        if($entity == Comments::ENTITY_REST && $status) {
            $this->actionChangeRating($entityId);
        }

        if($userView) {
            return $this->redirect(['user-comments', 'id' => $userId, 'type' => $type]);
        } else {
            return $this->redirect([$action, 'id' => $entityId]);
        }
    }

    /**
     * Finds the Comments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Comments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Approved comment
     * @param int $id
     * @param bool $list
     * @return mixed
     */
    public function actionApprovedComment($id, $list = false, $userView = false, $type = null)
    {
        $comment                 = $this->findModel($id);
        $comment->comment_status = Comments::COMMENT_ACTIVE;
        $comment->save();

        if($comment->comment_entity == Comments::ENTITY_REST) {
            $this->actionChangeRating($comment->entity_id);
        }

        if(!$list && !$userView) {
            return $this->redirect(['view', 'id' => $comment->comment_id]);
        } elseif($list && !$userView) {
            return $this->redirect([Comments::getAction($comment->comment_entity), 'id' => $comment->entity_id]);
        } elseif($list && $userView) {
            return $this->redirect(['user-comments', 'id' => $comment->user_id, 'type' => $type]);
        } elseif(!$list && $userView) {
            return $this->redirect(['view', 'id' => $comment->comment_id, 'userView' => true, 'type' => $type]);
        }
    }

    public function actionChangeRating($restId) {
        $rest = Restaurants::find()->where(['restaurant_id' => $restId])->one();
        $rest->restaurant_rating = $rest->getRating();

        $rest->save(false);
    }
}
