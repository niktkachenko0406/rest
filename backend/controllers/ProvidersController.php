<?php

namespace backend\controllers;

use Yii;
use common\models\Providers;
use common\models\ProvidersServices;
use backend\models\ProvidersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\lybrary\upload\Upload;

/**
 * ProvidersController implements the CRUD actions for Providers model.
 */
class ProvidersController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Providers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProvidersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Providers model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $servicesModel = new ProvidersServices;
        $services      = $servicesModel->getProviderServices($id);
        $servicesList  = [];

        foreach ($services as $service) {
            $servicesList[$service['subcategory']['category_id']][0]   = $service['subcategory']['category']['category_name'];
            $servicesList[$service['subcategory']['category_id']][1][] = $service['subcategory']['subcategory_name'];
        }

        return $this->render('view', [
            'model'    => $this->findModel($id),
            'services' => $servicesList
        ]);
    }

    /**
     * Creates a new Providers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model   = new Providers();
        $request = \Yii::$app->request;
        $post    = $request->post();

        if ($request->isPost && $model->load($post) && $model->validate()) {
            $file   = UploadedFile::getInstance($model, 'provider_avatar');
            $upload = new Upload();

            if ($file) {
                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->addThumbnails([[
                        'path'   => 'upload/provider/avatars',
                        'width'  => 270,
                        'height' => 185,
                        'type'   => 'adaptive'
                    ]])
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $model->provider_avatar = $upload->getFileName();

                    if ($oldImage = $model->oldAttributes['provider_avatar']) {
                        Upload::clearThumbnails('provider', $oldImage, ['avatars']);
                    }
                }
            } else {
                $model->provider_avatar = $model->oldAttributes['provider_avatar'];
            }

            if ($model->save()) {
                if ($post['providers_service']) {
                    $i             = 0;
                    $subCategories = [];

                    foreach ($post['providers_service'] as $subCategory) {
                        $subCategories[$i][] = $subCategory;
                        $subCategories[$i][] = $model->provider_id;
                        $i++;
                    }

                    ProvidersServices::multiInsert($subCategories, $model->provider_id);
                }
            }

            return $this->redirect(['view', 'id' => $model->provider_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Providers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model         = $this->findModel($id);
        $servicesModel = new ProvidersServices;
        $services      = $servicesModel->getProviderServices($id);
        $request       = \Yii::$app->request;
        $post          = $request->post();
        $servicesList  = [];

        foreach ($services as $subCategory) {
            $servicesList[$subCategory['subcategory_id']] = $subCategory['subcategory_id'];
        }

        if ($request->isPost && $model->load($post) && $model->validate()) {
            $file   = UploadedFile::getInstance($model, 'provider_avatar');
            $upload = new Upload();

            if ($file) {
                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->addThumbnails([[
                        'path'   => 'upload/provider/avatars',
                        'width'  => 270,
                        'height' => 185,
                        'type'   => 'adaptive'
                    ]])
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $model->provider_avatar = $upload->getFileName();

                    if ($oldImage = $model->oldAttributes['provider_avatar']) {
                        Upload::clearThumbnails('provider', $oldImage, ['avatars']);
                    }
                }
            } else {
                $model->provider_avatar = $model->oldAttributes['provider_avatar'];
            }

            if ($model->save()) {
                if ($post['providers_service']) {
                    $i             = 0;
                    $subCategories = [];

                    foreach ($post['providers_service'] as $subCategory) {
                        $subCategories[$i][] = $subCategory;
                        $subCategories[$i][] = $model->provider_id;
                        $i++;
                    }

                    ProvidersServices::multiInsert($subCategories, $model->provider_id);
                }
            }

            return $this->redirect(['view', 'id' => $model->provider_id]);
        } else {
            return $this->render('update', [
                'model'        => $model,
                'servicesList' => $servicesList
            ]);
        }
    }

    /**
     * Deletes an existing Providers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Providers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Providers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Providers::find()->where(['provider_id' => $id])->with('city', 'district')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
