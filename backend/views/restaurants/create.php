<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Restaurants */
/* @var $userForm backend\models\CreateRestForm */

$this->title                   = 'Добавить ресторан';
$this->params['breadcrumbs'][] = ['label' => 'Рестораны', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurants-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model'    => $model,
        'userForm' => $userForm
    ]) ?>
</div>
