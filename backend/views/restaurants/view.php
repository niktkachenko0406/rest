<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Blog;
use common\models\Comments;
use common\models\Restaurants;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Restaurants */

$this->title = $model->restaurant_name ?: 'Не указано';
$this->params['breadcrumbs'][] = ['label' => 'Рестораны', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurants-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['index'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->restaurant_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Сменить пароль', ['change-pass', 'userId' => $model->user_id, 'restId' => $model->restaurant_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->restaurant_id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Вы уверены, что хотите удалить этот ресторан?',
                'method'  => 'post',
            ],
        ]) ?>
        <?= $model->restaurant_status
            ? Html::a('Скрыть ресторан', ['approved-rest', 'id' => $model->restaurant_id], ['class' => 'btn btn-warning'])
            : Html::a('Сделать активным', ['approved-rest', 'id' => $model->restaurant_id, 'status' => $model::STATUS_ACTIVE], ['class' => 'btn btn-primary']) ?>

    </p>
    <p>
        <?= Html::a('Список постов', ['blog/index', 'restId' => $model->restaurant_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Список комментариев', ['comments/index', 'id' => $model->restaurant_id, 'entity' => Comments::ENTITY_REST], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Галерея', ['gallery', 'id' => $model->restaurant_id], ['class' => 'btn btn-warning']) ?>
    </p>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'label' => 'Статус ',
                'value' => Restaurants::getStatus($model->restaurant_status)
            ],
            [
                'label'  => 'Не одобренные посты',
                'format' => 'html',
                'value'  => Blog::isApproved($model->restaurant_id)
            ],
            [
                'label'  => 'Не одобренные комментарии',
                'format' => 'html',
                'value'  =>  Comments::isApproved(Comments::ENTITY_REST, $model->restaurant_id)
            ],
            [
                'label'  => 'Новые отзывы в блоге',
                'format' => 'html',
                'value'  => Comments::isApprovedBlogComment(Comments::ENTITY_BLOG, $model->restaurant_id)
            ],
            [
                'label'  => 'Рейтинг',
                'format' => 'html',
                'value'  => function($model) {
                    return $model->getRating();
                }
            ],
            [
                'attribute' => 'restaurant_avatar',
                'label'     => 'Изображение',
                'format'    => 'html',
                'value'     => function ($model) {
                    return Html::img($model->getAvatar('admin'), ['width' => '368px', 'style' => 'max-width:100%']);
                }
            ],
            [
                'label' => 'Email',
                'value' => $model->user->email
            ],
            [
                'label' => 'Телефон',
                'value' => $model->restaurant_phone ?: 'Не указан'
            ],
            'restaurant_short_desc',
            [
                'label'  => 'Тип заведения',
                'format' => 'html',
                'value'  => function ($model) {
                    return implode(',', ArrayHelper::map($model->types, 'type_id', 'type_name'));
                }
            ],
            [
                'label'  => 'Подробное описание',
                'format' => 'html',
                'value'  => function ($model) {
                    return $model->restaurant_desc;
                }
            ],
            'restaurant_address',
            [
                'label'  => 'Тип кухни',
                'format' => 'html',
                'value'  => function ($model) {
                    return implode(',', ArrayHelper::map($model->kitchen, 'kitchen_id', 'kitchen_name'));
                }
            ],
            [
                'label'  => 'Особенности',
                'format' => 'html',
                'value'  => function ($model) {
                    return implode(',', ArrayHelper::map($model->features, 'feature_id', 'feature_name'));
                }
            ],
            [
                'label'  => 'Повод',
                'format' => 'html',
                'value'  => function ($model) {
                    return implode(',', ArrayHelper::map($model->reasons, 'reason_id', 'reason_name'));
                }
            ],
            [
                'label' => 'Город',
                'value' => $model->city->city_name ?: 'Город не указан'
            ],
            [
                'label' => 'Район',
                'value' => $model->district->district_name ?: 'Район не указан'
            ],
            [
                'label'  => 'Режим работы',
                'format' => 'html',
                'value'  => function ($model) {
                    if(is_array($model->restaurant_schedule)) {
                        $schedule = '<ul>';

                        foreach($model->restaurant_schedule as $day => $time) {
                            if(is_array($model->restaurant_days) && in_array($day, $model->restaurant_days)) {
                                !empty($time['from']) ? $time['from'] : 'не указано';
                                $time['to'] ?: 'не указано';
                                $schedule = $schedule . '<li>' . Restaurants::getDay($day) . ' от: ' . $time['from'] . ' до: ' . $time['to'] . '</li>';
                            }
                        }

                        $schedule = $schedule . '</ul>';

                        return $schedule;
                    }
                }
            ],
        ],
    ]) ?>
</div>
