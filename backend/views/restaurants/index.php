<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Blog;
use common\models\Comments;
use common\models\Restaurants;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RestaurantsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Рестораны';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurants-index">
    <h1><?=Html::encode($this->title) ?></h1>
    <?= Html::a('Создать ресторан', ['create'], ['class' => 'btn btn-primary']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'user_email',
                'label'     => 'Email',
                'format'    => 'text',
                'content'   => function($model) {
                    if($model->user) {
                        return $model->user->email;
                    } else {
                        return 'Не указан';
                    }
                }
            ],
            [
                'attribute' => 'restaurant_name',
                'label'     => 'Название',
                'format'    => 'html',
                'content'   => function($model) {
                    return Html::a($model->restaurant_name, ['view', 'id' => $model->restaurant_id]);
                }
            ],
            [
                'attribute' => 'restaurant_phone',
                'label'     => 'Телефон',
                'format'    => 'html',
                'content'   => function($model) {
                    return $model->restaurant_phone ?: 'Не указан';
                }
            ],
            [
                'attribute' => 'city_id',
                'label'     => 'Город',
                'format'    => 'text',
                'content'   => function($model) {
                    if($model->city) {
                        return $model->city->city_name;
                    } else {
                        return 'Не указан';
                    }
                }
            ],
            [
                'label'     => 'Новые посты',
                'format'    => 'html',
                'content'   => function($model) {
                    return Blog::isApproved($model->restaurant_id);
                }
            ],
            [
                'label'     => 'Новые отзывы',
                'format'    => 'html',
                'content'   => function($model) {
                    return Comments::isApproved(Comments::ENTITY_REST, $model->restaurant_id);
                }
            ],
            [
                'attribute' => 'restaurant_status',
                'label'     => 'Статус',
                'format'    => 'html',
                'content'   => function($model) {
                    return Restaurants::getStatus($model->restaurant_status);
                }
            ],
            [
                'label'     => 'Новые отзывы в блоге',
                'format'    => 'html',
                'content'   => function($model) {
                    return Comments::isApprovedBlogComment(Comments::ENTITY_BLOG, $model->restaurant_id);
                }
            ],
            'restaurant_rating',
            ['class' => 'yii\grid\ActionColumn', 'header'=>'Действия'],
        ],
    ]); ?>
</div>
