<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RestaurantsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="restaurants-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'restaurant_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'restaurant_name') ?>

    <?= $form->field($model, 'restaurant_short_desc') ?>

    <?= $form->field($model, 'restaurant_desc') ?>

    <?php // echo $form->field($model, 'restaurant_average_check') ?>

    <?php // echo $form->field($model, 'restaurant_schedule') ?>

    <?php // echo $form->field($model, 'restaurant_address') ?>

    <?php // echo $form->field($model, 'restaurant_avatar') ?>

    <?php // echo $form->field($model, 'city_id') ?>

    <?php // echo $form->field($model, 'district_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
