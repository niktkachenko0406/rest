<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $galleryModel common\models\RestaurantGallery */

$this->title = 'Добавить изображение';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['gallery', 'id' => $id], ['class' => 'btn btn-success']) ?>
    </p>
    <?php $form = ActiveForm::begin([
        'id'      => 'galleryForm',
        'action'  => '',
        'method'  => 'post',
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>
    <?= $form->field($galleryModel, 'gallery_image')->widget(FileInput::className(), [
        'pluginOptions' => [
            'showPreview' => true,
            'showRemove'  => false,
            'showUpload'  => false,
            'browseLabel' => 'Выбрать изображение',
        ]
    ])?>
    <div class="form-group">
        <?= Html::submitButton('Загрузить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
