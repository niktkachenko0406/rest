<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $galleryModel common\models\RestaurantGallery */

$this->title                   = 'Галерея ресторана';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurants-index">
    <h1><?=Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['view', 'id' => $id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить изображение', ['upload-gallery', 'id' => $id], ['class' => 'btn btn-primary']) ?>
    </p>
    <br>
    <?php if(!empty($images)): ?>
        <?php foreach ($images as $image): ?>
            <div>
                <?= Html::img($galleryModel->getGalleryImage($image['gallery_image']), ['class' => 'btn btn-primary']) ?>
            </div>
            <br>
        <?php endforeach; ?>
    <?php else: ?>
        <p>Изображения отсутствуют</p>
    <?php endif; ?>
</div>
