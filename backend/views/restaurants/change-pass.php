<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title                   = 'Изменение пароля ресторану: ' . $user->email;
$this->params['breadcrumbs'][] = 'Изменение пароля';
?>
<div class="restaurants-update">
    <h1><?=Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['view', 'id' => $restId], ['class' => 'btn btn-success']) ?>
    </p>
        <?php $form = ActiveForm::begin([
            'id'      => 'profileForm',
            'action'  => '',
            'method'  => 'post',
            'options' => ['enctype'=>'multipart/form-data']
        ]); ?>
        <?= $form->field($passwordChange, 'newPassword')->passwordInput(['maxlength' => true]) ?>
        <?= $form->field($passwordChange, 'newPasswordRepeat')->passwordInput(['maxlength' => true]) ?>
        <button type="submit" class="animate btn btn-light btn-lg submit-btn">Сохранить</button>
    <?php ActiveForm::end(); ?>
</div>
