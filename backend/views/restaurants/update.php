<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Restaurants */

$this->title                   = 'Редактировать ресторан: ' . $model->restaurant_name;
$this->params['breadcrumbs'][] = ['label' => 'Рестораны', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->restaurant_name, 'url' => ['view', 'id' => $model->restaurant_id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="restaurants-update">
    <h1><?=Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['view', 'id' => $model->restaurant_id], ['class' => 'btn btn-success']) ?>
    </p>
    <?=$this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
