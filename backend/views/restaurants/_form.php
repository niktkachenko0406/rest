<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Cities;
use common\models\Districts;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use kartik\select2\Select2;
use vova07\imperavi\Widget;
use common\models\Features;
use common\models\Kitchens;
use common\models\Metro;
use common\models\Reasons;
use common\models\RestaurantGallery;
use common\models\Restaurants;
use common\models\Types;


/* @var $this yii\web\View */
/* @var $model common\models\Restaurants */
/* @var $form yii\widgets\ActiveForm */
/* @var $userForm backend\models\CreateRestForm */
?>

<div class="restaurants-form">

    <?php $form = ActiveForm::begin([
        'id'      => 'restaurantsForm',
        'action'  => '',
        'method'  => 'post',
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <div class="form-group">
        <?php if ($model->isNewRecord) { ?>
            <?= $form->field($userForm, 'email')->textInput(['maxlength' => true]) ?>
            <?= $form->field($userForm, 'password')->passwordInput(['maxlength' => true]) ?>
        <?php } ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'restaurant_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'restaurant_phone')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'restaurant_short_desc')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="form-group">
        <?= $form->field($model, 'restaurant_desc')->widget(Widget::className(), [
            'settings' => [
                'lang'      => 'ru',
                'minHeight' => 200,
                'plugins'   => [
                    'clips',
                    'fullscreen'
                ]
            ]
        ]); ?>
    </div>

    <div class="form-group">
        <label>Тип заведения</label>
        <?= Select2::widget([
            'name'          => 'types[]',
            'value'         => ArrayHelper::map($model->types, 'type_id', 'type_id'),
            'data'          => ArrayHelper::map(Types::getTypes(), 'type_id', 'type_name'),
            'size'          => Select2::LARGE,
            'maintainOrder' => true,
            'options' => [
                'placeholder' => 'Выберите тип',
                'multiple'    => true,
                'class'       => 'chosen-select'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <label>Кухня</label>
        <?= Select2::widget([
            'name'          => 'kitchens[]',
            'value'         => ArrayHelper::map($model->kitchen, 'kitchen_id', 'kitchen_id'),
            'data'          => ArrayHelper::map(Kitchens::getKitchens(), 'kitchen_id', 'kitchen_name'),
            'size'          => Select2::LARGE,
            'maintainOrder' => true,
            'options' => [
                'placeholder' => 'Выберите кухню',
                'multiple' => true,
                'class' => 'chosen-select'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <label>Особенности</label>
        <?= Select2::widget([
            'name'          => 'features[]',
            'value'         => ArrayHelper::map($model->features, 'feature_id', 'feature_id'),
            'data'          => ArrayHelper::map(Features::getFeatures(), 'feature_id', 'feature_name'),
            'size'          => Select2::LARGE,
            'maintainOrder' => true,
            'options' => [
                'placeholder' => 'Выберите особенности',
                'multiple'    => true,
                'class'       => 'chosen-select'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <label>Повод</label>
        <?= Select2::widget([
            'name'          => 'reasons[]',
            'value'         => ArrayHelper::map($model->reasons, 'reason_id', 'reason_id'),
            'data'          => ArrayHelper::map(Reasons::getReasons(), 'reason_id', 'reason_name'),
            'size'          => Select2::LARGE,
            'maintainOrder' => true,
            'options' => [
                'placeholder' => 'Выберите повод',
                'multiple' => true,
                'class' => 'chosen-select'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'restaurant_average_check')->textInput() ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'restaurant_address')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="form-group">
        <?php if (!$model->isNewRecord && !empty($model->restaurant_avatar)) { ?>
            <?= $form->field($model, 'restaurant_avatar')->widget(FileInput::className(), [
                'pluginOptions' => [
                    'initialPreview' => [
                        Html::img(Yii::$app->params['uploadUrl'] . '/restaurant/avatars/' . $model->restaurant_avatar, ['class'=>'file-preview-image']),
                    ],
                    'showPreview' => true,
                    'showRemove'  => false,
                    'showUpload'  => false,
                    'browseLabel' => 'Выбрать изображение',
                ],
            ])?>
        <?php } else { ?>
            <?= $form->field($model, 'restaurant_avatar')->widget(FileInput::className(), [
                'pluginOptions' => [
                    'showPreview' => true,
                    'showRemove'  => false,
                    'showUpload'  => false,
                    'browseLabel' => 'Выбрать изображение',
                ]
            ])?>
        <?php } ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'city_id')->dropDownList(
            ArrayHelper::map(
                Cities::find()->all(),
                'city_id',
                'city_name'
            ), [
                'prompt' => 'Город',
                'id'     => 'city-id'
            ]
        )?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'district_id')->widget(DepDrop::class, [
            'options'       => ['id'=>'district-id'],
            'data'          => (isset($model->city_id)) ? ArrayHelper::map(Districts::find()->where(['city_id' => $model->city_id])->all(), 'district_id', 'district_name') : [],
            'pluginOptions' => [
                'depends'     => ['city-id'],
                'placeholder' => 'Район',
                'loading'     => true,
                'url'         => Url::to(['/restaurants/get-districts']),
            ]
        ]);?>
    </div>

<!--    <div class="form-group">-->
<!--        <label>Метро</label>-->
<!--        --><?//= Html::dropDownList('metroId[]',
//            $model->getSelectedMetro(),
//            Metro::getMetroByDistrict($model->district_id),
//            [
//                'multiple' => 'multiple',
//                'class'    => 'js-rest-metro'
//            ]
//        ); ?>
<!--    </div>-->

    <input type="checkbox"
           name="Restaurants[restaurant_days][]"
           value="monday"
           <?php if(is_array($model->restaurant_days) && in_array('monday', $model->restaurant_days)): ?>checked<?php endif; ?> />
    <label class="control-label">Понедельник</label>
    <?= $form->field($model, 'restaurant_schedule[monday][from]')->textInput()->label(false); ?>
    <?= $form->field($model, 'restaurant_schedule[monday][to]')->textInput()->label(false); ?>

    <input type="checkbox"
           name="Restaurants[restaurant_days][]"
           value="tuesday"
           <?php if(is_array($model->restaurant_days) && in_array('tuesday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
    <label class="control-label">Вторник</label>
    <?= $form->field($model, 'restaurant_schedule[tuesday][from]')->textInput()->label(false); ?>
    <?= $form->field($model, 'restaurant_schedule[tuesday][to]')->textInput()->label(false); ?>

    <input type="checkbox"
           name="Restaurants[restaurant_days][]"
           value="wednesday"
           <?php if(is_array($model->restaurant_days) && in_array('wednesday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
    <label class="control-label">Среда</label>
    <?= $form->field($model, 'restaurant_schedule[wednesday][from]')->textInput()->label(false); ?>
    <?= $form->field($model, 'restaurant_schedule[wednesday][to]')->textInput()->label(false); ?>

    <input type="checkbox"
           name="Restaurants[restaurant_days][]"
           value="thursday"
           <?php if(is_array($model->restaurant_days) && in_array('thursday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
    <label class="control-label">Четверг</label>
    <?= $form->field($model, 'restaurant_schedule[thursday][from]')->textInput()->label(false); ?>
    <?= $form->field($model, 'restaurant_schedule[thursday][to]')->textInput()->label(false); ?>

    <input type="checkbox"
           name="Restaurants[restaurant_days][]"
           value="friday"
           <?php if(is_array($model->restaurant_days) && in_array('friday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
    <label class="control-label">Пятница</label>
    <?= $form->field($model, 'restaurant_schedule[friday][from]')->textInput()->label(false); ?>
    <?= $form->field($model, 'restaurant_schedule[friday][to]')->textInput()->label(false); ?>

    <input type="checkbox"
           name="Restaurants[restaurant_days][]"
           value="saturday"
           <?php if(is_array($model->restaurant_days) && in_array('saturday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
    <label class="control-label">Суббота</label>
    <?= $form->field($model, 'restaurant_schedule[saturday][from]')->textInput()->label(false); ?>
    <?= $form->field($model, 'restaurant_schedule[saturday][to]')->textInput()->label(false); ?>

    <input type="checkbox"
           name="Restaurants[restaurant_days][]"
           value="sunday"
           <?php if(is_array($model->restaurant_days) && in_array('sunday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
    <label class="control-label">Воскресенье</label>
    <?= $form->field($model, 'restaurant_schedule[sunday][from]')->textInput()->label(false); ?>
    <?= $form->field($model, 'restaurant_schedule[sunday][to]')->textInput()->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
