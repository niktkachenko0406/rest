<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RestaurantKitchen */

$this->title = 'Create Restaurant Kitchen';
$this->params['breadcrumbs'][] = ['label' => 'Restaurant Kitchens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-kitchen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
