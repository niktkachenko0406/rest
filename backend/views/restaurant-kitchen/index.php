<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RestaurantKitchenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Restaurant Kitchens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-kitchen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Restaurant Kitchen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'kitchen_id',
            'restaurant_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
