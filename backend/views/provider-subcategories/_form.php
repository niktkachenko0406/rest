<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ProviderCategories;

/* @var $this yii\web\View */
/* @var $model common\models\ProviderSubcategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-subcategories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subcategory_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map(
            ProviderCategories::find()->all(),
            'category_id',
            'category_name'
        ), [
            'prompt' => 'Категория',
            'id'     => 'category_id'
        ]
    )?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
