<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProviderSubcategories */

$this->title = 'Редактировать подкатегорию: ' . $model->subcategory_name;
$this->params['breadcrumbs'][] = ['label' => 'Подкатегории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->subcategory_name, 'url' => ['view', 'id' => $model->subcategory_id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="provider-subcategories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
