<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProviderSubcategories */

$this->title = 'Добавить подкатегорию';
$this->params['breadcrumbs'][] = ['label' => 'Provider Subcategories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-subcategories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
