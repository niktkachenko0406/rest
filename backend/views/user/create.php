<?php
use yii\helpers\Html;
use common\models\User;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $profile common\models\UserProfile */

$this->title = 'Создание пользователя';
$this->params['breadcrumbs'][] = ['label' => $type ? 'Администраторы' : 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['index', 'type' => $type ? User::USER_TYPE_ADMIN : null], ['class' => 'btn btn-success']) ?>
    </p>
    <?= $this->render('_form-create', [
        'user'    => $user,
        'profile' => $profile,
        'model'   => $model,
        'type'    => $type
    ]) ?>
</div>
