<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use common\models\UserProfile;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $profile common\models\UserProfile */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => $type ? 'Администраторы' : 'Пользователи', 'url' => ['index', 'type' => $type]];
$this->params['breadcrumbs'][] = $this->title;
$type = $type ? User::USER_TYPE_ADMIN : null;
?>
<div class="user-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['index', 'type' => $type], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::a('Редактировать',  ['update',      'id' => $model->id, 'type' => $type], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Сменить пароль', ['change-pass', 'id' => $model->id, 'type' => $type], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить',        ['delete',      'id' => $model->id, 'type' => $type], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Вы уверены, что хотите удалить пользователя?',
                'method'  => 'post',
            ],
        ]) ?>
        <?php
            if(!$type) {
                echo $model->status
                    ? Html::a('Скрыть пользователя', ['approved-user', 'id' => $model->id, 'status' => $model::STATUS_DELETED, 'type' => $type], ['class' => 'btn btn-warning'])
                    : Html::a('Сделать активным',    ['approved-user', 'id' => $model->id, 'status' => $model::STATUS_ACTIVE,  'type' => $type], ['class' => 'btn btn-primary']);
            }
        ?>
    </p>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'attribute' => 'status',
                'label'     => 'Статус',
                'format'    => 'text',
                'value'     => function($model) {
                    return $model->getStatus($model->status);
                }
            ],
            [
                'attribute' => 'profile_avatar',
                'label'     => 'Аватар',
                'format'    => 'html',
                'value'     => Html::img($model->getAvatar($model->userProfile->profile_avatar), ['width' => '150px', 'style' => 'max-width:100%'])
            ],
            'email',
            [
                'label' => 'Телефон',
                'value' => $model->userProfile->profile_phone ?: 'Телефон не указан'
            ],
            [
                'label' => 'Имя',
                'value' => $model->userProfile->profile_name ?: 'Имя не указано'
            ],
            [
                'label' => 'Фамилия',
                'value' => $model->userProfile->profile_surname ?: 'Фамилия не указана'
            ],
            [
                'attribute' => 'created_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],
            [
                'attribute' => 'updated_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],
        ],
    ]) ?>
    <p>
        <?= Html::a('Список комментариев', ['comments/user-comments', 'id' => $model->id, 'type' => $type], ['class' => 'btn btn-primary']) ?>
    </p>
</div>
