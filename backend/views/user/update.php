<?php

use yii\helpers\Html;
use common\models\User;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $profile common\models\UserProfile */

$this->title = 'Редактирование ' . ($type ? 'администратора' : 'пользователя') . ': ' . $user->email;
$this->params['breadcrumbs'][] = ['label' => $type ? 'Администраторы' : 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->email,   'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="user-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['view', 'id' => $user->id, 'type' => $type], ['class' => 'btn btn-success']) ?>
    </p>
    <?= $this->render('_form', [
        'user'    => $user,
        'profile' => $profile,
        'type'    => $type
    ]) ?>
</div>
