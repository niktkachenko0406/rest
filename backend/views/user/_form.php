<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use yii\helpers\Url;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $profile common\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'id'      => 'profileForm',
        'action'  => '',
        'method'  => 'post',
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?php if (!$profile->isNewRecord && !empty($profile->profile_avatar)) { ?>
        <?= $form->field($profile, 'profile_avatar')->widget(FileInput::className(), [
            'pluginOptions' => [
                'initialPreview' => [
                    Html::img($profile->getAvatar('admin'), ['class'=>'file-preview-image']),
                ],
                'showPreview' => true,
                'showRemove'  => false,
                'showUpload'  => false,
                'browseLabel' => 'Выбрать изображение',

            ],

        ])?>
    <?php } else { ?>
        <?= $form->field($profile, 'profile_avatar')->widget(FileInput::className(), [
            'pluginOptions' => [
                'showPreview' => true,
                'showRemove'  => false,
                'showUpload'  => false,
                'browseLabel' => 'Выбрать изображение',
            ]

        ])?>
    <?php } ?>

    <?= $form->field($profile, 'profile_phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($profile, 'profile_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($profile, 'profile_surname')->textInput(['maxlength' => true]) ?>
    <?= $form->field($user, 'status')->dropDownList($user->getStatus()) ?>

    <?php if($type) { ?>
        <?php $user->role = current(Yii::$app->authManager->getRolesByUser($user->id))->name; ?>
        <div class="form-group">
            <?= $form->field($user, 'role')->dropDownList(
                ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'name'), [
                    'options' => $role
                ]
            )?>
        </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
