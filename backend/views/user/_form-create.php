<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use yii\helpers\Url;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $profile common\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin([
        'id'      => 'userCreateForm',
        'action'  => '',
        'method'  => 'post',
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($profile, 'profile_avatar')->widget(FileInput::className(), [
        'pluginOptions' => [
            'showPreview' => true,
            'showRemove'  => false,
            'showUpload'  => false,
            'browseLabel' => 'Выбрать изображение',
        ]

    ])?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <?= $form->field($profile, 'profile_phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($profile, 'profile_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($profile, 'profile_surname')->textInput(['maxlength' => true]) ?>

    <?php if($type) { ?>
        <div class="form-group">
            <?= $form->field($model, 'status')->textInput(['readonly' => true, 'value' => User::STATUS_ACTIVE]) ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'role')->dropDownList(
                    ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'name'), [
                    'prompt' => 'Роль',
                ]
            )?>
        </div>
    <?php } else { ?>
        <div class="form-group">
            <?= $form->field($model, 'status')->dropDownList($user->getStatus()) ?>
        </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
