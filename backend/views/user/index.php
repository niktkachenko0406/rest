<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $type ? 'Администраторы' : 'Зарегистрированные пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::a('Создать', ['create', 'type' => $type], ['class' => 'btn btn-primary'])
    ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'email',
                'label'     => 'Email',
                'format'    => 'html',
                'content'   => function($model) use ($type) {
                    return Html::a($model->email, ['user/view', 'id' => $model->id, 'type' => $type]);
                }
            ],
            [
                'attribute' => 'profile_name',
                'label'     => 'Имя',
                'format'    => 'text',
                'content'   => function($model) {
                    $userProfile = $model->userProfile;

                    if($userProfile && !empty($userProfile->profile_name)) {
                        return $userProfile->profile_name;
                    } else {
                        return 'Не задано';
                    }
                }
            ],
            [
                'attribute' => 'profile_phone',
                'label'     => 'Телефон',
                'format'    => 'text',
                'content'   => function($model) {
                    $userProfile = $model->userProfile;

                    if($userProfile && !empty($userProfile->profile_phone)) {
                        return $userProfile->profile_phone;
                    } else {
                        return 'Не задано';
                    }
                }
            ],
            [
                'attribute' => 'status',
                'label'     => 'Статус',
                'format'    => 'text',
                'content'   => function($model) {
                    return $model->getStatus($model->status);
                }
            ],
            [
                'attribute' => 'created_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],
            [
                'attribute' => 'updated_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' =>'{view} {update} {delete}',
                'header'   =>'Действия',
                'buttons'  => [
                    'view' => function ($url, $model) use ($type) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->id, 'type' => $type]
                        );
                    },
                    'update' => function ($url, $model) use ($type) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            ['update', 'id' => $model->id, 'type' => $type]
                        );
                    }
                    ,
                    'delete' => function ($url, $model) use ($type) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->id, 'type' => $type], [
                                'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'data-method'  => 'post'
                            ]
                        );
                    }
                ]
            ],
        ],
    ]); ?>
</div>
