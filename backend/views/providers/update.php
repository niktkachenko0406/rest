<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Providers */

$this->title = 'Update Providers: ' . $model->provider_id;
$this->params['breadcrumbs'][] = ['label' => 'Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->provider_id, 'url' => ['view', 'id' => $model->provider_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="providers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'        => $model,
        'servicesList' => $servicesList
    ]) ?>

</div>
