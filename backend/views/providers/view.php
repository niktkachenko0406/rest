<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Providers */

$this->title = $model->provider_name;
$this->params['breadcrumbs'][] = ['label' => 'Поставщики', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="providers-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Товары', ['providers-products/index', 'providerId' => $model->provider_id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Редактировать', ['update', 'id' => $model->provider_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->provider_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'provider_name',
            'provider_short_desc',
            [
                'attribute' => 'provider_avatar',
                'label'     => 'Аватар',
                'format'    => 'html',
                'value'     => function ($model) {
                    return Html::img($model->getImg('admin'), ['width' => '270px', 'style' => 'max-width:100%']);
                }
            ],
            'provider_email:email',
            'provider_phone',
            'provider_site',
            [
                'label' => 'Город',
                'value' => $model->city->city_name
            ],
            [
                'label' => 'Район',
                'value' => $model->district->district_name
            ],
            'provider_address',
            [
                'label'  => 'Описание',
                'format' => 'html',
                'value'  => $model->provider_desc
            ],
        ],
    ]) ?>
</div>
<h1><?= Html::encode('Услуги') ?></h1>
<table class="table table-striped table-bordered detail-view">
    <?php foreach ($services as $category): ?>
        <tr>
            <td><?= $category[0] ?></td>
            <td>
                <?php foreach ($category[1] as $subCategory): ?>
                    <?= $subCategory ?><br>
                <?php endforeach; ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
