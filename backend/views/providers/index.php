<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProvidersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поставщики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="providers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить поставщика', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'provider_name',
                'label'     => 'Название',
                'format'    => 'html',
                'content'   => function ($model) {
                    return Html::a($model->provider_name, ['view', 'id' => $model->provider_id]);
                }
            ],
            'provider_email:email',
            'provider_phone',
            'provider_site',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
