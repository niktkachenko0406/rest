<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProvidersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="providers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'provider_id') ?>

    <?= $form->field($model, 'provider_name') ?>

    <?= $form->field($model, 'provider_short_desc') ?>

    <?= $form->field($model, 'provider_desc') ?>

    <?= $form->field($model, 'provider_email') ?>

    <?php // echo $form->field($model, 'provider_phone') ?>

    <?php // echo $form->field($model, 'provider_site') ?>

    <?php // echo $form->field($model, 'city_id') ?>

    <?php // echo $form->field($model, 'district_id') ?>

    <?php // echo $form->field($model, 'provider_address') ?>

    <?php // echo $form->field($model, 'provider_avatar') ?>

    <?php // echo $form->field($model, 'uri') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
