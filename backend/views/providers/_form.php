<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use common\models\Cities;
use common\models\Districts;
use common\models\ProviderCategories;

/* @var $this yii\web\View */
/* @var $model common\models\Providers */
/* @var $form yii\widgets\ActiveForm */

$categoryModel = new ProviderCategories;
?>

<div class="providers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'provider_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'provider_short_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'provider_desc')->widget(Widget::className(), [
        'settings' => [
            'lang'      => 'ru',
            'minHeight' => 200,
            'plugins'   => [
                'clips',
                'fullscreen'
            ]
        ]
    ]); ?>

    <?= $form->field($model, 'provider_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'provider_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'provider_site')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->dropDownList(
        ArrayHelper::map(
            Cities::find()->all(),
            'city_id',
            'city_name'
        ), [
            'prompt' => 'Город',
            'id'     => 'city-id'
        ]
    )?>

    <?= $form->field($model, 'district_id')->widget(DepDrop::class, [
        'options'       => ['id'=>'district-id'],
        'data'          => (isset($model->city_id)) ? ArrayHelper::map(Districts::find()->where(['city_id' => $model->city_id])->all(), 'district_id', 'district_name') : [],
        'pluginOptions' => [
            'depends'     => ['city-id'],
            'placeholder' => 'Район',
            'loading'     => true,
            'url'         => Url::to(['/restaurants/get-districts']),
        ]
    ]);?>

    <?= $form->field($model, 'provider_address')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->isNewRecord && !empty($model->provider_avatar)) { ?>
        <?= $form->field($model, 'provider_avatar')->widget(FileInput::className(), [
            'pluginOptions' => [
                'initialPreview' => [
                    Html::img(Yii::$app->params['uploadUrl'] . '/provider/avatars/' . $model->provider_avatar, ['class'=>'file-preview-image']),
                ],
                'showPreview' => true,
                'showRemove'  => false,
                'showUpload'  => false,
                'browseLabel' => 'Выбрать изображение',
            ],
        ])?>
    <?php } else { ?>
        <?= $form->field($model, 'provider_avatar')->widget(FileInput::className(), [
            'pluginOptions' => [
                'showPreview' => true,
                'showRemove'  => false,
                'showUpload'  => false,
                'browseLabel' => 'Выбрать изображение',
            ]
        ])?>
    <?php } ?>

    <div class="form-group">
        <?= Select2::widget([
            'name'     => 'providers_service[]',
            'language' => 'ru',
            'value'    => $servicesList,
            'data'     => $categoryModel->getServices(),
            'size'     => Select2::LARGE,
            'options'  => [
                'placeholder' => 'Выберите подкатегории ...',
                'multiple'    => true
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
