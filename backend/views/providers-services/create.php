<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProvidersServices */

$this->title = 'Create Providers Services';
$this->params['breadcrumbs'][] = ['label' => 'Providers Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="providers-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
