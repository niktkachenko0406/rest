<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Profile', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'profile_id',
//            'user_id',
            [
                'attribute' =>'user_id',
                'label'     => 'Email',
                'format'    => 'text',
                'content'   => function($model) {
                    if($model->user) {
                        return $model->user->email;
                    }
                }
            ],
            'profile_name',
            'profile_surname',
            'profile_avatar',
            'profile_phone',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
