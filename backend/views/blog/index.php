<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Comments;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $rest backend\models\RestaurantsSearch */

$restName    = $rest->restaurant_name ?: 'Не указано';
$this->title = 'Список постов ресторана ' . $restName;

$this->params['breadcrumbs'][] = ['label' => 'Рестораны', 'url' => ['restaurants/index']];
$this->params['breadcrumbs'][] = ['label' => $restName, 'url' => ['restaurants/view', 'id' => $rest->restaurant_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['restaurants/view', 'id' => $rest->restaurant_id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить статью', ['blog/create', 'id' => $rest->restaurant_id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'blog_title',
                'label'     => 'Заголовок',
                'format'    => 'html',
                'content'   => function($model) {
                    return Html::a($model->blog_title, ['view', 'id' => $model->blog_id]);
                }
            ],
            [
                'attribute' => 'blog_status',
                'label'     => 'Статус',
                'format'    => 'text',
                'content'   => function($model) {
                    return $model->getStatus($model->blog_status);
                }
            ],
            [
                'label'   => 'Не одобренные комментарии',
                'format'  => 'html',
                'content' => function($model) {
                    return Comments::isApproved(Comments::ENTITY_BLOG, $model->blog_id);
                }
            ],
            [
                'attribute' => 'created_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'header' => 'Действия'
            ],
        ],
    ]); ?>
</div>
