<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Comments;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */

$this->title = $model->blog_title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['blog/index', 'restId' => $model->restaurant_id], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->blog_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить',       ['delete', 'id' => $model->blog_id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Удалить данный пост?',
                'method'  => 'post',
            ],
        ]) ?>
        <?= Html::a('Комментарии', ['comments/blog-comments', 'id' => $model->blog_id], ['class' => 'btn btn-primary']) ?>
        <?= $model->blog_status ? '' : Html::a('Одобрить', ['approved-post', 'id' => $model->blog_id], ['class' => 'btn btn-primary']) ?>
    </p>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'label'  => 'Не одобренные комментарии',
                'format' => 'html',
                'value'  =>  Comments::isApproved(Comments::ENTITY_BLOG, $model->blog_id)
            ],
            [
                'attribute' => 'blog_image',
                'label'     => 'Изображение',
                'format'    => 'html',
                'value'     => function ($model) {
                    return Html::img($model->getImg('admin'), ['width' => '368px', 'style' => 'max-width:100%']);
                }
            ],
            [
                'label' => 'Ресторан',
                'value' => function($model) {
                    return $model->restaurant->restaurant_name ?: 'Не указано';
                }
            ],
            'blog_title',
            [
                'label'  => 'Текст',
                'format' => 'html',
                'value'  => function($model) {
                    return $model->blog_text;
                }
            ],
            [
                'label' => 'Статус',
                'value' => function($model) {
                    return $model->getStatus($model->blog_status);
                }
            ],
            [
                'attribute' => 'created_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ]
            ],
        ],
    ]) ?>

</div>
