<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use common\models\Blog;
use yii\helpers\ArrayHelper;
use common\models\Restaurants;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-form">

    <?php $form = ActiveForm::begin(
        [
            'id'      => 'blogForm',
            'action'  => '',
            'method'  => 'post',
            'options' => ['enctype'=>'multipart/form-data']
        ]
    ); ?>
    <?= $form->field($model, 'blog_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'blog_text')->widget(Widget::className(), [
        'settings' => [
            'lang'      => 'ru',
            'minHeight' => 200,
            'plugins'   => [
                'clips',
                'fullscreen'
            ]
        ]
    ]); ?>
    <?php if (!$model->isNewRecord && !empty($model->blog_image)) { ?>
        <?= $form->field($model, 'blog_image')->widget(FileInput::className(), [
            'pluginOptions' => [
                'initialPreview' => [
                    Html::img(Yii::$app->params['uploadUrl'] . '/blog/' . $model->blog_image, ['class'=>'file-preview-image', 'alt'=>$model->blog_image, 'title'=>$model->blog_image]),
                ],
                'showPreview' => true,
                'showRemove'  => false,
                'showUpload'  => false,
                'browseLabel' => 'Выбрать изображение',
            ],
        ])?>
    <?php } else { ?>
        <?= $form->field($model, 'blog_image')->widget(FileInput::className(), [
            'pluginOptions' => [
                'showPreview' => true,
                'showRemove'  => false,
                'showUpload'  => false,
                'browseLabel' => 'Выбрать изображение',
            ]
        ])?>
    <?php } ?>

    <?= $form->field($model, 'blog_status')->dropDownList($model->getStatus())?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
