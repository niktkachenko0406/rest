<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */

$this->title = 'Редактировать статью блога: ' . $model->blog_title;
$this->params['breadcrumbs'][] = ['label' => $model->blog_id, 'url' => ['view', 'id' => $model->blog_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="blog-update">
    <p>
        <?= Html::a('Назад', ['blog/view', 'id' => $model->blog_id], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
