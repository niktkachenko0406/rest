<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Orders', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'user_id',
                'label'     => 'Пользователь',
                'format'    => 'text',
                'content'   => function($model) {
                    $user        = $model->user;
                    $userProfile = $user->userProfile;
                    $result      = $userProfile && !empty($userProfile->profile_name) ? $userProfile->profile_name : $user->email;

                    return Html::a($result, ['user/view', 'id' => $user->id], ['target' => '_blank']);
                }
            ],
            'order_person',
            [
                'attribute' => 'restaurant_id',
                'label'     => 'Ресторан',
                'format'    => 'text',
                'content'   => function($model) {
                    $rest = $model->restaurant;

                    return Html::a($rest->restaurant_name, ['restaurants/view', 'id' => $rest->restaurant_id], ['target' => '_blank']);
                }
            ],
            'order_phone',
            'order_date'
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
