<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Reasons */

$this->title = 'Create Reasons';
$this->params['breadcrumbs'][] = ['label' => 'Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reasons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
