<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Reasons */

$this->title = 'Update Reasons: ' . $model->reason_id;
$this->params['breadcrumbs'][] = ['label' => 'Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reason_id, 'url' => ['view', 'id' => $model->reason_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reasons-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
