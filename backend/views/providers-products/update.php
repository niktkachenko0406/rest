<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProvidersProducts */

$this->title = 'Редактирование продукта: ' . $model->product_title;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index', 'providerId' => $model->provider_id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="providers-products-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
