<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\ProvidersProducts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="providers-products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_title')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->isNewRecord && !empty($model->product_image)) { ?>
        <?= $form->field($model, 'product_image')->widget(FileInput::className(), [
            'pluginOptions' => [
                'initialPreview' => [
                    Html::img(Yii::$app->params['uploadUrl'] . '/provider/product/' . $model->product_image, ['class'=>'file-preview-image', 'alt'=>$model->product_image, 'title'=>$model->product_image]),
                ],
                'showPreview' => true,
                'showRemove'  => false,
                'showUpload'  => false,
                'browseLabel' => 'Выбрать изображение',
            ],
        ])?>
    <?php } else { ?>
        <?= $form->field($model, 'product_image')->widget(FileInput::className(), [
            'pluginOptions' => [
                'showPreview' => true,
                'showRemove'  => false,
                'showUpload'  => false,
                'browseLabel' => 'Выбрать изображение',
            ]
        ])?>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Отредактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
