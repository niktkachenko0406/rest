<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProvidersProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты поставщика: ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="providers-products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Назад', ['providers/view', 'id' => $providerId], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить товар', ['create', 'providerId' => $providerId], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns' => [
            'product_title',
            [
                'attribute' => 'product_image',
                'label'     => 'Изображение',
                'format'    => 'html',
                'content'     => function ($model) {
                    return Html::img($model->getImg('admin'), ['width' => '170px', 'style' => 'max-width:100%']);
                }
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'header'   => 'Действия',
            ],
        ],
    ]); ?>
</div>
