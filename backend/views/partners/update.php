<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Partners */

$this->title = 'Редактировать статью: ' . $model->partner_title;
$this->params['breadcrumbs'][] = ['label' => 'Партнерам', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->partner_id, 'url' => ['view', 'id' => $model->partner_id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="partners-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
