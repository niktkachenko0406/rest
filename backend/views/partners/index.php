<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PartnersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\Partners */

$this->title = 'Партнерам';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partners-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить статью', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'tableOptions' => [
            'class' => 'news-table table table-striped table-bordered'
        ],
        'columns'      => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'email',
                'label'     => 'Автор',
                'format'    => 'text',
                'content'   => function($model) {
                    $user = $model->user;

                    return $user->email;
                }
            ],
            [
                'attribute' => 'partner_title',
                'label'     => 'Заголовок',
                'format'    => 'html',
                'content'   => function ($model) {
                    return Html::a(StringHelper::truncate($model->partner_title, 100), ['view', 'id' => $model->partner_id]);
                }
            ],
//            'partner_text:ntext',
            [
                'attribute' => 'partner_text',
                'label'     => 'Текст',
                'format'    => 'html',
                'content'   => function ($model) {
                    return $model->partner_text;
                }
            ],
            [
                'attribute' => 'created_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],

            ['class' => 'yii\grid\ActionColumn', 'header'=>'Действия'],
        ],
    ]); ?>
</div>
