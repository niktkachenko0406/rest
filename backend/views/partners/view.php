<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Partners */
/* @var $user common\models\User */

$this->title = $model->partner_title;
$this->params['breadcrumbs'][] = ['label' => 'Партнерам', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$user        = $model->user;
?>
<div class="partners-view">
    <p>
        <?= Html::a('Назад', ['partners/index'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= StringHelper::truncate($model->partner_title,100,'...'); ?></h1>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->partner_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->partner_id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'attribute' => 'partner_image',
                'label'     => 'Изображение',
                'format'    => 'html',
                'value'     => function ($model) {
                    return Html::img($model->getImg('admin'), ['width' => '500px', 'style' => 'max-width:100%']);
                }
            ],
            [
                'label' => 'Автор',
                'value' => $user->email
            ],
            [
                'attribute' => 'partner_title',
                'value'     => function ($model) {
                    return StringHelper::truncate($model->partner_title, 100);
                }
            ],
            [
                'attribute' => 'partner_text',
                'label'     => 'Текст',
                'format'    => 'html',
                'value'     => function ($model) {
                    return $model->partner_text;
                }
            ],
//            'partner_text:ntext',
            [
                'attribute' => 'created_at',
                'format'    => [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ]
            ],
        ],
    ]) ?>

</div>
