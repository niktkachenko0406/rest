<?php

use yii\helpers\Html;
use \yii\helpers\StringHelper;
use yii\widgets\DetailView;
use common\models\Comments;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $user common\models\User */
/* @var $userProfile common\models\UserProfile */

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$user        = $model->user;
$userProfile = $user->userProfile;
?>
<div class="news-view">
    <p>
        <?= Html::a('Назад', ['news/index'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= StringHelper::truncate($model->news_title,100,'...'); ?></h1>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->news_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->news_id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method'  => 'post',
            ],
        ]) ?>
        <?= Html::a('Комментарии', ['comments/news-comments', 'id' => $model->news_id], ['class' => 'btn btn-primary']) ?>
    </p>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'label'  => 'Не одобренные комментарии',
                'format' => 'html',
                'value'  =>  Comments::isApproved(Comments::ENTITY_NEWS, $model->news_id)
            ],
            [
                'attribute' => 'news_image',
                'label'     => 'Изображение',
                'format'    => 'html',
                'value'     => function ($model) {
                    return Html::img($model->getImg('admin'), ['width' => '500px', 'style' => 'max-width:100%']);
                }
            ],
            [
                'label' => 'Автор',
                'value' => $userProfile->profile_name ?: $user->email
            ],
            [
                'attribute' => 'news_title',
                'value'     => function ($model) {
                    return StringHelper::truncate($model->news_title, 100);
                }
            ],
            [
                'attribute' => 'news_text',
                'format'    => 'html',
                'value'     => $model->news_text
            ],
            [
                'attribute' => 'created_at',
                'format'    => [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ]
            ],
        ],
    ]) ?>

</div>
