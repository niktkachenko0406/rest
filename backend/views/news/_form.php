<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'id'      => 'newsForm',
        'action'  => '',
        'method'  => 'post',
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'news_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_text')->widget(Widget::className(), [
        'settings' => [
            'lang'      => 'ru',
            'minHeight' => 200,
            'plugins'   => [
                'clips',
                'fullscreen'
            ]
        ]
    ]); ?>

    <?php if (!$model->isNewRecord && !empty($model->news_image)) { ?>
        <?= $form->field($model, 'news_image')->widget(FileInput::className(), [
            'pluginOptions' => [
                'initialPreview' => [
                    Html::img($model->getImg('admin'), ['class'=>'file-preview-image']),
                ],
                'showPreview' => true,
                'showRemove' => false,
                'showUpload' => false,
                'browseLabel' => 'Выбрать изображение',

            ],

        ])?>
    <?php } else { ?>
        <?= $form->field($model, 'news_image')->widget(FileInput::className(), [
            'pluginOptions' => [
                'showPreview' => true,
                'showRemove' => false,
                'showUpload' => false,
                'browseLabel' => 'Выбрать изображение',
            ]

        ])?>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
