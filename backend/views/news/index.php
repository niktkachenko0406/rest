<?php

use common\models\Comments;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\News */

$this->title                   = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">
    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p><?= Html::a('Добавить новость', ['create'], ['class' => 'btn btn-success']) ?></p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'tableOptions' => [
            'class' => 'news-table table table-striped table-bordered'
        ],
        'columns'      => [
            [
                'attribute' => 'profile_name',
                'label'     => 'Автор',
                'format'    => 'text',
                'content'   => function($model) {
                    $user        = $model->user;
                    $userProfile = $user->userProfile;

                    if($userProfile && !empty($userProfile->profile_name)) {
                        return $userProfile->profile_name;
                    } else {
                        return $user->email;
                    }
                }
            ],
            [
                'attribute' => 'news_title',
                'label'     => 'Заголовок',
                'format'    => 'html',
                'content'   => function ($model) {
                    return Html::a(StringHelper::truncate($model->news_title, 100), ['view', 'id' => $model->news_id]);
                }
            ],
//            [
//                'attribute' => 'image',
//                'label'     => 'Изображение',
//                'format'    => 'html',
//                'value' => function ($model) {
//                    return Html::img($model->getImg('admin'),
//                        ['width' => '300px', 'style' => 'max-width:100%']);
//                }
//            ],
            [
                'label'  => 'Не одобренные комментарии',
                'format' => 'html',
                'value'  => function ($model) {
                    return Comments::isApproved(Comments::ENTITY_NEWS, $model->news_id);
                }
            ],
            [
                'attribute' => 'created_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],

            ['class' => 'yii\grid\ActionColumn', 'header'=>'Действия'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
