<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\models\User;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Resto Spilnota',
        'brandUrl'   => Yii::$app->homeUrl,
        'options'    => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Главная', 'url' => Url::to(['/site/index'])],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Авторизация', 'url' => Url::to(['/site/login'])];
    } else {
//        $menuItems[] = ['label' => 'Администрирование', 'url' => ['/admin']];
        $menuItems[] = ['label' => 'Рестораны', 'url' => Url::to(['/restaurants/index'])];
        $menuItems[] = ['label' => 'Новости',   'url' => Url::to(['/news/index'])];

        if (\Yii::$app->user->can('admin')) {
            $menuItems[] = ['label' => 'Пользователи', 'items' => [
                ['label' => 'Пользователи',   'url' => Url::to(['/user/index'])],
                ['label' => 'Администраторы', 'url' => Url::to(['/user/index', 'type' => User::USER_TYPE_ADMIN])],
            ]];
            $menuItems[] = ['label' => 'Локация', 'items' => [
                ['label' => 'Города', 'url' => Url::to(['/cities/index'])],
                ['label' => 'Районы', 'url' => Url::to(['/districts/index'])],
                ['label' => 'Метро',  'url' => Url::to(['/metro/index'])]
            ]];
            $menuItems[] = ['label' => 'Общее', 'items' => [
                ['label' => 'Типы ресторанов',    'url' => Url::to(['/types/index'])],
                ['label' => 'Кухни ресторанов',   'url' => Url::to(['/kitchens/index'])],
                ['label' => 'Повод ресторанов',   'url' => Url::to(['/reasons/index'])],
                ['label' => 'Функции ресторанов', 'url' => Url::to(['/features/index'])]
            ]];
            $menuItems[] = ['label' => 'Поставщики', 'items' => [
                ['label' => 'Поставщики',   'url' => Url::to(['/providers/index'])],
                ['label' => 'Категории',    'url' => Url::to(['/provider-categories/index'])],
                ['label' => 'Подкатегории', 'url' => Url::to(['/provider-subcategories/index'])],
            ]];
            $menuItems[] = ['label' => 'Партнерам', 'url' => Url::to(['/partners/index'])];
            $menuItems[] = ['label' => 'Заказы',    'url' => Url::to(['/orders/index'])];
        }
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->email . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items'   => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Resto Spilnota <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
