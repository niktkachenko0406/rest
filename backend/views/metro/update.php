<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Metro */

$this->title = 'Отредактировать станцию: ' . $model->station_name;
$this->params['breadcrumbs'][] = ['label' => 'Метро', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->station_name, 'url' => ['view', 'id' => $model->station_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metro-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'  => $model,
        'cities' => $cities
    ]) ?>

</div>
