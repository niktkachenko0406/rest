<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MetroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\Metro */

$this->title = 'Метро';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metro-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить станцию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'station_id',
            'station_name',
            'branch_name',
            [
                'attribute' =>'city_id',
                'label'     => 'Город',
                'format'    => 'text',
                'content'   => function($model) {
                    return $model->city->city_name;
                }
            ],
            [
                'attribute' =>'district_id',
                'label'     => 'Район',
                'format'    => 'text',
                'content'   => function($model) {
                    return $model->district->district_name;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
