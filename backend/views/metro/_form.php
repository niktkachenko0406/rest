<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Cities;
use kartik\depdrop\DepDrop;


/* @var $this yii\web\View */
/* @var $model common\models\Metro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="metro-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'station_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'branch_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->dropDownList(Cities::getCities(), ['prompt' => 'выбрать', 'id' => 'js-city-id']); ?>

    <?= $form->field($model, 'district_id')->widget(DepDrop::className(), [
        'options' => ['id' => 'js-district-id'],
        'pluginOptions'=>[
            'depends'     =>['js-city-id'],
            'placeholder' => 'Выбрать',
            'url'         => Url::to(['metro/get-districts'])
        ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
