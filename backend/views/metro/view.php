<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Cities;
use common\models\Districts;

/* @var $this yii\web\View */
/* @var $model common\models\Metro */

$this->title = $model->station_name;
$this->params['breadcrumbs'][] = ['label' => 'Метро', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metro-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->station_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->station_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот обьект?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'station_id',
            'station_name',
            'branch_name',
            [                      // the owner name of the model
                'label' => 'Город',
                'value' => Cities::getCityName($model->city_id),
            ],
            [                      // the owner name of the model
                'label' => 'Район',
                'value' => Districts::getDistrictName($model->district_id),
            ],
        ],
    ]) ?>

</div>
