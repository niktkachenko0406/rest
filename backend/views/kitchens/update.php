<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Kitchens */

$this->title = 'Update Kitchens: ' . $model->kitchen_id;
$this->params['breadcrumbs'][] = ['label' => 'Kitchens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kitchen_id, 'url' => ['view', 'id' => $model->kitchen_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kitchens-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
