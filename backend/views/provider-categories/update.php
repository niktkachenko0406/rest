<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProviderCategories */

$this->title = 'Редактировать категорию: ' . $model->category_name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->category_name, 'url' => ['view', 'id' => $model->category_id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="provider-categories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
