<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RestaurantFeatures */

$this->title = 'Update Restaurant Features: ' . $model->feature_id;
$this->params['breadcrumbs'][] = ['label' => 'Restaurant Features', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->feature_id, 'url' => ['view', 'id' => $model->feature_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="restaurant-features-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
