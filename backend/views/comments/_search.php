<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CommentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'comment_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'entity_id') ?>

    <?= $form->field($model, 'comment_text') ?>

    <?= $form->field($model, 'comment_kitchen_rating') ?>

    <?php // echo $form->field($model, 'comment_interier_rating') ?>

    <?php // echo $form->field($model, 'comment_service_rating') ?>

    <?php // echo $form->field($model, 'comment_ambience_rating') ?>

    <?php // echo $form->field($model, 'comment_entity') ?>

    <?php // echo $form->field($model, 'comment_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
