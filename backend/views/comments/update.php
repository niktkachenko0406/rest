<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Comments */

$this->title = 'Редактировать комментарий: ' . $model->comment_id;
$this->params['breadcrumbs'][] = ['label' => $model->comment_id, 'url' => ['view', 'id' => $model->comment_id, 'userView' => $userView, 'type' => $type]];
$this->params['breadcrumbs'][] = 'Редактирование';

?>
<div class="comments-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Назад', ['comments/view', 'id' => $model->comment_id, 'userView' => $userView, 'type' => $type], ['class' => 'btn btn-success']) ?>
    </p>
    <?= $this->render('_form', [
        'model'  => $model,
        'entity' => $entity
    ]) ?>
</div>
