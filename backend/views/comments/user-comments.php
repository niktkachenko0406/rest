<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CommentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $user backend\models\UserProfileSearch */

$userName    = $user->userProfile ? $user->userProfile->profile_name : $user->email;
$this->title = 'Список комментариев ' . ($type ? 'администратора' : 'пользователя') . ': ' . $userName;
$this->params['breadcrumbs'][] = ['label' => $type ? 'Администраторы' : 'Пользователи', 'url' => ['user/index', 'type' => $type]];
$this->params['breadcrumbs'][] = ['label' => $userName, 'url' => ['user/view', 'id' => $user->id, 'type' => $type]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Назад', ['user/view', 'id' => $user->id, 'type' => $type], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'comment_text',
                'format'    => 'html',
                'value'     => function ($model) use ($type) {
                    return Html::a(StringHelper::truncate($model->comment_text, 100), ['view', 'id' => $model->comment_id, 'userView' => true, 'type' => $type], ['data-pjax' => 0]);
                }
            ],
            [
                'attribute' => 'comment_status',
                'label'     => 'Статус',
                'format'    => 'text',
                'content'   => function($model) {
                    return $model->getStatus($model->comment_status);
                }
            ],
            [
                'format'    => 'html',
                'content'   => function($model) use ($type) {
                    if(!$model->comment_status) {
                        return Html::a('Одобрить', ['approved-comment', 'id' => $model->comment_id, 'list' => true, 'userView' => true, 'type' => $type]);
                    }
                }
            ],
            [
                'attribute' => 'comment_entity',
                'label'     => 'Тип',
                'format'    => 'text',
                'content'   => function($model) {
                    return $model->getType($model->comment_entity);
                }
            ],
            [
                'attribute' => 'created_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' =>'{view} {update} {delete}',
                'header'   =>'Действия',
                'buttons'  => [
                    'view' => function ($url, $model) use ($type) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->comment_id, 'userView' => true, 'type' => $type],
                            ['data-pjax' => '0']
                        );
                    },
                    'update' => function ($url, $model) use ($type) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            ['update', 'id' => $model->comment_id, 'userView' => true, 'type' => $type],
                            ['data-pjax' => '0']
                        );
                    },
                    'delete' => function ($url, $model) use ($type) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->comment_id, 'userView' => true, 'type' => $type], [
                                'data-pjax'    => '0',
                                'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'data-method'  => 'post'
                            ]
                        );
                    }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
