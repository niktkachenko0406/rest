<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Comments;

/* @var $this yii\web\View */
/* @var $model common\models\Comments */
$action      = Comments::getAction($model->comment_entity);
$this->title = $model->comment_id;
$user        = $model->user;
$userProfile = $user->userProfile;

$this->params['breadcrumbs'][] = ['label' => 'Список комментариев', 'url' => ['comments/' . $action, 'id' => $model->entity_id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="comments-view">
    <p>
        <?= $userView
            ? Html::a('Назад', ['comments/user-comments', 'id' => $model->user_id, 'type' => $type], ['class' => 'btn btn-success'])
            : Html::a('Назад', ['comments/' . $action, 'id' => $model->entity_id], ['class' => 'btn btn-success'])
        ?>
    </p>
    <p>
        <?= Html::a('Реактировать', ['update', 'id' => $model->comment_id, 'userView' => $userView, 'type' => $type], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить',      ['delete', 'id' => $model->comment_id, 'userView' => $userView, 'type' => $type], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Удалить данный комментарий?',
                'method'  => 'post',
            ],
        ]) ?>
        <?php if(!$model->comment_status) {
            echo Html::a('Одобрить', ['approved-comment', 'id' => $model->comment_id, 'list' => false, 'userView' => $userView, 'type' => $type], ['class' => 'btn btn-primary']);
        } ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'label'  => 'Автор',
                'format' => 'html',
                'value'  => Html::a($userProfile->profile_name ?: $user->email, ['user/view', 'id' => $model->user_id], ['target' => '_blank'])
            ],
            [
                'label'     => 'Текст коментария',
                'format'    => 'html',
                'value'     => $model->comment_text
            ],
            [
                'label' => 'Статус',
                'value' => function($model) {
                    return $model->getStatus($model->comment_status);
                }
            ],
            [
                'label' => 'Тип',
                'value' => function($model) {
                    return $model->getType($model->comment_entity);
                }
            ],
            [
                'attribute' => 'created_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],
        ],
    ]) ?>

    <?php if($model->comment_entity == Comments::ENTITY_REST) {?>
        <?= DetailView::widget([
            'model'      => $model,
            'attributes' => [
                'comment_kitchen_rating',
                'comment_interier_rating',
                'comment_service_rating',
                'comment_ambience_rating'
            ],
        ]) ?>
    <?php } ?>
</div>
