<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Comments;

/* @var $this yii\web\View */
/* @var $model common\models\Comments */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="comments-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'comment_text')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'comment_status')->dropDownList($model->getStatus())?>

    <?php if($entity == Comments::ENTITY_REST) { ?>
        <?= $form->field($model, 'comment_kitchen_rating')->dropDownList($model->getRatingList())?>
        <?= $form->field($model, 'comment_interier_rating')->dropDownList($model->getRatingList())?>
        <?= $form->field($model, 'comment_service_rating')->dropDownList($model->getRatingList())?>
        <?= $form->field($model, 'comment_ambience_rating')->dropDownList($model->getRatingList())?>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
