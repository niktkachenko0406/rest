<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use common\models\Comments;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CommentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $blog common\models\Blog */

$this->title = 'Список комментариев';

$this->params['breadcrumbs'][] = ['label' => 'Ресторан',        'url' => ['restaurants/view', 'id' => $blog->restaurant_id]];
$this->params['breadcrumbs'][] = ['label' => 'Блог ресторана',  'url' => ['blog/index', 'restId' => $blog->restaurant_id]];
$this->params['breadcrumbs'][] = ['label' => $blog->blog_title, 'url' => ['blog/view', 'id' => $blog->blog_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Назад', ['blog/view', 'id' => $blog->blog_id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(
            'Добавить комментарий',
            ['comments/create', 'entity_id' => $blog->blog_id, 'entity' => Comments::ENTITY_BLOG],
            ['class' => 'btn btn-success']
        ) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'user_id',
                'label'     => 'Автор',
                'format'    => 'html',
                'content'   => function($model) {
                    $user        = $model->user;
                    $userProfile = $user->userProfile;
                    return Html::a($userProfile && !empty($userProfile->profile_name) ? $userProfile->profile_name : $user->email, ['user/view', 'id' => $model->user_id], ['target' => '_blank', 'data-pjax' => 0]);
                }
            ],
            [
                'attribute' => 'comment_text',
                'format'    => 'html',
                'value'     => function ($model) {
                    return Html::a(StringHelper::truncate($model->comment_text, 100), ['view', 'id' => $model->comment_id], ['data-pjax' => 0]);
                }
            ],
            [
                'attribute' => 'comment_status',
                'label'     => 'Статус',
                'format'    => 'text',
                'content'   => function($model) {
                    return $model->getStatus($model->comment_status);
                }
            ],
            [
                'format'  => 'html',
                'content' => function($model) {
                    if(!$model->comment_status) {
                        return Html::a('Одобрить', ['approved-comment', 'id' => $model->comment_id, 'list' => true]);
                    }
                }
            ],
            [
                'attribute' => 'comment_entity',
                'label'     => 'Тип',
                'format'    => 'text',
                'content'   => function($model) {
                    return $model->getType($model->comment_entity);
                }
            ],
            [
                'attribute' => 'created_at',
                'format'    =>  [
                    'date',
                    'HH:mm:ss dd.MM.yyyy'
                ],
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' =>'{view} {update} {delete}',
                'header'   =>'Действия'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
