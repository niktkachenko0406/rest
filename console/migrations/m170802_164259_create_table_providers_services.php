<?php

use yii\db\Migration;

class m170802_164259_create_table_providers_services extends Migration
{
    public function up()
    {
        $this->createTable('providers_services', [
            'id'             => $this->primaryKey()->unsigned()->unique(),
            'subcategory_id' => $this->integer()->unsigned()->notNull(),
            'provider_id'    => $this->integer()->unsigned()->notNull()
        ]);

        $this->createIndex(
            'idx-providers_services-subcategory_id',
            'providers_services',
            'subcategory_id'
        );

        $this->createIndex(
            'idx-providers_services-provider_id',
            'providers_services',
            'provider_id'
        );

        $this->addForeignKey(
            'fk-providers_services-subcategory_id',
            'providers_services',
            'subcategory_id',
            'provider_subcategories',
            'subcategory_id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-providers_services-provider_id',
            'providers_services',
            'provider_id',
            'providers',
            'provider_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-providers_services-provider_id',
            'providers_services'
        );

        $this->dropForeignKey(
            'fk-providers_services-subcategory_id',
            'providers_services'
        );

        $this->dropIndex(
            'idx-providers_services-provider_id',
            'providers_services'
        );

        $this->dropIndex(
            'idx-providers_services-subcategory_id',
            'providers_services'
        );

        $this->dropTable('providers_services');
    }
}
