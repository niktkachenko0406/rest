<?php

use yii\db\Migration;

class m170705_172534_create_table_restaurant_metro extends Migration
{
    public function up()
    {
        $this->createTable('restaurant_metro', [
            'id'            => $this->primaryKey()->unsigned()->unique(),
            'restaurant_id' => $this->integer()->unsigned()->notNull(),
            'station_id'    => $this->integer()->unsigned()->notNull(),
        ]);

        $this->createIndex(
            'idx-restaurant-metro-station_id',
            'restaurant_metro',
            'station_id'
        );

        $this->createIndex(
            'idx-restaurant-metro-restaurant_id',
            'restaurant_metro',
            'restaurant_id'
        );

        $this->addForeignKey(
            'fk-restaurant-metro-station_id',
            'restaurant_metro',
            'station_id',
            'metro',
            'station_id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-restaurant-metro-restaurant_id',
            'restaurant_metro',
            'restaurant_id',
            'restaurants',
            'restaurant_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-restaurant-metro-station_id',
            'restaurant_metro'
        );

        $this->dropForeignKey(
            'fk-restaurant-metro-restaurant_id',
            'restaurant_metro'
        );

        $this->dropIndex(
            'idx-restaurant-metro-station_id',
            'restaurant_metro'
        );

        $this->dropIndex(
            'idx-restaurant-metro-restaurant_id',
            'restaurant_metro'
        );

        $this->dropTable('restaurant_metro');
    }
}
