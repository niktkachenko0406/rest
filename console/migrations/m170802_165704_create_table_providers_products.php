<?php

use yii\db\Migration;

class m170802_165704_create_table_providers_products extends Migration
{
    public function up()
    {
        $this->createTable('providers_products', [
            'product_id'    => $this->primaryKey()->unsigned()->unique(),
            'provider_id'   => $this->integer()->unsigned()->notNull(),
            'product_title' => $this->string(255)->null(),
            'product_image' => $this->string(32)->null(),
            'uri'           => $this->string(255)->notNull()
        ]);

        $this->createIndex(
            'idx-providers_products-provider_id',
            'providers_products',
            'provider_id'
        );

        $this->addForeignKey(
            'fk-providers_products-provider_id',
            'providers_products',
            'provider_id',
            'providers',
            'provider_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-providers_products-provider_id',
            'providers_products'
        );

        $this->dropIndex(
            'idx-providers_products-provider_id',
            'providers_products'
        );

        $this->dropTable('providers_products');
    }
}
