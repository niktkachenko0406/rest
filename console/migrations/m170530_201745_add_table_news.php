<?php

use yii\db\Migration;

class m170530_201745_add_table_news extends Migration
{
    public function up() {
        $this->createTable('news', [
            'news_id'    => $this->primaryKey()->unsigned()->unique(),
            'user_id'    => $this->integer()->notNull(),
            'news_title' => $this->string(255)->null(),
            'news_text'  => $this->text()->null(),
            'news_image' => $this->string(32)->null(),
            'created_at' => $this->timestamp()
        ]);
    }

    public function down() {
        $this->dropTable('news');
    }
}
