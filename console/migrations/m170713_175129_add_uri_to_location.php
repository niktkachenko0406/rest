<?php

use yii\db\Migration;

class m170713_175129_add_uri_to_location extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cities', 'uri', $this->string(255)->notNull());
        $this->addColumn('districts', 'uri', $this->string(255)->notNull());
        $this->addColumn('metro', 'uri', $this->string(255)->notNull());
        $this->addColumn('features', 'uri', $this->string(255)->notNull());
        $this->addColumn('kitchens', 'uri', $this->string(255)->notNull());
        $this->addColumn('reasons', 'uri', $this->string(255)->notNull());
        $this->addColumn('types', 'uri', $this->string(255)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('cities', 'uri');
        $this->dropColumn('districts', 'uri');
        $this->dropColumn('metro', 'uri');
        $this->dropColumn('features', 'uri');
        $this->dropColumn('kitchens', 'uri');
        $this->dropColumn('reasons', 'uri');
        $this->dropColumn('types', 'uri');
    }
}
