<?php

use yii\db\Migration;

class m170626_195953_create_table_restaurant_features extends Migration
{
    public function up()
    {
        $this->createTable('restaurant_features', [
            'id'            => $this->primaryKey()->unsigned()->unique(),
            'restaurant_id' => $this->integer()->unsigned()->notNull(),
            'feature_id'    => $this->integer()->unsigned()->notNull(),
        ]);

        $this->createIndex(
            'idx-restaurant-features-feature_id',
            'restaurant_features',
            'feature_id'
        );

        $this->createIndex(
            'idx-restaurant-features-restaurant_id',
            'restaurant_features',
            'restaurant_id'
        );

        $this->addForeignKey(
            'fk-restaurant-features-feature_id',
            'restaurant_features',
            'feature_id',
            'features',
            'feature_id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-restaurant-features-restaurant_id',
            'restaurant_features',
            'restaurant_id',
            'restaurants',
            'restaurant_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-restaurant-features-feature_id',
            'restaurant_features'
        );

        $this->dropForeignKey(
            'fk-restaurant-features-restaurant_id',
            'restaurant_features'
        );

        $this->dropIndex(
            'idx-restaurant-features-feature_id',
            'restaurant_features'
        );

        $this->dropIndex(
            'idx-restaurant-features-restaurant_id',
            'restaurant_features'
        );

        $this->dropTable('restaurant_features');
    }
}
