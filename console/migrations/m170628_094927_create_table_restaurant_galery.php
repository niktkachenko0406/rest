<?php

use yii\db\Migration;

class m170628_094927_create_table_restaurant_galery extends Migration
{
    public function up()
    {
        $this->createTable('restaurant_gallery', [
            'gallery_id' => $this->primaryKey()->unsigned()->unique(),
            'restaurant_id' => $this->integer()->unsigned()->notNull(),
            'gallery_image' => $this->string(20)->notNull()
        ]);

        $this->createIndex(
            'idx-restaurant-gallery-restaurant_id',
            'restaurant_gallery',
            'restaurant_id'
        );

        $this->addForeignKey(
            'fk-restaurant-gallery-restaurant_id',
            'restaurant_gallery',
            'restaurant_id',
            'restaurants',
            'restaurant_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-restaurant-gallery-restaurant_id',
            'restaurant_gallery'
        );

        $this->dropIndex(
            'idx-restaurant-gallery-restaurant_id',
            'restaurant_gallery'
        );

        $this->dropTable('restaurant_gallery');
    }
}
