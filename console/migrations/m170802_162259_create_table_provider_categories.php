<?php

use yii\db\Migration;

class m170802_162259_create_table_provider_categories extends Migration
{
    public function up()
    {
        $this->createTable('provider_categories', [
            'category_id'   => $this->primaryKey()->unsigned()->unique(),
            'category_name' => $this->string(50)->notNull(),
            'uri'           => $this->string(255)->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('provider_categories');
    }
}
