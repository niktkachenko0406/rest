<?php

use yii\db\Migration;

class m170702_103553_alter_column_uri_to_rest_and_news extends Migration
{
    public function up()
    {
        $this->addColumn('news', 'uri', $this->string(255)->notNull()->after('news_image'));
        $this->addColumn('restaurants', 'uri', $this->string(255)->notNull());
        $this->addColumn('blog', 'uri', $this->string(255)->notNull()->after('blog_status'));
    }

    public function down()
    {
        $this->dropColumn('news', 'uri');
        $this->dropColumn('restaurants', 'uri');
        $this->dropColumn('blog', 'uri');
    }
}
