<?php

use yii\db\Migration;

class m170626_193908_create_table_features extends Migration
{
    public function up()
    {
        $this->createTable('features', [
            'feature_id'   => $this->primaryKey()->unsigned()->unique(),
            'feature_name' => $this->string(50)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('features');
    }
}
