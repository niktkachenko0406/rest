<?php

use yii\db\Migration;

class m170821_171415_add_table_subscribers extends Migration
{
    public function up()
    {
        $this->createTable('subscribers', [
            'subscriber_id' => $this->primaryKey()->unsigned()->unique(),
            'email'         => $this->string()->notNull()->unique(),
        ]);
    }

    public function down()
    {
        $this->dropTable('subscribers');
    }
}
