<?php

use yii\db\Migration;

class m170626_193823_create_table_types extends Migration
{
    public function up()
    {
        $this->createTable('types', [
            'type_id'   => $this->primaryKey()->unsigned()->unique(),
            'type_name' => $this->string(50)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('types');
    }
}
