<?php

use yii\db\Migration;

class m170524_165613_add_table_user_profile extends Migration
{
    public function up()
    {
        $this->createTable('user_profile', [
            'profile_id'      => $this->primaryKey()->unsigned()->unique(),
            'user_id'         => $this->integer()->notNull(),
            'profile_name'    => $this->string(80)->null(),
            'profile_surname' => $this->string(80)->null(),
            'profile_avatar'  => $this->string(32)->null()
        ]);

        $this->createIndex(
            'idx-profile-user_id',
            'user_profile',
            'user_id'
        );

        $this->addForeignKey(
            'fk-profile-user_id',
            'user_profile',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-profile-user_id',
            'user_profile'
        );

        $this->dropIndex(
            'idx-profile-user_id',
            'user_profile'
        );

        $this->dropTable('user_profile');
    }
}
