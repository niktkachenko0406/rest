<?php

use yii\db\Migration;

class m170814_193823_create_table_favorites extends Migration
{
    public function up()
    {
        $this->createTable('favorites', [
            'favorites_id'  => $this->primaryKey()->unsigned()->unique(),
            'user_id'       => $this->integer()->notNull(),
            'restaurant_id' => $this->integer()->unsigned()->notNull(),
        ]);

        $this->createIndex(
            'idx-favorites-user_id',
            'favorites',
            'user_id'
        );

        $this->createIndex(
            'idx-favorites-restaurant_id',
            'favorites',
            'restaurant_id'
        );

        $this->addForeignKey(
            'fk-favorites-user_id',
            'favorites',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-favorites-restaurant_id',
            'favorites',
            'restaurant_id',
            'restaurants',
            'restaurant_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-favorites-restaurant_id',
            'favorites'
        );

        $this->dropForeignKey(
            'fk-favorites-user_id',
            'favorites'
        );

        $this->dropIndex(
            'idx-favorites-restaurant_id',
            'favorites'
        );

        $this->dropIndex(
            'idx-favorites-user_id',
            'favorites'
        );

        $this->dropTable('favorites');
    }
}
