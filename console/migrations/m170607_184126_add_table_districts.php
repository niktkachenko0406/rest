<?php

use yii\db\Migration;

class m170607_184126_add_table_districts extends Migration
{
    public function up()
    {
        $this->createTable('districts', [
            'district_id'   => $this->primaryKey()->unsigned()->unique(),
            'district_name' => $this->string(100)->notNull(),
            'city_id'       => $this->integer()->unsigned()->notNull(),
        ]);

        $this->createIndex(
            'idx-district-city_id',
            'districts',
            'city_id'
        );

        $this->addForeignKey(
            'fk-district-city_id',
            'districts',
            'city_id',
            'cities',
            'city_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-district-city_id',
            'districts'
        );

        $this->dropIndex(
            'idx-district-city_id',
            'districts'
        );

        $this->dropTable('districts');
    }
}
