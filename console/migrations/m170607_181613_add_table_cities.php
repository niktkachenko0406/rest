<?php

use yii\db\Migration;

class m170607_181613_add_table_cities extends Migration
{
    public function up()
    {
        $this->createTable('cities', [
            'city_id'      => $this->primaryKey()->unsigned()->unique(),
            'city_name'         => $this->string(80)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('cities');
    }
}
