<?php

use yii\db\Migration;

class m170705_205044_create_table_comments extends Migration
{
    public function safeUp()
    {
        $this->createTable('comments', [
            'comment_id'              => $this->primaryKey()->unsigned()->unique(),
            'user_id'                 => $this->integer()->notNull(),
            'entity_id'               => $this->integer()->unsigned()->notNull(),
            'comment_text'            => $this->text()->notNull(),
            'comment_kitchen_rating'  => $this->smallInteger(1)->notNull()->defaultValue(0),
            'comment_interier_rating' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'comment_service_rating'  => $this->smallInteger(1)->notNull()->defaultValue(0),
            'comment_ambience_rating' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'comment_entity'          => $this->smallInteger(1)->notNull(),
            'comment_status'          => $this->smallInteger(1)->defaultValue(0)->notNull(),

        ]);

        $this->createIndex(
            'idx-comments-user_id',
            'comments',
            'user_id'
        );

        $this->addForeignKey(
            'fk-comments-user_id',
            'comments',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-comments-user_id',
            'comments'
        );

        $this->dropIndex(
            'idx-comments-user_id',
            'comments'
        );

        $this->dropTable('comments');
    }
}
