<?php

use yii\db\Migration;

class m170802_155757_create_table_providers extends Migration
{
    public function up()
    {
        $this->createTable('providers', [
            'provider_id'         => $this->primaryKey()->unsigned()->unique(),
            'provider_name'       => $this->string(100)->null(),
            'provider_short_desc' => $this->string(1024)->null(),
            'provider_desc'       => $this->text()->null(),
            'provider_email'      => $this->string(100)->null(),
            'provider_phone'      => $this->string(20)->null(),
            'provider_site'       => $this->string(100)->null(),
            'city_id'             => $this->integer()->unsigned()->null(),
            'district_id'         => $this->integer()->unsigned()->null(),
            'provider_address'    => $this->string(255)->null(),
            'provider_avatar'     => $this->string(32)->null(),
            'uri'                 => $this->string(255)->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('providers');
    }
}
