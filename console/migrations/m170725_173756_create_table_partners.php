<?php

use yii\db\Migration;

class m170725_173756_create_table_partners extends Migration
{
    public function up() {
        $this->createTable('partners', [
            'partner_id'    => $this->primaryKey()->unsigned()->unique(),
            'user_id'       => $this->integer()->notNull(),
            'partner_title' => $this->string(255)->null(),
            'partner_text'  => $this->text()->null(),
            'partner_image' => $this->string(32)->null(),
            'uri'           => $this->string(255)->notNull(),
            'created_at'    => $this->integer()->notNull()
        ]);
    }

    public function down() {
        $this->dropTable('partners');
    }
}
