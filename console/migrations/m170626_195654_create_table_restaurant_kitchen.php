<?php

use yii\db\Migration;

class m170626_195654_create_table_restaurant_kitchen extends Migration
{
    public function up()
    {
        $this->createTable('restaurant_kitchen', [
            'id'            => $this->primaryKey()->unsigned()->unique(),
            'kitchen_id'    => $this->integer()->unsigned()->notNull(),
            'restaurant_id' => $this->integer()->unsigned()->notNull(),
        ]);

        $this->createIndex(
            'idx-restaurant-kitchen-kitchen_id',
            'restaurant_kitchen',
            'kitchen_id'
        );

        $this->createIndex(
            'idx-restaurant-kitchen-restaurant_id',
            'restaurant_kitchen',
            'restaurant_id'
        );

        $this->addForeignKey(
            'fk-restaurant-kitchen-kitchen_id',
            'restaurant_kitchen',
            'kitchen_id',
            'kitchens',
            'kitchen_id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-restaurant-kitchen-restaurant_id',
            'restaurant_kitchen',
            'restaurant_id',
            'restaurants',
            'restaurant_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-restaurant-kitchen-kitchen_id',
            'restaurant_kitchen'
        );

        $this->dropForeignKey(
            'fk-restaurant-kitchen-restaurant_id',
            'restaurant_kitchen'
        );

        $this->dropIndex(
            'idx-restaurant-kitchen-kitchen_id',
            'restaurant_kitchen'
        );

        $this->dropIndex(
            'idx-restaurant-kitchen-restaurant_id',
            'restaurant_kitchen'
        );

        $this->dropTable('restaurant_kitchen');
    }
}
