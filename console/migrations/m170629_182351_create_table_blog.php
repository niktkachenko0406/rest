<?php

use yii\db\Migration;

class m170629_182351_create_table_blog extends Migration
{
    public function safeUp()
    {
        $this->createTable('blog', [
            'blog_id'       => $this->primaryKey()->unsigned()->unique(),
            'restaurant_id' => $this->integer()->unsigned()->notNull(),
            'blog_title'    => $this->string(255)->null(),
            'blog_text'     => $this->text()->null(),
            'blog_image'    => $this->string(32)->null(),
            'blog_status'   => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at'    => $this->timestamp()
        ]);

        $this->createIndex(
            'idx-blog-restaurant_id',
            'blog',
            'restaurant_id'
        );

        $this->addForeignKey(
            'fk-blog-restaurant_id',
            'blog',
            'restaurant_id',
            'restaurants',
            'restaurant_id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-blog-restaurant_id',
            'blog'
        );

        $this->dropIndex(
            'idx-blog-restaurant_id',
            'blog'
        );

        $this->dropTable('blog');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170629_182351_create_table_blog cannot be reverted.\n";

        return false;
    }
    */
}
