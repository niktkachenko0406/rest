<?php

use yii\db\Migration;

class m170802_162754_create_table_provider_subcategories extends Migration
{
    public function up()
    {
        $this->createTable('provider_subcategories', [
            'subcategory_id'   => $this->primaryKey()->unsigned()->unique(),
            'subcategory_name' => $this->string(100)->notNull(),
            'category_id'      => $this->integer()->unsigned()->notNull(),
            'uri'              => $this->string(255)->notNull()
        ]);

        $this->createIndex(
            'idx-subcategory-category_id',
            'provider_subcategories',
            'category_id'
        );

        $this->addForeignKey(
            'fk-subcategory-category_id',
            'provider_subcategories',
            'category_id',
            'provider_categories',
            'category_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-subcategory-category_id',
            'provider_subcategories'
        );

        $this->dropIndex(
            'idx-subcategory-category_id',
            'provider_subcategories'
        );

        $this->dropTable('provider_subcategories');
    }
}
