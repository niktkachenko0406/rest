<?php

use yii\db\Migration;

class m170703_192027_create_table_restaurant_reasons extends Migration
{
    public function up()
    {
        $this->createTable('reasons', [
            'reason_id'   => $this->primaryKey()->unsigned()->unique(),
            'reason_name' => $this->string(50)->notNull(),
        ]);

        $this->createTable('restaurant_reasons', [
            'id'            => $this->primaryKey()->unsigned()->unique(),
            'restaurant_id' => $this->integer()->unsigned()->notNull(),
            'reason_id'     => $this->integer()->unsigned()->notNull(),
        ]);

        $this->createIndex(
            'idx-restaurant-reasons-reason_id',
            'restaurant_reasons',
            'reason_id'
        );

        $this->createIndex(
            'idx-restaurant-reasons-restaurant_id',
            'restaurant_reasons',
            'restaurant_id'
        );

        $this->addForeignKey(
            'fk-restaurant-reasons-reason_id',
            'restaurant_reasons',
            'reason_id',
            'reasons',
            'reason_id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-restaurant-reasons-restaurant_id',
            'restaurant_reasons',
            'restaurant_id',
            'restaurants',
            'restaurant_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('restaurant_reasons');
        $this->dropTable('reasons');
    }
}
