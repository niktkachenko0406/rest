<?php

use yii\db\Migration;

class m170710_173742_add_columns_rest_and_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('restaurants', 'restaurant_status','tinyint(1) not null default 0');
        $this->addColumn('restaurants', 'restaurant_phone', $this->string(20)->null());
        $this->addColumn('restaurants', 'restaurant_rating', 'tinyint(1) not null default 0');
        $this->addColumn('user_profile', 'profile_phone', $this->string(20)->null());
    }

    public function safeDown()
    {
        $this->dropColumn('restaurants', 'restaurant_status');
        $this->dropColumn('restaurants', 'restaurant_phone');
        $this->dropColumn('restaurants', 'restaurant_rating');
        $this->dropColumn('user_profile', 'profile_phone');
    }
}
