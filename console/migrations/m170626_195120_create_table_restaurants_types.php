<?php

use yii\db\Migration;

class m170626_195120_create_table_restaurants_types extends Migration
{
    public function up()
    {
        $this->createTable('restaurant_types', [
            'id'            => $this->primaryKey()->unsigned()->unique(),
            'type_id'       => $this->integer()->unsigned()->notNull(),
            'restaurant_id' => $this->integer()->unsigned()->notNull()
        ]);

        $this->createIndex(
            'idx-restaurant-types-type_id',
            'restaurant_types',
            'type_id'
        );

        $this->createIndex(
            'idx-restaurant-types-restaurant_id',
            'restaurant_types',
            'restaurant_id'
        );

        $this->addForeignKey(
            'fk-restaurant-types-type_id',
            'restaurant_types',
            'type_id',
            'types',
            'type_id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-restaurant-types-restaurant_id',
            'restaurant_types',
            'restaurant_id',
            'restaurants',
            'restaurant_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-restaurant-types-type_id',
            'restaurant_types'
        );

        $this->dropIndex(
            'idx-restaurant-types-type_id',
            'restaurant_types'
        );

        $this->dropForeignKey(
            'fk-restaurant-types-restaurant_id',
            'restaurant_types'
        );

        $this->dropIndex(
            'idx-restaurant-types-restaurant_id',
            'restaurant_types'
        );

        $this->dropTable('restaurant_types');
    }
}
