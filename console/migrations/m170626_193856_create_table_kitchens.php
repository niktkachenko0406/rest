<?php

use yii\db\Migration;

class m170626_193856_create_table_kitchens extends Migration
{
    public function up()
    {
        $this->createTable('kitchens', [
            'kitchen_id'   => $this->primaryKey()->unsigned()->unique(),
            'kitchen_name' => $this->string(50)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('kitchens');
    }
}
