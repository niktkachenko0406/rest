<?php

use yii\db\Migration;

class m170626_195017_add_location_to_restaurants_table extends Migration
{
    public function up()
    {
        $this->addColumn('restaurants', 'city_id', $this->integer()->unsigned());
        $this->addColumn('restaurants', 'district_id', $this->integer()->unsigned());
        $this->addColumn('restaurants', 'restaurant_days', $this->string(255)->null());
    }

    public function down()
    {
        $this->dropColumn('restaurants', 'city_id');
        $this->dropColumn('restaurants', 'district_id');
        $this->dropColumn('restaurants', 'restaurant_days');
    }
}
