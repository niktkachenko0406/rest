<?php

use yii\db\Migration;

class m170703_193259_change_column_created_at_in_news extends Migration
{
    public function up()
    {
        $this->truncateTable('news');
        $this->alterColumn('news', 'created_at', $this->integer()->notNull());
    }

    public function down()
    {
        $this->alterColumn('news', 'created_at', $this->timestamp());
    }
}
