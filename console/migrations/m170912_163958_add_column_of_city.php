<?php

use yii\db\Migration;

class m170912_163958_add_column_of_city extends Migration
{
    public function up()
    {
        $this->addColumn('cities', 'of_city', $this->string(80)->null());
    }

    public function down()
    {
        $this->dropColumn('cities', 'of_city');
    }
}
