<?php

use yii\db\Migration;

class m170809_200211_change_column_rating extends Migration
{
    public function up()
    {
        $this->alterColumn('restaurants', 'restaurant_rating', $this->float()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->alterColumn('restaurants', 'restaurant_rating', 'tinyint(1) not null default 0');
    }
}
