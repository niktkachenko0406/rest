<?php

use yii\db\Migration;

class m170709_110337_change_column_created_at_in_blog extends Migration
{
    public function up()
    {
        $this->truncateTable('blog');
        $this->alterColumn('blog', 'created_at', $this->integer()->notNull());
    }

    public function down()
    {
        $this->truncateTable('blog');
        $this->alterColumn('blog', 'created_at', $this->timestamp());
    }
}
