<?php

use yii\db\Migration;

class m170608_180748_create_table_restaurants extends Migration
{
    public function up()
    {
        $this->createTable('restaurants', [
            'restaurant_id'            => $this->primaryKey()->unsigned()->unique(),
            'user_id'                  => $this->integer()->notNull(),
            'restaurant_name'          => $this->string(50)->null(),
            'restaurant_short_desc'    => $this->string(1024)->null(),
            'restaurant_desc'          => $this->text()->null(),
            'restaurant_average_check' => $this->integer()->null(),
            'restaurant_schedule'      => $this->string(1024)->null(),
            'restaurant_address'       => $this->string(255)->null(),
            'restaurant_avatar'        => $this->string(32)->null()
        ]);

        $this->createIndex(
            'idx-restaurant-user_id',
            'restaurants',
            'user_id'
        );

        $this->addForeignKey(
            'fk-restaurant-user_id',
            'restaurants',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-restaurant-user_id',
            'restaurants'
        );

        $this->dropIndex(
            'idx-restaurant-user_id',
            'restaurants'
        );

        $this->dropTable('restaurants');
    }
}
