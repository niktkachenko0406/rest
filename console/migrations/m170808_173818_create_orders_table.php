<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m170808_173818_create_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orders', [
            'order_id'         => $this->primaryKey(),
            'order_person'     => $this->integer()->notNull(),
            'user_id'          => $this->integer()->notNull(),
            'restaurant_id'    => $this->integer()->unsigned()->notNull(),
            'order_phone'      => $this->string()->notNull(),
            'order_date'       => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('orders');
    }
}
