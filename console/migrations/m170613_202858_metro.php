<?php

use yii\db\Migration;

class m170613_202858_metro extends Migration
{
    public function up()
    {
        $this->createTable('metro', [
            'station_id'   => $this->primaryKey()->unsigned()->unique(),
            'station_name' => $this->string(80)->notNull(),
            'branch_name'  => $this->string(80)->notNull(),
            'city_id'      => $this->integer()->unsigned()->notNull(),
            'district_id'  => $this->integer()->unsigned()->null(),
        ]);

        $this->createIndex(
            'idx-metro-city_id',
            'metro',
            'city_id'
        );

        $this->addForeignKey(
            'fk-metro-city_id',
            'metro',
            'city_id',
            'cities',
            'city_id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-metro-city_id',
            'metro'
        );

        $this->dropIndex(
            'idx-metro-city_id',
            'metro'
        );

        $this->dropTable('metro');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
