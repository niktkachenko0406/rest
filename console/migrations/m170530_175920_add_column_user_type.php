<?php

use yii\db\Migration;

class m170530_175920_add_column_user_type extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'user_type', $this->integer()->notNull()->after('status'));
    }

    public function down()
    {
        $this->dropColumn('user', 'user_type');
    }
}
