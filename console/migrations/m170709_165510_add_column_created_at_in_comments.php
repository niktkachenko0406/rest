<?php

use yii\db\Migration;

class m170709_165510_add_column_created_at_in_comments extends Migration
{
    public function up()
    {
        $this->truncateTable('comments');
        $this->addColumn('comments', 'created_at', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('comments', 'created_at');
    }
}
