<?php
 return [
     '/'         => 'site/index',
     '/contacts' => 'site/contact',

     [
         'pattern' => 'restaurant/<city:c-[a-z0-9_\-.]+>/<district:d-[a-z0-9_\-.]+>/<metro:m-[a-z0-9_\-.\s]+>/<type:t-[a-z0-9_\-.\s]+>/<kitchen:k-[a-z0-9_\-.\s]+>/<feature:f-[a-z0-9_\-.\s]+>',
         'route'    => 'restaurant/index',
         'defaults' => [
             'city'     => '',
             'district' => '',
             'metro'    => '',
             'type'     => '',
             'kitchen'  => '',
             'feature'  => '',
         ],
     ],

     [
         'pattern' => 'provider/<filter:prs-[a-z0-9_\-.\s]+>',
         'route'   => 'provider/index',
         'defaults' => [
             'filter' => '',
         ],
     ],

     '<controller:[a-zA-Z0-9\-]+>'                         => '<controller>/index',
//     '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
     '<controller:[a-zA-Z0-9\-]+>/<uri:r-[a-z0-9_\-.]+>'   => '<controller>/view',
     '<controller:[a-zA-Z0-9\-]+>/<uri:n-[a-z0-9_\-.]+>'   => '<controller>/view',
     '<controller:[a-zA-Z0-9\-]+>/<uri:b-[a-z0-9_\-.]+>'   => '<controller>/view',
 ];