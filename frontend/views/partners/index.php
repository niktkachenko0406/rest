<?php
/** @var Partners $partner */

use common\models\Partners;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Партнерам';

?>
<!-- Page header section start -->
<section class="page-title" style="background-image: url(img/partners-bg.jpg)">
    <div class="container">
        <h1 class="lora text-center">Партнерам</h1>
    </div>
</section>
<!-- Page header section end -->

<!-- Partners section start -->
<section class="partners">
    <div class="container">
        <div class="row partners-list">
            <?php foreach ($provider->getModels() as $partner): ?>
                <div class="post-item col-lg-3 col-md-4 col-sm-6">
                    <img src="<?= $partner->getImg(); ?>" alt="">
                    <ul class="post-item__tags list-unstyled">
                        <li class="post-tag"><a href="#">#мнение</a></li>
                    </ul>
                    <a href="<?= Url::to(['view', 'uri' => $partner->uri]); ?>" class="post-item__title"><?= $partner->partner_title; ?></a>
                </div>
            <?php endforeach; ?>
        </div>
        <nav class="post-navigation">
            <?= LinkPager::widget([
                'pagination' => $provider->pagination,
            ]);?>
        </nav>
    </div>
</section>
<!-- Partners section end -->
