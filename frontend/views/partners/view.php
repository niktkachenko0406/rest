<?php
/** @var Partners $partner */

use common\models\Partners;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $partner->partner_title;
?>


<!-- Page header section start -->
<section class="page-title" style="background-image: url(/img/new-bg.jpg)">
    <div class="container">
        <a href="<?= Url::to(['index']); ?>" class="page-title__back btn-stroke btn btn-sm animate pull-left">Назад</a>
        <h1 class="lora text-center">Партнерам</h1>
    </div>
</section>
<!-- Page header section end -->

<!-- New content section start -->
<section class="new-single">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="new-single__header">
                    <h2 class="new-single__title lora"><?= $partner->partner_title; ?></h2>
                    <div class="new-single__date"><?= Yii::$app->formatter->asDate($partner->created_at, 'php:d.m.Y'); ?></div>
                    <div class="new-single__image">
                        <img src="<?= $partner->getImg(); ?>" alt="">
                    </div>
                </div>
                <div class="new-single__content">
                    <?= $partner->partner_text; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- New content section end -->

<!-- Other news section start -->
<section class="other-news">
    <div class="container">
        <h2 class="lora text-center">Другие статьи</h2>
        <div class="news-list clearfix">
            <?php foreach ($lastPartners as $item): ?>
                <div class="grid-item grid-item-static grid-item--width3">
                    <a href="<?= Url::to(['view', 'uri' => $item->uri]); ?>" class="new-item">
                        <div class="new-item__thumb">
                            <img src="<?= $item->getImg(); ?>" alt="">
                        </div>
                        <div class="new-item__caption">
                            <h3 class="new-item__title lora"><?= $item->partner_title; ?></h3>
                            <div class="new-item__date"><?= Yii::$app->formatter->asDate($item->created_at, 'php:d.m.Y'); ?></div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- Other news section start -->