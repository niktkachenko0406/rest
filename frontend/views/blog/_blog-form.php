<?php
/** @var Blog $model */

use common\models\Blog;
use vova07\imperavi\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<div class="row">
    <?php $form = ActiveForm::begin([
        'id'     => 'blogForm',
        'action' => Url::to(['/blog/post-save', 'id' => $model->blog_id ?: null]),
        'method' => 'post'
    ]); ?>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-upload restaurant-profile-img">
                        <div class="upload-thumb upload-thumb-rest">
                            <img id="js-blog-preview" src="<?= $model->getAvatar(); ?>" alt="">
                        </div>
                        <div class="upload-btn animate">
                            Загрузить главное фото
                            <?= $form->field($model, 'blog_image', [
                                'options' => ['tag' => false]
                            ])->fileInput(['id' => 'js-blog-img'])->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="rest-form-sett">
                <?= $form->field($model, 'restaurant_id')->hiddenInput(['value' => $restId])->label(false); ?>

                <?= $form->field($model, 'blog_title')->textarea([
                    'class' => 'form-control',
                    'rows'  => '2'
                ]); ?>

                <?= $form->field($model, 'blog_text')->widget(Widget::className(), [
                    'settings' => [
                        'lang' => 'ru',
                        'minHeight' => 200,
                    ],
//                'class' => 'form-control',
//                'rows'  => '4'
                ]); ?>
                <button type="submit" class="animate btn btn-light btn-lg submit-btn">
                    <?php if($model->isNewRecord): ?>
                        Опубликовать
                    <?php else: ?>
                        Сохранить
                    <?php endif; ?>
                </button>
                <?php if($model->blog_status == Blog::POST_STATUS_ACTIVE): ?>
                    <a href="#" class="animate btn btn-light btn-lg">Посмотреть</a>
                <?php endif; ?>
            </div>

        </div>

    <?php ActiveForm::end(); ?>
</div>