<?php

/* @var $this \yii\web\View
 * @var $content string
 * @var User $user
 * @var UserProfile $userProfile
 */


use common\models\LoginForm;
use common\models\User;
use common\models\UserProfile;
use common\models\Subscribers;
use frontend\models\OrderForm;
use frontend\models\SignupForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;

AppAsset::register($this);

$user        = $this->context->user;
$userProfile = $this->context->user->userProfile;
$orderForm   = new OrderForm();
$subscriberForm = new Subscribers();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <!-- Fonts -->
    <link rel="stylesheet" href="/fonts/Roboto-font/Roboto-font.css">
    <link rel="stylesheet" href="/fonts/Lora-font/Lora-font.css">

    <!-- Icons -->
    <link rel="stylesheet" href="/fonts/Ionicons-icon/css/ionicons.min.css">
    <link rel="stylesheet" href="/fonts/icofont-icons/icofont-icons.css">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<header class="page-header <?php if(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index'): ?>header-home<?php else: ?>header-page<?php endif;?>">

    <!-- Top bar start -->
    <div class="top-bar">
        <div class="container">
            <div class="row row-middle">
                <div class="col-sm-5 col-lg-5 top-bar__search">
                    <form action="<?= Url::to(['/site/search']); ?>" method="get" class="search-form">
                        <input class="search-input" name="search" type="text" placeholder="Поиск">
                        <button type="submit" class="search-btn"><span class="ion-ios-search-strong"></span></button>
                        <input type="submit" value="" class="hidden">
                    </form>
                </div>
                <div class="col-sm-2 col-lg-2 top-bar__logo text-center">
                    <a href="/"><img src="/img/logo.png" alt=""></a>
                </div>
                <div class="col-sm-5 col-lg-5 top-bar__btns text-right">
                    <?php if(!Yii::$app->user->isGuest && $user->user_type != User::USER_TYPE_USER): ;?>
                        <div class="top-bar__menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-more"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= Url::to(['/partners/index'])?>">Партнерам</a></li>
                                <li><a href="<?= Url::to(['/provider/index'])?>">Каталог поставщиков</a></li>
                                <li><a href="<?= Url::to(['/site/consulting'])?>">Консалтинг</a></li>
    <!--                            <li><a href="order-table.html">Стол заказов</a></li>-->
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php if(Yii::$app->user->isGuest): ?>
                        <button class="animate btn btn-stroke btn-sm" data-toggle="modal" data-target="#login-modal">Служебный вход</button>
                        <button class="animate btn btn-stroke btn-sm" data-toggle="modal" data-target="#login-modal">Вход</button>
                        <button class="animate btn btn-stroke btn-sm" data-toggle="modal" data-target="#signin-modal">Регистрация</button>
                    <?php else: ?>
                        <div class="top-bar__user">
                            <div class="profile-avatar">
                                <img src="<?= $userProfile ? $userProfile->getAvatar() : null; ?>" alt="">
                            </div>
                            <div class="profile-link">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <?= $userProfile ? $user->getFullName() : $user->email; ?>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= $user->user_type == User::USER_TYPE_REST ? Url::to(['restaurant/profile']) : Url::to(['user/index'])?>">Профиль</a></li>
                                    <li><a href="<?=Url::to(['site/logout']); ?>">Выйти</a></li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Top bar end -->

    <!-- Main menu start -->
    <nav class="navbar main-menu">
        <div class="container">
            <div class="mob-menu-wrap">
                <a href="index.html" class="logo"><img src="../img/logo.png" alt=""></a>
                <span class="mob-menu ion-navicon"></span>
            </div>
            <ul class="nav navbar-nav">
                <span class="mob-menu ion-android-close"></span>
                <li><a href="/">Главная</a></li>
                <li><a href="<?= Url::to(['/restaurant/index']); ?>">Каталог ресторанов</a></li>
                <li><a href="<?= Url::to(['/news/index']); ?>">Новости</a></li>
                <li><a href="<?= Url::to(['/blog/index'])?>">Блог</a></li>
<!--                <li><a href="#">Как это работает</a></li>-->
                <li><a href="<?=Url::to(['site/contact']); ?>">Контакты</a></li>
            </ul>
        </div>
    </nav>
    <!-- Main menu end -->

</header>

<main class="page-content">
    <?= $content ?>
    <section class="subscribe">

            <div class="container">
                <div class="row row-middle">
                    <div class="col-md-6">
                        <h2 class="subscribe__title block-title block-title-white lora">Новое на Resto Spilnota<sup>&reg;</sup></h2>
                        <p class="subscribe__text">Подпишитесь на нашу рассылку, чтобы быть первым кто узнает о наших новостях.</p>
                    </div>

                    <div class="col-md-6">
                        <?php
                        $form = ActiveForm::begin([
                            'action'               => Url::to(['site/subscriber']),
                            'id'                   => 'subscriberForm',
                            'method'               => 'post',
                        ]);
                        ?>
                        <div class="input-group light-style">
                            <span class="email-input">
                                <input type="text" class="form-control js-subscriber-email" name="email" placeholder="Электронный адрес">
                            </span>
                            <span class="input-group-btn">
                                <button class="btn btn-default animate js-subscriber" type="button">Подписаться</button>
                            </span>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>

                </div>
            </div>
    </section>
</main>

<footer class="page-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-md-3 footer__logo">
                <a href="/"><img src="../img/logo-black.png" alt="" width="140"></a>
            </div>
            <div class="col-sm-7 col-md-6 footer__menu-right">
                <div class="clearfix">
                    <ul class="footer-menu list-unstyled">
                        <li><a href="/">Главная</a></li>
                        <li><a href="<?= Url::to(['/restaurant/index']); ?>">Каталог ресторанов</a></li>
                        <li><a href="<?= Url::to(['/news/index']); ?>">Новости</a></li>
                        <li><a href="<?= Url::to(['/blog/index'])?>">Блог</a></li>
<!--                        <li><a href="#">Как это работает?</a></li>-->
                        <li><a href="<?=Url::to(['site/contact']); ?>">Контакты</a></li>
                    </ul>
                    <ul class="footer-menu list-unstyled">
                        <li><a href="<?= Url::to(['/partners/index'])?>">Партнерам</a></li>
                        <li><a href="<?= Url::to(['/provider/index'])?>">Каталог поставщиков</a></li>
                        <li><a href="<?= Url::to(['/site/consulting'])?>">Консалтинг</a></li>
<!--                        <li><a href="#">Стол заказов</a></li>-->
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 footer__social-copy">
                <div class="footer-social">
                    <p>Социальные сети</p>
                    <ul class="social-list list-inline">
                        <li><a href="#" class="ion-social-instagram-outline"></a></li>
                        <li><a href="#" class="ion-social-vimeo"></a></li>
                        <li><a href="#" class="ion-social-pinterest-outline"></a></li>
                        <li><a href="#" class="ion-social-facebook"></a></li>
                        <li><a href="#" class="ion-social-twitter"></a></li>
                    </ul>
                </div>
                <div class="footer-copy">
                    &copy; 2017 Resto Spilnota<sup>&reg;</sup>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="to-top animate"><span class="ion-android-arrow-up"></span></div>

<!-- Login popup  -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <?=$this->render('/partials/login', ['model' => new LoginForm()]); ?>
</div>

<!-- Registr popup  -->
<div class="modal fade" id="signin-modal" tabindex="-1" role="dialog" aria-labelledby="signinModalLabel">
    <?=$this->render('/partials/register', ['model' => new SignupForm()]); ?>
</div>

<!-- Order popup  -->
<div class="modal fade" id="order-modal" tabindex="-1" role="dialog" aria-labelledby="signinModalLabel">
    <div class="modal-dialog js-order-form" role="document">

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
