<?php
/** @var ProviderCategories $category */
/** @var ProviderSubcategories $subCategory */

use common\models\ProviderCategories;
use common\models\ProviderSubcategories;

$subcategories = ProviderSubcategories::getSubCategoriesByUri($params);

?>

<?php foreach (ProviderCategories::getCategories() as $category): ?>
<div class="form-group">
    <div class="dropdown">
        <label><?= $category->category_name; ?>:</label>
        <a class="filter-dropdown-toggle dropdown-toggle">
           <?php foreach ($subcategories as $subcategory): ?>
               <?php if($subcategory->category_id == $category->category_id): ?>
                   <?= $subcategory->subcategory_name; ?>,
               <?php endif; ?>
           <?php endforeach; ?>
        </a>
        <ul class="filter-dropdown dropdown-menu js-filter-provider" data-params="<?= htmlspecialchars(json_encode($params)); ?>">
            <?php $filters = $params['filter'] ? explode(' ', $params['filter']) : []; ?>
            <?php foreach($category->providerSubcategories as $subCategory): ?>
                <div class="checkbox-box show-item filter-link">
                    <input type="checkbox"
                           class="checkbox js-filter-val"
                           name="filter"
                           <?php if(in_array($subCategory->uri, $filters)): ?>checked="checked"<?php endif; ?>
                           value="<?= $subCategory->uri?>"
                           id="filterType<?=$subCategory->subcategory_id?>" />

                    <label class="js-filter-checked" for="filterType<?=$subCategory->subcategory_id?>">
                        <a href="#"><?=$subCategory->subcategory_name?></a>
                    </label>
                </div>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php endforeach; ?>