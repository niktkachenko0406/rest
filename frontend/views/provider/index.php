<?php
use frontend\assets\ProviderAsset;
use kop\y2sp\ScrollPager;
use yii\helpers\Url;

ProviderAsset::register($this);

$this->title = 'Каталог поставщиков';
?>
<!-- Suppliers section start -->
<section class="suppliers">
    <div class="container">
        <h2 class="block-title lora text-center">Каталог поставщиков</h2>

        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <form id="js-provider-filter-form" action="">
                        <?= $this->render('_filter', ['params' => $params]); ?>
                    </form>
                </div>
            </div>
            <div class="col-md-9">
<!--                <div class="result-panel clearfix">-->
<!--                    <div class="map-show text-right">-->
<!--                        <button class="btn btn-stroke btn-sm animate">Показать на карте</button>-->
<!--                    </div>-->
<!--                </div>-->

                <ul class="list-unstyled suppliers-list">
                    <?php foreach ($providers->getModels() as $provider): ?>
                        <li class="supplier-item animate">
                            <div class="supplier-item__thumb">
                                <a href="<?= Url::to(['view', 'uri' => $provider->uri]); ?>">
                                    <img src="<?= $provider->getImg(); ?>" alt="">
                                </a>
                            </div>
                            <div class="supplier-item__content">
                                <h3 class="lora supplier-item__title">
                                    <a href="<?= Url::to(['view', 'uri' => $provider->uri]); ?>"><?= $provider->provider_name; ?></a>
                                </h3>
                                <div class="supplier-item__desc"><?= $provider->provider_short_desc; ?></div>
                                <ul class="list-unstyled supplier-item__contacts">
                                    <li class="phone"><a href="#"><?= $provider->provider_phone; ?></a></li>
                                    <li class="email">
                                        <a href="mailto:<?= $provider->provider_email; ?>">
                                            <?= $provider->provider_email; ?>
                                        </a>
                                    </li>
                                    <li class="web">
                                        <a href="http://<?= $provider->provider_site; ?>" target="_blank"><?= $provider->provider_site; ?></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <?= ScrollPager::widget([
                    'pagination'      => $providers->pagination,
                    'container'       => '.list-unstyled',
                    'item'            => '.supplier-item',
                    'triggerTemplate' => '<a href="javascript:void(0)" class="load-more  animate">Показать еще</a>',
                    'noneLeftText'    => ''
                ]);?>

            </div>
        </div>
    </div>
</section>
<!-- Suppliers section end -->