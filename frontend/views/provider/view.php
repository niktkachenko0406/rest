<?php
/** @var Providers $provider */
use common\models\Providers;

$this->title = $provider->provider_name;
?>
<!-- Supplier header section start -->
<section class="rest-header supplier-header" style="background-image: url(/img/supplier-bg.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 class="rest-header__title lora"><?= $provider->provider_name; ?></h1>
            </div>
            <div class="col-md-4">
                <div class="abs-box supplier-contacts">
                    <h3 class="lora">Контакты</h3>
                    <ul class="list-unstyled supplier-item__contacts">
                        <li class="phone"><a href="#"><?= $provider->provider_phone; ?></a></li>
                        <li class="email">
                            <a href="mailto:<?= $provider->provider_email; ?>">
                                <?= $provider->provider_email; ?>
                            </a>
                        </li>
                        <li class="web">
                            <a href="http://<?= $provider->provider_site; ?>" target=«_blank» ><?= $provider->provider_site; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Supplier header section end -->

<!-- Supplier content section start -->
<section class="supplier-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="supplier-products">
                    <div class="new-single__content">
                        <?= $provider->provider_desc; ?>
                    </div>
                    <h3 class="lora">Товары компании</h3>
                    <ul class="list-unstyled products-list row">
                        <?php foreach ($products as $product): ?>
                            <li class="product-item col-lg-3 col-md-4 col-sm-6">
                                <img src="<?= $product->getImg(); ?>" alt="">
                                <div class="product-item__title"><?= $product->product_title; ?></div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="rest-content__map">
                    <div class="map-address js-rest-address" data-lat="<?= $coordinates['lat']?>" data-lng="<?= $coordinates['lng']?>">
                        <?= $provider->city->city_name; ?>, <?= $provider->provider_address; ?>
                    </div>
                    <div id="rest-map" class="google-map"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Supplier content section end -->