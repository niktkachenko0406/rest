<?php
/**
 * @var Restaurants $restaurant
 * @var View $this
 */

use common\models\Comments;
use common\models\Restaurants;
use frontend\assets\RestaurantAsset;
use kop\y2sp\ScrollPager;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;

$this->title = 'Рестораны';

RestaurantAsset::register($this);

$params = Yii::$app->request->get();

if($sort == 'price' || $sort == 'date' || $sort == 'rating') {
    $class = 'label animate active';
}
?>
<section class="search-rest-form js-params" data-params="<?= htmlspecialchars(json_encode($params)); ?>">
    <div class="container">
        <form action="<?= Url::to(['restaurant/index']); ?>" class="filter-form form-grey no-btn clearfix">
            <div class="form-group filter-form__persons">
                <select name="person" id="filter-persons" class="chosen-select filter-select js-person">
                    <option <?php if(isset($params['person']) && $params['person'] == 1): ?>selected="selected"<?php endif; ?> value="1">1 персона</option>
                    <option <?php if(isset($params['person']) && $params['person'] == 2): ?>selected="selected"<?php endif; ?> value="2">2 персоны</option>
                    <option <?php if(isset($params['person']) && $params['person'] == 4): ?>selected="selected"<?php endif; ?> value="4">4 персоны</option>
                    <option <?php if(isset($params['person']) && $params['person'] == 6): ?>selected="selected"<?php endif; ?> value="6">6 персон</option>
                    <option <?php if(isset($params['person']) && $params['person'] == 10): ?>selected="selected"<?php endif; ?> value="10">10 персон</option>
                </select>
            </div>
            <div class="form-group filter-form__date">
                <input type="text"
                       name="date"
                       class="form-control datetimepicker js-date"
                       placeholder="Выберите дату"
                       value="<?= isset($params['date']) ? $params['date'] : null?>"
                       data-loc="left">
            </div>
            <div class="form-group filter-form__loc">
                <input type="text"
                       name="search"
                       class="form-control js-search"
                       value="<?= isset($params['search']) ? $params['search'] : null?>"
                       placeholder="Улица, бар, кальян...">
            </div>
        </form>
    </div>
</section>

<!-- New restaurants section start -->
<section class="restaurants-result">
    <div class="container">
        <h2 class="block-title lora text-center">Каталог заведений Киева</h2>
        <div class="row">
            <div class="col-md-3">
                <div class="sidebar">
                    <?= Html::beginForm('', 'get', [
                        'id' => 'filterForm',
                    ]); ?>
                        <?= $this->render('_filters', ['params' => $params]); ?>
                    <?= Html::endForm(); ?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="result-panel clearfix">
                    <div class="sort">
                        <label>Сортировать по:</label>
                        <div class="sort-variant list-inline">
                            <?= $provider->getSort()->link('rating', [
                                'class' => ($sort == 'rating') ? $class : 'label animate',
                            ]); ?>
                            <?= $provider->getSort()->link('price', [
                                'class' => ($sort == 'price') ? $class : 'label animate',
                            ]); ?>
                            <?= $provider->getSort()->link('date', [
                                'class' => ($sort == 'date') ? $class : 'label animate',
                            ]); ?>
                        </div>
                    </div>
                    <div class="map-show text-right">
                        <button class="btn btn-stroke btn-sm animate <?php if($params['city']): ?>js-show-map active<?php endif; ?>">Показать на карте</button>
                    </div>
                </div>

                <div id="js-rest-list-map" class="rest-list-map"></div>

                <ul class="list-unstyled restaur-list">
                    <?php foreach ($provider->getModels() as $restaurant): ?>
                    <li class="restaurant-item animate">
                        <div class="restaurant-img">
                            <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri]); ?>">
                                <img src="<?= $restaurant->getAvatar(); ?>" alt="">
                            </a>
                            <div class="restaurant-img__icons">
                                <a href="#" class="restaurant-like animate js-favorites <?= in_array($restaurant->restaurant_id, $favorites) ? 'active' : '' ?>"
                                    data-rest-id="<?= $restaurant->restaurant_id ?>"
                                    data-status="<?= in_array($restaurant->restaurant_id, $favorites) ? 1 : 0 ?>">
                                    <span class="ion-ios-heart-outline"></span>
                                </a>
                                <div class="restaurant-rating"><?= $restaurant->restaurant_rating; ?></div>
                                <a href="#" class="restaurant-comments animate"><?= Comments::countComments($restaurant->restaurant_id) ?></a>
                            </div>
                        </div>
                        <div class="restaurant-caption">
                            <div class="restaurant__head">
                                <h3 class="restaurant__title lora">
                                    <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri]); ?>"><?= $restaurant->restaurant_name; ?></a>
                                </h3>
                                <div class="restaurant__text"><?= $restaurant->restaurant_short_desc; ?></div>
                            </div>
                            <div class="restaurant__footer">
                                <div class="restaurant__loc"><?= $restaurant->restaurant_address; ?></div>
                                <a href="#"
                                   class="btn btn-default animate js-rest-order"
                                   data-toggle="modal"
                                   data-rest-id="<?= $restaurant->restaurant_id; ?>"
                                   data-target="#order-modal">Зарезервировать стол</a>
                            </div>
                        </div>
                    </li>
                    <?php endforeach; ?>
                </ul>

                <?= ScrollPager::widget([
                    'pagination'      => $provider->pagination,
                    'container'       => '.list-unstyled',
                    'item'            => '.restaurant-item',
                    'triggerTemplate' => '<a href="javascript:void(0)" class="load-more load-more-sm animate">Показать еще</a>',
                    'noneLeftText'    => ''
                ]);?>


            </div>
        </div>
    </div>
</section>
<!-- New restaurants section end -->