<?php
/**
 * @var Restaurants $restaurant
 * @var Comments $comment
 * @var OrderForm $orderModel
 */

use common\models\Cities;
use common\models\Comments;
use common\models\Restaurants;
use frontend\models\OrderForm;
use frontend\assets\RestaurantAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = $restaurant->restaurant_name;
$userProfile = $user->userProfile;

RestaurantAsset::register($this);
?>

<!-- Restaurant header section start -->
<section class="rest-header" style="background-image: url(<?= $restaurant->getAvatar(); ?>)">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="rest-header__rating restaurant-rating"><?= $restaurant->getRating(); ?></div>
                <h1 class="rest-header__title lora"><?= $restaurant->restaurant_name; ?></h1>
            </div>
        </div>
    </div>
</section>
<!-- Restaurant header section end -->

<!-- Restaurant content section start -->
<section class="rest-content">
    <div class="container">
        <div class="row ordex-box-fix-wrap">
            <div class="col-md-4 col-md-push-8 ordex-box-fix">
                <div class="rest-header__order abs-box form-grey">
                    <?php $form = ActiveForm::begin([
                        'id'     => 'orderForm',
                        'action' => Url::to(['/restaurant/order']),
                        'method' => 'post',
                        'class'  => 'form-grey'
                    ]); ?>
                        <div class="row row-10">
                            <div class="col-10 col-sm-5 col-md-12 col-lg-5">
                                <?= $form->field($orderModel, 'order_person')->dropDownList($orderModel->personsList(),
                                    [
                                        'id'    => 'filter-persons',
                                        'class' => 'chosen-select filter-select'
                                    ]
                                )->label(false)?>
                            </div>
                            <div class="col-10 col-sm-7 col-md-12 col-lg-7">
                                <?= $form->field($orderModel, 'order_date')->textInput([
                                    'class'       => 'form-control datetimepicker',
                                    'placeholder' => '2 января, 12:00',
                                    'data-loc'    => 'left'
                                ])->label(false); ?>
                            </div>
                        </div>

                        <?= $form->field($orderModel, 'user_name')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'Имя'
                        ])->label(false)?>

                        <?= $form->field($orderModel, 'user_surname')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'Фамилия'
                        ])->label(false)?>

                        <?= $form->field($orderModel, 'user_email')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'Email'
                        ])->label(false)?>

                        <?= $form->field($orderModel, 'order_phone')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'Телефон'
                        ])->label(false)?>

                        <?= $form->field($orderModel, 'restaurant_id')->hiddenInput(
                            ['value' => $restaurant->restaurant_id]
                        )->label(false); ?>

                        <?= $form->field($orderModel, 'user_id')->hiddenInput()->label(false); ?>

                        <button class="animate btn btn-primary btn-lg btn-block order-btn">Зарезирвировать стол</button>

                        <div class="form-group-inline rest-header__order-rating">
                            <label>Рейтинг:</label>
                            <div class="star-rating"><span style="width:<?= $restaurant->getRatingPercent(); ?>%"></span></div>
                        </div>
                        <div class="form-group-inline">
                            <label>Средний чек:</label>
                            <b><?= $restaurant->restaurant_average_check; ?> грн.</b>
                        </div>
                        <a href="#" class="restaurant-like animate js-favorites <?= in_array($restaurant->restaurant_id, $favorites) ? 'active' : '' ?>"
                           data-rest-id="<?= $restaurant->restaurant_id ?>"
                           data-status="<?= in_array($restaurant->restaurant_id, $favorites) ? 1 : 0 ?>">
                            <span class="ion-ios-heart-outline"></span>
                        </a>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-md-8 col-md-pull-4">
                <div class="rest-content__gallery clearfix">
                    <?php foreach ($restaurant->gallery as $gallery): ?>
                        <a href="<?= $gallery->getImg('big'); ?>" data-lightbox="rest-gall-group" class="rest-gall">
                            <img src="<?= $gallery->getImg('preview'); ?>" alt="">
                        </a>
                    <?php endforeach; ?>
                </div>
                <div class="rest-content__tags">
                    <a href="<?= Url::to(['/restaurant/index', 'city' => $restaurant->city->uri]); ?>"
                       class="label animate">
                        <?= $restaurant->city->city_name; ?>
                    </a>
                    <a href="<?= Url::to(['/restaurant/index', 'district' => $restaurant->district->uri]); ?>"
                       class="label animate">
                        <?= $restaurant->district->district_name; ?>
                    </a>

                    <?php foreach ($restaurant->metro as $metro): ?>
                        <a href="<?= Url::to(['/restaurant/index', 'metro' => $metro->uri]); ?>"
                           class="label animate">
                            <?= $metro->station_name ?>
                        </a>
                    <?php endforeach; ?>

                    <?php foreach ($restaurant->types as $type): ?>
                        <a href="<?= Url::to(['/restaurant/index', 'type' => $type->uri]); ?>"
                           class="label animate">
                            <?= $type->type_name ?>
                        </a>
                    <?php endforeach; ?>

                    <?php foreach ($restaurant->kitchen as $kitchen): ?>
                        <a href="<?= Url::to(['/restaurant/index', 'kitchen' => $kitchen->uri]); ?>"
                           class="label animate">
                            <?= $kitchen->kitchen_name ?>
                        </a>
                    <?php endforeach; ?>

                    <?php foreach ($restaurant->features as $feature): ?>
                        <a href="<?= Url::to(['/restaurant/index', 'feature' => $feature->uri]); ?>"
                           class="label animate">
                            <?= $feature->feature_name ?>
                        </a>
                    <?php endforeach; ?>
                </div>
                <div class="rest-content__desc">
                    <h3 class="lora">Описание ресторана</h3>
                    <div class="rest-description">
                        <?= $restaurant->restaurant_desc; ?>
                    </div>
                </div>
                <div class="rest-content__map">
                    <div class="map-address js-rest-address" data-lat="<?= $coordinates['lat']?>" data-lng="<?= $coordinates['lng']?>">
                        <?= $restaurant->city->city_name; ?>, <?= $restaurant->restaurant_address; ?>
                    </div>
                    <div id="rest-map" class="google-map"></div>
                </div>
                <div class="rest-content__menu">
                    <button class="animate btn btn-primary btn-lg show-menu-btn" data-toggle="collapse" data-target="#menuImage">Посмотреть меню</button>
                    <div class="menu-img collapse" id="menuImage">
                        <img src="/img/nemu-thumb.jpg" alt="">
                    </div>
                </div>
                <div class="rest-content__reviews">
                    <h3 class="lora">Отзывы <span class="badge"><?= count($restaurant->comments); ?></span></h3>
                    <div class="row">
                        <?php if(!empty($restaurant->comments)): ?>
                            <?php foreach ($restaurant->comments as $comment): ?>
                                <?php
                                    $author        = $comment->user;
                                    $authorProfile = $author->userProfile;
                                ?>
                                <div class="col-lg-6">
                                    <div class="review-item">
                                        <div class="review-item__header">
                                            <div class="review-item__title">
                                                <img src="<?= $authorProfile ? $authorProfile->getAvatar() : '/img/avatar-default.jpg' ?>" alt="" class="review-item__avatar">
                                                <div class="review-item__name"><?= $authorProfile ? $authorProfile->getFullName() : $author->email ?></div>
                                            </div>
                                            <div class="review-item__text">
                                                <?= $comment->comment_text; ?>
                                            </div>
                                        </div>
                                        <div class="review-item__footer">
                                            <div class="form-group-inline">
                                                <label>Кухня:</label>
                                                <div class="star-rating">
                                                    <span style="width:<?= $comment->getRatingPercent('comment_kitchen_rating')?>%"></span>
                                                </div>
                                            </div>
                                            <div class="form-group-inline">
                                                <label>Интерьер:</label>
                                                <div class="star-rating">
                                                    <span style="width:<?= $comment->getRatingPercent('comment_interier_rating')?>%"></span>
                                                </div>
                                            </div>
                                            <div class="form-group-inline">
                                                <label>Обслуживание:</label>
                                                <div class="star-rating">
                                                    <span style="width:<?= $comment->getRatingPercent('comment_service_rating')?>%"></span>
                                                </div>
                                            </div>
                                            <div class="form-group-inline">
                                                <label>Атмосфера:</label>
                                                <div class="star-rating">
                                                    <span style="width:<?= $comment->getRatingPercent('comment_ambience_rating')?>%"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <?php if(Yii::$app->user->isGuest): ?>
                        <div class="review-item no-login text">Чтобы добавить отзыв,
                            <a href="#" data-toggle="modal" data-target="#login-modal">войдите</a> или
                            <a href="#" data-toggle="modal" data-target="#signin-modal">зарегестрируйтесь</a>.
                        </div>
                    <?php else: ?>
                        <?php $form = \yii\bootstrap\ActiveForm::begin([
                            'id'                     => 'commentsForm',
                            'action'                 => Url::to(['/restaurant/save-comment', 'uri' => $restaurant->uri]),
                            'method'                 => 'post',
                            'enableClientValidation' => true,
                            'options' => [
                                'validateOnSubmit' => true,
                            ]
                        ]); ?>
                            <div class="review-item add-review">
                                <div class="review-item__header">
                                    <div class="review-item__title">
                                        <img src="<?= $userProfile ? $userProfile->getAvatar() : '/img/avatar-default.jpg' ?>" alt="" class="review-item__avatar">
                                        <div class="review-item__name"><?= $userProfile ? $userProfile->getFullName() : $user->email ?></div>
                                    </div>
                                    <div class="review-item__text">
                                        <?= $form->field($commentsModel, 'comment_text')->textarea([
                                            'class'       => 'string-textarea',
                                            'placeholder' => 'Напишите отзыв...'
                                        ])->label(false); ?>
                                    </div>
                                </div>
                                <?= $form->field($commentsModel, 'entity_id')->hiddenInput([
                                        'value' => $restaurant->restaurant_id
                                ])->label(false); ?>
                                <div class="review-item__footer clearfix">
                                    <div class="form-group-inline">
                                        <label for="kitchen-rating">Кухня:</label>
                                        <div class="stars-rew">
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                        </div>
                                        <?= $form->field($commentsModel, 'comment_kitchen_rating', [
                                            'options' => ['tag' => false]
                                        ])->hiddenInput([
                                            'value' => 0,
                                            'class' => 'rating-val',
                                            'id'    => 'kitchen-rating'
                                        ])->label(false); ?>
                                    </div>
                                    <div class="form-group-inline">
                                        <label for="interier-rating">Интерьер:</label>
                                        <div class="stars-rew">
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                        </div>
                                        <?= $form->field($commentsModel, 'comment_interier_rating', [
                                            'options' => ['tag' => false]
                                        ])->hiddenInput([
                                            'value' => 0,
                                            'class' => 'rating-val',
                                            'id'    => 'interier-rating'
                                        ])->label(false); ?>
                                    </div>
                                    <div class="form-group-inline">
                                        <label for="service-rating">Обслуживание:</label>
                                        <div class="stars-rew">
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                        </div>
                                        <?= $form->field($commentsModel, 'comment_service_rating', [
                                            'options' => ['tag' => false]
                                        ])->hiddenInput([
                                            'value' => 0,
                                            'class' => 'rating-val',
                                            'id'    => 'service-rating'
                                        ])->label(false); ?>
                                    </div>
                                    <div class="form-group-inline">
                                        <label for="ambience-rating">Атмосфера:</label>
                                        <div class="stars-rew">
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                            <span class="star ion-ios-star-outline"></span>
                                        </div>
                                        <?= $form->field($commentsModel, 'comment_ambience_rating', [
                                            'options' => ['tag' => false]
                                        ])->hiddenInput([
                                            'value' => 0,
                                            'class' => 'rating-val',
                                            'id'    => 'ambience-rating'
                                        ])->label(false); ?>
                                    </div>
                                </div>
                            </div>
                            <?= Html::button('Добавить отзыв', [
                                'class' => 'animate btn btn-light btn-lg btn-block',
                                'type'  => 'submit'
                            ])?>
                        <?php \yii\bootstrap\ActiveForm::end(); ?><br>
                        <?php if($message = Yii::$app->session->getFlash('comment')): ?>
                            <div class="alert alert-success" role="alert"><?= $message ?></div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Restaurant content section end -->

<!-- Similar restourants section start -->
<section class="similar-rest">
    <div class="container">
        <h3 class="lora">Похожие рестораны</h3>
        <ul class="list-unstyled restaur-list four-col">
            <?php if($similars): ?>
                <?php foreach ($similars as $restaurant): ?>
                    <li class="restaurant-item animate">
                        <div class="restaurant-img">
                            <img src="<?= $restaurant->getAvatar(); ?>" alt="">
                            <div class="restaurant-img__icons">
                                <a href="#" class="restaurant-like animate js-favorites <?= in_array($restaurant->restaurant_id, $favorites) ? 'active' : '' ?>"
                                   data-rest-id="<?= $restaurant->restaurant_id ?>"
                                   data-status="<?= in_array($restaurant->restaurant_id, $favorites) ? 1 : 0 ?>">
                                    <span class="ion-ios-heart-outline"></span>
                                </a>
                                <div class="restaurant-rating"><?= $restaurant->restaurant_rating; ?></div>
                                <a href="#" class="restaurant-comments animate"><?= Comments::countComments($restaurant->restaurant_id) ?></a>
                            </div>
                        </div>
                        <div class="restaurant-caption">
                            <div class="restaurant__head">
                                <h3 class="restaurant__title lora">
                                    <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri]); ?>">
                                        <?= $restaurant->restaurant_name; ?>
                                    </a>
                                </h3>
                                <div class="restaurant__text"><?= $restaurant->restaurant_short_desc; ?></div>
                            </div>
                            <div class="restaurant__footer">
                                <div class="restaurant__loc"><?= $restaurant->restaurant_address; ?></div>
                                <a href="#"
                                   class="btn btn-default animate js-rest-order"
                                   data-rest-id="<?= $restaurant->restaurant_id; ?>"
                                   data-toggle="modal"
                                   data-target="#order-modal">Зарезервировать стол
                                </a>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</section>
<!-- Similar restourants section end -->