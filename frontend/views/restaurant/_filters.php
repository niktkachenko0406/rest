<?php
/** @var Cities $city */
/** @var Districts $district */
/** @var Metro $metro */
/** @var Types $type */
/** @var Kitchens $kitchen */
/** @var Features $feature */
/** @var Reasons $reason */

use common\models\Cities;
use common\models\Districts;
use common\models\Features;
use common\models\Kitchens;
use common\models\Metro;
use common\models\Reasons;
use common\models\Types;
use yii\helpers\Url;

?>
<div class="form-group">
    <div class="dropdown">
        <label>Город:</label>
        <a class="filter-dropdown-toggle dropdown-toggle">
            <?= $params['city'] ? Cities::getCityName(null, $params['city']) : ''?>
        </a>
        <ul class="filter-dropdown dropdown-menu">
            <?php foreach (Cities::getCities(true) as $key => $city): ?>
                <div class="filter-link">
                    <a href="<?= Url::to(['index', 'city' => $key]); ?>">
                        <?= $city; ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<?php if($params['city']): ?>
    <div class="form-group">
        <div class="dropdown">
            <label>Район:</label>
            <a class="filter-dropdown-toggle dropdown-toggle">
                <?= $params['district'] ? Districts::getDistrictName(null, $params['district']) : ''?>
            </a>
            <ul class="filter-dropdown dropdown-menu">
                <?php foreach (Districts::getDistricts(Cities::getCityId($params['city'])) as $district): ?>
                    <div class="filter-link">
                        <a href="<?= Url::to(array_merge($params, ['index', 'district' => $district->uri])); ?>"><?= $district->district_name; ?></a>
                    </div>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>

<?php if($params['city']): ?>
    <div class="form-group">
        <div class="dropdown">
            <label>Метро:</label>
            <a class="filter-dropdown-toggle dropdown-toggle">
                <?= $params['metro'] ? Metro::getMetroNames($params['metro']) : ''; ?>
            </a>
            <ul class="filter-dropdown dropdown-menu js-filter-metro" data-params="<?= htmlspecialchars(json_encode($params)); ?>">
                <?php $stations = $params['metro'] ? explode(' ', $params['metro']) : []; ?>
                <?php foreach (Metro::getMetroByCity(Cities::getCityId($params['city'])) as $metro): ?>
                    <div class="checkbox-box show-item filter-link">
                        <input type="checkbox"
                               class="checkbox js-metro-val"
                               name="metro[]"
                               <?php if(in_array($metro->uri, $stations)): ?>checked="checked"<?php endif; ?>
                               value="<?= $metro->uri?>"
                               id="filterType<?=$metro->station_id?>" />

                        <label class="js-metro-checked" for="filterType<?=$metro->station_id?>">
                            <a href="<?= Url::to(array_merge($params, ['index', 'metro' => $metro->uri])); ?>"><?= $metro->station_name; ?></a>
                        </label>
                    </div>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>

<div class="form-group">
    <div class="dropdown">
        <label>Тип заведения:</label>
        <a class="filter-dropdown-toggle dropdown-toggle">
            <?= $params['type'] ? Types::getTypeNames($params['type']) : ''; ?>
        </a>
        <ul class="filter-dropdown dropdown-menu js-filter-type" data-params="<?= htmlspecialchars(json_encode($params)); ?>">
            <?php $types = $params['type'] ? explode(' ', $params['type']) : []; ?>
            <?php foreach (Types::getTypes() as $type): ?>
                <div class="checkbox-box show-item filter-link">
                    <input type="checkbox"
                           class="checkbox js-type-val"
                           name="type[]"
                           <?php if(in_array($type->uri, $types)): ?>checked="checked"<?php endif; ?>
                           value="<?= $type->uri?>"
                           id="filterType<?=$type->type_id?>" />

                    <label class="js-type-checked" for="filterType<?=$type->type_id?>">
                        <a href="<?= Url::to(array_merge($params, ['index', 'type' => $type->uri])); ?>"><?= $type->type_name; ?></a>
                    </label>
                </div>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<div class="form-group">
    <div class="dropdown">
        <label>Кухня:</label>
        <a class="filter-dropdown-toggle dropdown-toggle">
            <?= $params['kitchen'] ? Kitchens::getKitchenNames($params['kitchen']) : ''; ?>
        </a>
        <ul class="filter-dropdown dropdown-menu js-filter-kitchen" data-params="<?= htmlspecialchars(json_encode($params)); ?>">
            <?php $kitchens = isset($params['kitchen']) ? explode(' ', $params['kitchen']) : []; ?>
            <?php foreach (Kitchens::getKitchens() as $kitchen): ?>
                <div class="checkbox-box show-item filter-link">
                    <input type="checkbox"
                           class="checkbox js-kitchen-val"
                           name="kitchen[]"
                           <?php if(in_array($kitchen->uri, $kitchens)): ?>checked="checked"<?php endif; ?>
                           value="<?= $kitchen->uri?>"
                           id="filterType<?=$kitchen->kitchen_id?>" />

                    <label class="js-kitchen-checked" for="filterType<?=$kitchen->kitchen_id?>">
                        <a href="<?= Url::to(array_merge($params, ['index', 'kitchen' => $kitchen->uri])); ?>"><?= $kitchen->kitchen_name; ?></a>
                    </label>
                </div>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<div class="form-group">
    <div class="dropdown">
        <label>Повод:</label>
        <a class="filter-dropdown-toggle dropdown-toggle">
            <?= $params['reason'] ? Reasons::getReasonNames($params['reason']) : ''; ?>
        </a>
        <ul class="filter-dropdown dropdown-menu js-filter-reason" data-params="<?= htmlspecialchars(json_encode($params)); ?>">
            <?php $reasons = $params['reason'] ? explode(' ', $params['reason']) : []; ?>
            <?php foreach (Reasons::getReasons() as $reason): ?>
                <div class="checkbox-box show-item filter-link">
                    <input type="checkbox"
                           class="checkbox js-reason-val"
                           name="reason[]"
                           <?php if(in_array($reason->uri, $reasons)): ?>checked="checked"<?php endif; ?>
                           value="<?= $reason->uri?>"
                           id="filterType<?= $reason->reason_id?>" />

                    <label class="js-reason-checked" for="filterType<?= $reason->reason_id?>">
                        <a href="<?= Url::to(array_merge($params, ['index', 'reason' => $reason->uri])); ?>"><?= $reason->reason_name; ?></a>
                    </label>
                </div>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<div class="form-group">
    <div class="dropdown">
        <label>Особенности:</label>
        <a class="filter-dropdown-toggle dropdown-toggle">
            <?= $params['feature'] ? Features::getFeatureNames($params['feature']) : ''; ?>
        </a>
        <ul class="filter-dropdown dropdown-menu js-filter-feature" data-params="<?= htmlspecialchars(json_encode($params)); ?>">
            <?php $features = $params['feature'] ? explode(' ', $params['feature']) : []; ?>
            <?php foreach (Features::getFeatures() as $feature): ?>
                <div class="checkbox-box show-item filter-link">
                    <input type="checkbox"
                           class="checkbox js-feature-val"
                           name="feature[]"
                           <?php if(in_array($feature->uri, $features)): ?>checked="checked"<?php endif; ?>
                           value="<?= $feature->uri?>"
                           id="filterType<?=$feature->feature_id?>" />

                    <label class="js-feature-checked" for="filterType<?=$feature->feature_id?>">
                        <a href="<?= Url::to(array_merge($params, ['index', 'feature' => $feature->uri])); ?>"><?= $feature->feature_name; ?></a>
                    </label>
                </div>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

