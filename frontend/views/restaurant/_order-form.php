<?php
/** @var Restaurants $restaurant */
/** @var OrderForm $orderModel */

use common\models\Restaurants;
use frontend\models\OrderForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title text-center lora" id="signinModalLabel">Зарезирвировать стол</h2>
    </div>
    <div class="modal-body order-modal">
        <?php $form = ActiveForm::begin([
            'id'     => 'orderForm',
            'action' => Url::to(['/restaurant/order']),
            'method' => 'post',
            'class'  => 'form-grey'
        ]); ?>
            <div class="row row-10">
                <div class="col-10 col-sm-5">
                    <?= $form->field($orderModel, 'order_person')->dropDownList($orderModel->personsList(),
                        [
                            'id'     => 'filter-persons',
                            'prompt' => 'Выбрать',
                            'class'  => 'chosen-select filter-select js-order-persons'
                        ]
                    )->label(false)?>
                </div>
                <div class="col-10 col-sm-7">
                    <?= $form->field($orderModel, 'order_date')->textInput([
                        'class'       => 'form-control datetimepicker js-order-date',
                        'placeholder' => 'Выберите дату',
                        'data-loc'    => 'left'
                    ])->label(false); ?>
                </div>
            </div>

            <?= $form->field($orderModel, 'user_name')->textInput([
                'class' => 'form-control',
                'placeholder' => 'Имя'
            ])->label(false)?>

            <?= $form->field($orderModel, 'user_surname')->textInput([
                'class' => 'form-control',
                'placeholder' => 'Фамилия'
            ])->label(false)?>

            <?= $form->field($orderModel, 'user_email')->textInput([
                'class' => 'form-control',
                'placeholder' => 'Email'
            ])->label(false)?>

            <?= $form->field($orderModel, 'order_phone')->textInput([
                'class' => 'form-control',
                'placeholder' => 'Телефон'
            ])->label(false)?>

            <?= $form->field($orderModel, 'restaurant_id')->hiddenInput(
                ['value' => $restaurant->restaurant_id]
            )->label(false); ?>

            <?= $form->field($orderModel, 'user_id')->hiddenInput()->label(false); ?>

        <button class="animate btn btn-primary btn-lg btn-block order-btn">Зарезирвировать стол</button>
            <div class="form-group-inline rest-header__order-rating">
                <label>Рейтинг:</label>
                <div class="star-rating">
                    <span style="width:<?= $restaurant->getRatingPercent(); ?>%"></span>
                </div>
            </div>
            <div class="form-group-inline">
                <label>Средний чек:</label>
                <b><?= $restaurant->restaurant_average_check; ?> грн.</b>
            </div>
            <a href="#" class="restaurant-like animate">
                <span class="ion-ios-heart-outline"></span>
            </a>
        <?php ActiveForm::end(); ?>
    </div>
</div>
