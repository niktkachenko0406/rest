<?php
/**
 * @var Restaurants $rest
 * @var Restaurants $restaurant
 * @var Blog $blog
 */

use common\models\Blog;
use common\models\Restaurants;
use common\models\Comments;
use frontend\assets\ProfileAsset;
use frontend\assets\RestaurantAsset;
use frontend\widgets\profile\ProfileWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

ProfileAsset::register($this);

$this->title = $rest->restaurant_name;
?>
<!-- Profile header section start -->
<section class="profile-header rest-profile-header" style="background-image: url('/img/rest-profile-bg.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3 col-lg-2">
                <div class="profile-header__thumb">
                    <img src="<?= $rest->getAvatar(); ?>" alt="">
                </div>
            </div>
            <div class="col-sm-8 col-md-9 col-lg-10">
                <h1 class="profile-header__title lora"><?= $rest->restaurant_name; ?></h1>
                <a  href="<?= Url::to(['/restaurant/view', 'uri' => $rest->uri]); ?>" class="btn btn-sm btn-stroke profile-header__btn">Посмотреть профиль</a>
            </div>
        </div>
    </div>
</section>
<!-- Profile header section end -->

<!-- Profile settings tab section start -->
<section class="profile-tabs">
    <div class="container">
        <ul class="nav nav-tabs rest-tabs">
            <li class="active">
                <a data-toggle="tab" href="#profile-settings">Настройки профиля</a>
            </li>
            <li>
                <a id="js-rest-setting" data-toggle="tab" href="#rest-settings-tab">Настройки ресторана</a>
            </li>
            <li>
                <a data-toggle="tab" id="js-rest-blog" href="#blog-tab">Блог</a>
            </li>
            <li>
                <a data-toggle="tab" href="#favorites-tab">Избранное</a>
            </li>
        </ul>

        <div class="tab-content">
            <?= ProfileWidget::widget([
                'profile'        => $profile,
                'passwordChange' => $passwordChange
            ]); ?>
            <div id="rest-settings-tab" class="tab-pane fade js-rest-setting-tab">

            </div>
            <div id="blog-tab" class="tab-pane fade">
                <div class="rest-create-new js-form-post-bloc hidden">

                </div>

                <div class="rest-news-list js-list-posts">
                    <div class="double-header">
                        <div class="double-header__title">
                            <h2 class="lora">Записи блога</h2>
                        </div>
                        <div class="double-header__btn">
                            <a href="#"
                               class="js-add-new js-add-post btn btn-stroke btn-sm"
                               data-rest-id="<?= $rest->restaurant_id; ?>">Добавить новую</a>
                        </div>
                    </div>

                    <div class="news-list clearfix row">
                        <?php foreach ($rest->blog as $blog): ?>
                            <div class="grid-item grid-item-static grid-item--width3">
                                <a href="new-single.html" class="new-item">
                                    <div class="new-item__thumb">
                                        <img src="<?= $blog->getAvatar(); ?>" alt="">
                                    </div>
                                    <div class="new-item__caption">
                                        <h3 class="new-item__title lora"><?= $blog->blog_title; ?></h3>
                                        <div class="new-item__date"><?= Yii::$app->formatter->asDate($blog->created_at, 'php:d.m.y'); ?></div>
                                    </div>
                                </a>
                                <div class="new-item__controls">
                                    <a href="#"
                                       class="btn btn-stroke js-post-edit btn-sm"
                                       data-id="<?= $blog->blog_id; ?>"
                                       data-rest-id="<?= $rest->restaurant_id; ?>">Редактировать</a>
                                    <a href="<?= Url::to(['/blog/delete', 'id' => $blog->blog_id]); ?>" class="btn btn-stroke btn-sm">Удалить</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div id="favorites-tab" class="tab-pane fade">
                <ul class="list-unstyled restaur-list favorites-list four-col">
                    <?php foreach ($favoriteRests as $restaurant): ?>
                        <li class="restaurant-item animate js-favorite-rest">
                            <div class="restaurant-img">
                                <img src="<?= $restaurant->getAvatar(); ?>" alt="">
                                <div class="restaurant-img__icons">
                                    <a href="#" class="restaurant-like animate js-favorites-user-rest <?= in_array($restaurant->restaurant_id, $favorites) ? 'active' : '' ?>"
                                       data-rest-id="<?= $restaurant->restaurant_id ?>"
                                       data-status="<?= in_array($restaurant->restaurant_id, $favorites) ? 1 : 0 ?>">
                                        <span class="ion-ios-heart-outline"></span>
                                    </a>
                                    <div class="restaurant-rating"><?= $restaurant->restaurant_rating; ?></div>
                                    <a href="#" class="restaurant-comments animate"><?= Comments::countComments($restaurant->restaurant_id) ?></a>
                                </div>
                            </div>
                            <div class="restaurant-caption">
                                <div class="restaurant__head">
                                    <h3 class="restaurant__title lora">
                                        <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri]); ?>">
                                            <?= $restaurant->restaurant_name; ?>
                                        </a>
                                    </h3>
                                    <div class="restaurant__text"><?= $restaurant->restaurant_short_desc; ?></div>
                                </div>
                                <div class="restaurant__footer">
                                    <div class="restaurant__loc"><?= $restaurant->restaurant_address; ?></div>
                                    <a href="#"
                                       class="btn btn-default animate js-rest-order"
                                       data-rest-id="<?= $restaurant->restaurant_id; ?>"
                                       data-toggle="modal"
                                       data-target="#order-modal">Зарезервировать стол</a>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- Profile settings tab section end -->