<?php
/**
 * @var View $this
 * @var Restaurants $model
 * @var RestaurantGallery $gallery
 */

use common\models\Cities;
use common\models\Districts;
use common\models\Features;
use common\models\Kitchens;
use common\models\Metro;
use common\models\Reasons;
use common\models\RestaurantGallery;
use common\models\Restaurants;
use common\models\Types;
use frontend\assets\RestaurantAsset;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use \yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use yii\web\View;
?>

<div class="row">
    <div class="col-md-7 col-lg-8 col-md-push-5 col-lg-push-4">
        <div class="row">
            <div class="col-md-6">
                <div class="form-upload restaurant-profile-img">
                    <div class="upload-thumb upload-thumb-rest">
                        <img id="js-restaurant-avatar" src="<?= $model->getAvatar(); ?>" alt="">
                    </div>
                    <div id="js-rest-avatar" class="upload-btn animate" data-id="<?= $model->restaurant_id; ?>">
                        Загрузить главное фото
                        <input name="rest_avatar" type="file">
                    </div>
                </div>
            </div>
        </div>
        <ul class="list-unstyled gall-list-upload clearfix js-added-files">
            <?php foreach ($model->gallery as $gallery): ?>
                <li class="gall-upload-item">
                    <a href="#" class="upload-close ion-ios-close js-delete-image" data-file-id="<?= $gallery->gallery_id; ?>"></a>
                    <img src="/upload/restaurant/gallery/preview/<?= $gallery->gallery_image; ?>" onerror="src='/img/rest-gall-1.jpg'" alt="">
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="clearfix">
            <div id="js-upload-gallery" class="upload-btn upload-gall animate">
                Загрузить фото для галереи
                <input type="file" multiple>
            </div>
            <a href="#" class="btn-link js-remove-image-all">Удалить все</a>
        </div>
        <div class="form-upload menu-upload">
            <div class="menu-upload__preview">
                <img src="/img/nemu-thumb.jpg" alt="">
            </div>
            <div class="upload-btn animate">
                Загрузить меню
                <input type="file">
            </div>
        </div>

    </div>
    <div class="rest-form-sett col-md-5 col-lg-4 col-md-pull-7 col-lg-pull-8">
        <?php $form = ActiveForm::begin([
            'id'                     => 'restaurantForm',
            'action'                 => Url::to(['/restaurant/rest-setting']),
            'method'                 => 'post',
            'enableClientValidation' => true,
            'options' => [
                'enctype'          => 'multipart/form-data',
                'validateOnSubmit' => true,
            ]
        ]); ?>

            <?= $form->field($model, 'restaurant_name')->textInput(); ?>
            <div class="form-group">
                <label>Тип заведения</label>
                <?= Select2::widget([
                    'name'          => 'types[]',
                    'value'         => ArrayHelper::map($model->types, 'type_id', 'type_id'),
                    'data'          => ArrayHelper::map(Types::getTypes(), 'type_id', 'type_name'),
                    'size'          => Select2::LARGE,
                    'maintainOrder' => true,
                    'options' => [
                        'placeholder' => 'Выберите тип',
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
            <?= $form->field($model, 'restaurant_short_desc')->textarea(['rows' => 3]); ?>
            <?= $form->field($model, 'restaurant_desc')->textarea(['rows' => 4]); ?>
            <div class="form-group">
                <label>Кухня</label>
                <?= Select2::widget([
                    'name'          => 'kitchens[]',
                    'value'         => ArrayHelper::map($model->kitchen, 'kitchen_id', 'kitchen_id'),
                    'data'          => ArrayHelper::map(Kitchens::getKitchens(), 'kitchen_id', 'kitchen_name'),
                    'size'          => Select2::LARGE,
                    'maintainOrder' => true,
                    'options' => [
                        'placeholder' => 'Выберите кухню',
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>

            <?= $form->field($model, 'restaurant_average_check')->input('number'); ?>

            <?= $form->field($model, 'restaurant_phone')->textInput(); ?>

            <div class="form-group">
                <label>Режим работы</label>
                <ul class="list-unstyled schedule-list">
                    <li class="schedule-day">
                        <div class="schedule-day__checkbox checkbox-box">
                            <input type="checkbox"
                                   class="checkbox js-schedule-day"
                                   name="Restaurants[restaurant_days][]"
                                   value="monday"
                                   id="schedule-1"
                                   <?php if(is_array($model->restaurant_days) && in_array('monday', $model->restaurant_days)): ?>checked<?php endif; ?> />
                            <label for="schedule-1">Пн</label>
                        </div>
                        <div class="schedule-day__ranger form-ranger bill-slider-wrap">
                            <div class="slider-time"></div>
                            <?= $form->field($model, 'restaurant_schedule[monday][from]')->textInput([
                                'class' => 'min hidden'
                            ])->label(false); ?>

                            <?= $form->field($model, 'restaurant_schedule[monday][to]')->textInput([
                                'class' => 'max hidden',
                            ])->label(false); ?>
                        </div>
                    </li>
                    <li class="schedule-day">
                        <div class="schedule-day__checkbox checkbox-box">
                            <input type="checkbox"
                                   class="checkbox js-schedule-day"
                                   name="Restaurants[restaurant_days][]"
                                   value="tuesday"
                                   id="schedule-2"
                                   <?php if(is_array($model->restaurant_days) && in_array('tuesday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
                            <label for="schedule-2">Вт</label>
                        </div>
                        <div class="schedule-day__ranger form-ranger bill-slider-wrap">
                            <div class="slider-time"></div>
                            <?= $form->field($model, 'restaurant_schedule[tuesday][from]')->textInput([
                                'class' => 'min hidden',
                            ])->label(false); ?>

                            <?= $form->field($model, 'restaurant_schedule[tuesday][to]')->textInput([
                                'class' => 'max hidden',
                            ])->label(false); ?>
                        </div>
                    </li>
                    <li class="schedule-day">
                        <div class="schedule-day__checkbox checkbox-box">
                            <input type="checkbox"
                                   name="Restaurants[restaurant_days][]"
                                   value="wednesday"
                                   class="checkbox js-schedule-day"
                                   id="schedule-3"
                                   <?php if(is_array($model->restaurant_days) && in_array('wednesday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
                            <label for="schedule-3">Ср</label>
                        </div>
                        <div class="schedule-day__ranger form-ranger bill-slider-wrap">
                            <div class="slider-time"></div>
                            <?= $form->field($model, 'restaurant_schedule[wednesday][from]')->textInput([
                                'class' => 'min hidden',
                            ])->label(false); ?>

                            <?= $form->field($model, 'restaurant_schedule[wednesday][to]')->textInput([
                                'class' => 'max hidden',
                            ])->label(false); ?>

                        </div>
                    </li>
                    <li class="schedule-day">
                        <div class="schedule-day__checkbox checkbox-box">
                            <input type="checkbox"
                                   name="Restaurants[restaurant_days][]"
                                   value="thursday"
                                   class="checkbox js-schedule-day"
                                   id="schedule-4"
                                   <?php if(is_array($model->restaurant_days) && in_array('thursday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
                            <label for="schedule-4">Чт</label>
                        </div>
                        <div class="schedule-day__ranger form-ranger bill-slider-wrap">
                            <div class="slider-time"></div>
                            <?= $form->field($model, 'restaurant_schedule[thursday][from]')->textInput([
                                'class' => 'min hidden',
                            ])->label(false); ?>

                            <?= $form->field($model, 'restaurant_schedule[thursday][to]')->textInput([
                                'class' => 'max hidden',
                            ])->label(false); ?>

                        </div>
                    </li>
                    <li class="schedule-day">
                        <div class="schedule-day__checkbox checkbox-box">
                            <input type="checkbox"
                                   name="Restaurants[restaurant_days][]"
                                   value="friday"
                                   class="checkbox js-schedule-day"
                                   id="schedule-5"
                                   <?php if(is_array($model->restaurant_days) && in_array('friday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
                            <label for="schedule-5">Пт</label>
                        </div>
                        <div class="schedule-day__ranger form-ranger bill-slider-wrap">
                            <div class="slider-time"></div>
                            <?= $form->field($model, 'restaurant_schedule[friday][from]')->textInput([
                                'class' => 'min hidden',
                            ])->label(false); ?>

                            <?= $form->field($model, 'restaurant_schedule[friday][to]')->textInput([
                                'class' => 'max hidden',
                            ])->label(false); ?>

                        </div>
                    </li>
                    <li class="schedule-day">
                        <div class="schedule-day__checkbox checkbox-box">
                            <input type="checkbox"
                                   name="Restaurants[restaurant_days][]"
                                   value="saturday"
                                   class="checkbox js-schedule-day"
                                   id="schedule-6"
                                   <?php if(is_array($model->restaurant_days) && in_array('saturday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
                            <label for="schedule-6">Сб</label>
                        </div>
                        <div class="schedule-day__ranger form-ranger bill-slider-wrap">
                            <div class="slider-time"></div>
                            <?= $form->field($model, 'restaurant_schedule[saturday][from]')->textInput([
                                'class' => 'min hidden',
                            ])->label(false); ?>

                            <?= $form->field($model, 'restaurant_schedule[saturday][to]')->textInput([
                                'class' => 'max hidden',
                            ])->label(false); ?>

                        </div>
                    </li>
                    <li class="schedule-day">
                        <div class="schedule-day__checkbox checkbox-box">
                            <input type="checkbox"
                                   name="Restaurants[restaurant_days][]"
                                   value="sunday"
                                   class="checkbox js-schedule-day"
                                   id="schedule-7"
                                   <?php if(is_array($model->restaurant_days) && in_array('sunday', $model->restaurant_days)): ?>checked<?php endif; ?>/>
                            <label for="schedule-7">Вс</label>
                        </div>
                        <div class="schedule-day__ranger form-ranger bill-slider-wrap">
                            <div class="slider-time"></div>
                            <?= $form->field($model, 'restaurant_schedule[sunday][from]')->textInput([
                                'class' => 'min hidden',
                            ])->label(false); ?>

                            <?= $form->field($model, 'restaurant_schedule[sunday][to]')->textInput([
                                'class' => 'max hidden',
                            ])->label(false); ?>

                        </div>
                    </li>
                </ul>
            </div>
            <div class="form-group">
                <label>Особенности</label>
                <?= Select2::widget([
                    'name'          => 'features[]',
                    'value'         => ArrayHelper::map($model->features, 'feature_id', 'feature_id'),
                    'data'          => ArrayHelper::map(Features::getFeatures(), 'feature_id', 'feature_name'),
                    'size'          => Select2::LARGE,
                    'maintainOrder' => true,
                    'options' => [
                        'placeholder' => 'Выберите особенности',
                        'multiple'    => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>

            <div class="form-group">
                <label>Повод</label>
                <?= Select2::widget([
                    'name'          => 'reasons[]',
                    'value'         => ArrayHelper::map($model->reasons, 'reason_id', 'reason_id'),
                    'data'          => ArrayHelper::map(Reasons::getReasons(), 'reason_id', 'reason_name'),
                    'size'          => Select2::LARGE,
                    'maintainOrder' => true,
                    'options' => [
                        'placeholder' => 'Выберите повод',
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>

            <?= $form->field($model, 'city_id')->dropDownList(Cities::getCities(), ['prompt' => 'выбрать', 'id' => 'js-city-id', 'class' => 'chosen-select']); ?>

            <?= $form->field($model, 'district_id')->widget(DepDrop::className(), [
                'type'           => DepDrop::TYPE_SELECT2,
                'data'           => [$model->district_id => Districts::getDistrictName($model->district_id)],
                'select2Options' => [
                    'pluginOptions' => ['allowClear'=>true],
                ],
                'options'        => ['id'=>'js-district-id', 'class' => 'chosen-select'],
                'pluginOptions'=>[
                    'depends'     =>['js-city-id'],
                    'placeholder' => 'Выбрать',
                    'initialize'  => true,
                    'url'         => Url::to(['restaurant/get-districts'])
                ]
            ]); ?>

            <div class="form-group">
                <label>Метро</label>
                <?= Html::dropDownList('metroId[]',
                    $model->getSelectedMetro(),
                    ArrayHelper::map(Metro::getMetroByDistrict($model->district_id), 'station_id', 'station_name'),
                    [
                        'multiple' => 'multiple',
                        'class'    => 'js-rest-metro chosen-metro'
                    ]
                ); ?>
            </div>



            <?= $form->field($model, 'restaurant_address')->textInput(); ?>
<!--            <div class="form-group">-->
<!--                <label>Меню</label>-->
<!--                <div class="input-group input-group-icon">-->
<!--                    <input type="text" class="form-control menu-file-title" value="restmenu2017.csv">-->
<!--                    <span class="input-group-addon"><a href="#" class="ion-ios-close"></a></span>-->
<!--                </div>-->
<!--                <div class="upload-btn upload-menu animate">-->
<!--                    Загрузить CSV файл-->
<!--                    <input type="file">-->
<!--                </div>-->
<!--            </div>-->
            <button type="submit" class="animate btn btn-light btn-lg submit-btn">Сохранить</button>
        <?php ActiveForm::end(); ?>
    </div>
</div>