<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title text-center lora" id="signinModalLabel">Регистрация</h2>

        </div>
        <div class="modal-body">
            <ul class="nav nav-tabs reg-tabs">
                <li class="active"><a href="#user-reg" data-type="0" class="js-user-type" data-toggle="tab">
                    <span class="ion-android-person"></span>Регистрация пользователя</a>
                </li>
                <li><a href="#restaurant-reg" data-type="1" class="js-user-type" data-toggle="tab">
                    <span class="ion-android-restaurant"></span>Регистрация ресторана</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="user-reg" class="tab-pane fade in active">
                    <?php
                    $form = ActiveForm::begin([
                        'action'               => Url::to(['site/signup']),
                        'id'                   => 'form-signup',
                        'class'                => 'form-grey',
                        'enableAjaxValidation' => true
                    ]);
                    ?>
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <?= $form->field($model, 'email')->textInput(['class' => 'form-control'])->label('Ел. почта') ?>
                            <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control'])->label('Пароль') ?>
                            <?= $form->field($model, 'user_type')->hiddenInput(['class' => 'form-control js-user-type-value', 'value' => 0])->label(false) ?>
                            <input type="submit" value="Зарегистрироваться" class="btn form-btn btn-lg btn-primary animate">
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div id="restaurant-reg" class="tab-pane fade in">
                    <div class="text-center">
                        Тут будет форма регистрации для ресторанов
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>