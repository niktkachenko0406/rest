<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title text-center lora" id="loginModalLabel">Вход</h2>
        </div>
        <div class="modal-body">
            <?php
                $form = ActiveForm::begin([
                    'action'               => Url::to(['site/login']),
                    'id'                   => 'login-form',
                    'class'                => 'form-grey',
                    'enableAjaxValidation' => true
                ]);
            ?>
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">

                        <?= $form->field($model, 'email')->textInput(['class' => 'form-control'])->label('Ел. почта') ?>

                        <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control'])->label('Пароль') ?>
                        <input type="submit" value="Войти" class="btn form-btn btn-lg btn-primary animate">
                        <a href="#" class="btn-link btn-lg pull-right animate">Забыли пароль?</a>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
