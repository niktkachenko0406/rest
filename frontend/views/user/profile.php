<?php
/** @var Restaurants $restaurant */
use common\models\Restaurants;
use frontend\assets\ProfileAsset;
use common\models\Comments;
use frontend\widgets\profile\ProfileWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

ProfileAsset::register($this);

?>
<!-- Profile header section start -->
<section class="profile-header">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3 col-lg-2">
                <div class="profile-header__thumb">
                    <img src="<?=$profile->getAvatar() ?: null ?>" alt="">
                </div>
            </div>
            <div class="col-sm-8 col-md-9 col-lg-10">
                <h1 class="profile-header__title lora"><?=$user->getFullName(); ?></h1>
                <div class="profile-header__mail"><?= $user->email; ?></div>
            </div>
        </div>
    </div>
</section>
<!-- Profile header section end -->

<!-- Profile settings tab section start -->
<section class="profile-tabs">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#profile-settings">Настройки профиля</a></li>
            <li><a data-toggle="tab" href="#profile-favorites">Избранное</a></li>
            <li><a data-toggle="tab" href="#profile-history">История резервов</a></li>
        </ul>

        <div class="tab-content">
            <?= ProfileWidget::widget([
                    'profile'        => $profile,
                    'passwordChange' => $passwordChange
            ]); ?>
            <div id="profile-favorites" class="tab-pane fade">
                <?php if($favoriteRests): ?>
                    <ul class="list-unstyled restaur-list favorites-list four-col">
                        <?php foreach ($favoriteRests as $restaurant): ?>
                            <li class="restaurant-item animate js-favorite-rest">
                                <div class="restaurant-img">
                                    <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri]); ?>">
                                        <img src="<?= $restaurant->getAvatar(); ?>" alt="">
                                    </a>
                                    <div class="restaurant-img__icons">
                                        <a href="#" class="restaurant-like animate js-favorites-user-rest <?= in_array($restaurant->restaurant_id, $favorites) ? 'active' : '' ?>"
                                           data-rest-id="<?= $restaurant->restaurant_id ?>"
                                           data-status="<?= in_array($restaurant->restaurant_id, $favorites) ? 1 : 0 ?>">
                                            <span class="ion-ios-heart-outline"></span>
                                        </a>
                                        <div class="restaurant-rating"><?= $restaurant->restaurant_rating; ?></div>
                                        <a href="#" class="restaurant-comments animate"><?= Comments::countComments($restaurant->restaurant_id) ?></a>
                                    </div>
                                </div>
                                <div class="restaurant-caption">
                                    <div class="restaurant__head">
                                        <h3 class="restaurant__title lora">
                                            <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri]); ?>">
                                                <?= $restaurant->restaurant_name; ?>
                                            </a>
                                        </h3>
                                        <div class="restaurant__text"><?= $restaurant->restaurant_short_desc; ?></div>
                                    </div>
                                    <div class="restaurant__footer">
                                        <div class="restaurant__loc"><?= $restaurant->restaurant_address; ?></div>
                                        <a href="#"
                                           class="btn btn-default animate js-rest-order"
                                           data-rest-id="<?= $restaurant->restaurant_id; ?>"
                                           data-toggle="modal"
                                           data-target="#order-modal">Зарезервировать стол</a>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div id="profile-history" class="tab-pane fade">
                История резервов
                <?php if($orderRests): ?>
                    <ul class="list-unstyled restaur-list favorites-list four-col">
                        <?php foreach ($orderRests as $order): ?>
                            <?php $restaurant = $order->restaurant ?>
                            <li class="restaurant-item animate js-favorite-rest">
                                <div class="restaurant-img">
                                    <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri]); ?>">
                                        <img src="<?= $restaurant->getAvatar(); ?>" alt="">
                                    </a>
                                    <div class="restaurant-img__icons">
                                        <a href="#" class="restaurant-like animate js-favorites-user-order <?= in_array($restaurant->restaurant_id, $favorites) ? 'active' : '' ?>"
                                           data-rest-id="<?= $restaurant->restaurant_id ?>"
                                           data-status="<?= in_array($restaurant->restaurant_id, $favorites) ? 1 : 0 ?>">
                                            <span class="ion-ios-heart-outline"></span>
                                        </a>
                                        <div class="restaurant-rating"><?= $restaurant->restaurant_rating; ?></div>
                                        <a href="#" class="restaurant-comments animate"><?= Comments::countComments($restaurant->restaurant_id) ?></a>
                                    </div>
                                </div>
                                <div class="restaurant-caption">
                                    <div class="restaurant__head">
                                        <h4 class="restaurant__title lora"><?= $order->order_date ?></h4>
                                        <h4 class="restaurant__title lora">Персон: <?= $order->order_person ?></h4>
                                        <h3 class="restaurant__title lora">
                                            <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri]); ?>">
                                                <?= $restaurant->restaurant_name; ?>
                                            </a>
                                        </h3>
                                        <div class="restaurant__text"><?= $restaurant->restaurant_short_desc; ?></div>
                                    </div>
                                    <div class="restaurant__footer">
                                        <div class="restaurant__loc"><?= $restaurant->restaurant_address; ?></div>
                                        <a href="#"
                                           class="btn btn-default animate js-rest-order"
                                           data-rest-id="<?= $restaurant->restaurant_id; ?>"
                                           data-toggle="modal"
                                           data-target="#order-modal">Зарезервировать стол</a>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- Profile settings tab section end -->