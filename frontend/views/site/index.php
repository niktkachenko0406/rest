<?php
/** @var Restaurants $restaurant */
/** @var News $newsOne */
/** @var Blog $post */

use common\models\Blog;
use common\models\News;
use common\models\Restaurants;
use frontend\assets\RestaurantAsset;
use yii\helpers\Url;

RestaurantAsset::register($this);

$this->title = 'Главная';
?>
<!-- Site title section start -->
<section class="site-title">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="site-title__text text-center">
                    <h1 class="lora">Лучшие рестораны Украины</h1>
                    <div class="page-subtitle">Добавить краткое описание, чтобы ваши клиенты могли узнать вас лучше.</div>
                </div>
            </div>
        </div>

        <div class="site-title__form">
            <form action="<?= Url::to(['restaurant/index']); ?>" method="get" class="filter-form clearfix">
                <div class="form-group filter-form__persons">
                    <select name="person" id="filter-persons" class="chosen-select filter-select">
                        <option value="1">1 персона</option>
                        <option value="2">2 персоны</option>
                        <option value="4">4 персоны</option>
                        <option value="6">6 персон</option>
                        <option value="10">10 персон</option>
                    </select>
                </div>
                <div class="form-group filter-form__date">
                    <input type="text" name="date" class="form-control datetimepicker" placeholder="Выберите дату" data-loc="left">
                </div>
<!--                <div class="form-group filter-form__date">-->
<!--                    <input type="text" name="day" class="form-control datepicker" placeholder="2 января" data-loc="left">-->
<!--                </div>-->
<!--                <div class="form-group filter-form__time">-->
<!--                    <select name="time" id="filter-time" class="chosen-select  filter-select">-->
<!--                        <option value="10:00">10:00</option>-->
<!--                        <option value="10:30">10:30</option>-->
<!--                        <option value="11:00">11:00</option>-->
<!--                        <option value="11:30">11:30</option>-->
<!--                        <option value="12:00">12:00</option>-->
<!--                    </select>-->
<!--                </div>-->
                <div class="form-group filter-form__loc">
                    <input type="text" name="search" class="form-control" placeholder="Улица, бар, кальян...">
                </div>
                 <button type="submit" class="animate btn btn-primary btn-lg">Найти ресторан</button>
<!--                <a href="search-result.html" class="animate btn btn-primary btn-lg">Найти ресторан</a> <!-- This button only for frontend. If you need form submit - remove this link and uncomment previous button -->
            </form>
        </div>
    </div>
</section>
<!-- Site title section end -->

<!-- New restaurants section start -->
<?php if($restaurants): ?>
    <section class="new-restaurants">
        <div class="container">
            <h2 class="block-title lora text-center">Новое на Resto Spilnota<sup>&reg;</sup></h2>
            <ul class="list-unstyled restaur-list">
                <?php foreach ($restaurants as $restaurant): ?>
                    <li class="restaurant-item animate">
                        <div class="restaurant-img">
                            <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri]); ?>">
                                <img src="<?= $restaurant->getAvatar(); ?>" alt="">
                            </a>
                            <div class="restaurant-img__icons">
                                <a href="" class="restaurant-like animate js-favorites <?= in_array($restaurant->restaurant_id, $favorites) ? 'active' : '' ?>"
                                    data-rest-id="<?= $restaurant->restaurant_id ?>"
                                    data-status="<?= in_array($restaurant->restaurant_id, $favorites) ? 1 : 0 ?>">
                                    <span class="ion-ios-heart-outline"></span>
                                </a>
                                <div class="restaurant-rating"><?= $restaurant->restaurant_rating; ?></div>
                                <a href="#" class="restaurant-comments animate"><?= count($restaurant->comments); ?></a>
                            </div>
                        </div>
                        <div class="restaurant-caption">
                            <div class="restaurant__head">
                                <h3 class="restaurant__title lora">
                                    <a href="<?= Url::to(['/restaurant/view', 'uri' => $restaurant->uri])?>"><?= $restaurant->restaurant_name; ?></a>
                                </h3>
                                <div class="restaurant__text"><?= $restaurant->restaurant_short_desc; ?></div>
                            </div>
                            <div class="restaurant__footer">
                                <div class="restaurant__loc"><?= $restaurant->restaurant_address; ?></div>
                                <a href="#0"
                                   class="js-rest-order btn btn-default animate"
                                   data-toggle="modal"
                                   data-rest-id="<?= $restaurant->restaurant_id; ?>"
                                   data-target="#order-modal">Зарезервировать стол</a>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="text-center">
                <a href="<?= Url::to(['/restaurant/index']); ?>" class="btn btn-stroke more-btn animate">Все рестораны</a>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- New restaurants section end -->

<!-- Last news section start -->
<?php if($news): ?>
    <section class="last-news">
        <div class="container">
            <h2 class="block-title lora text-center">Последние новости</h2>
            <div class="grid news-list masonry-news clearfix">
                <?php foreach ($news as $newsOne): ?>
                    <div class="grid-item grid-item-abs">
                        <a href="<?= Url::to(['view', 'uri' => $newsOne->uri]); ?>" class="new-item">
                            <div class="new-item__thumb">
                                <img src="<?= $newsOne->getImg(); ?>" alt="">
                            </div>
                            <div class="new-item__caption">
                                <h3 class="new-item__title lora"><?= $newsOne->news_title; ?></h3>
                                <div class="new-item__date"><?= Yii::$app->formatter->asDate($newsOne->created_at, 'php:d.m.Y'); ?></div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
                <div class="grid-sizer"></div>
            </div>
            <div class="text-center">
                <a href="<?= Url::to(['/news/index']); ?>" class="btn btn-stroke more-btn animate">Все новости</a>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- Last news section end -->

<!-- Last posts from blog section start -->
<?php if($blogPosts): ?>
    <section class="last-posts">
        <div class="container">
            <h2 class="block-title block-title-white lora text-center">Последние посты из блога</h2>
            <div class="row post-list">
                <?php foreach ($blogPosts as $post): ?>
                    <div class="col-xs-6 col-md-3 post-item">
                        <img src="<?= $post->getImg(); ?>" alt="">
<!--                        <ul class="post-item__tags list-unstyled">-->
<!--                            <li class="post-tag"><a href="#">#мнение</a></li>-->
<!--                        </ul>-->
                        <div class="post-item__title">
                            <a href="<?= Url::to(['/blog/view', 'uri' => $post->uri]); ?>"><?= $post->blog_title; ?></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="text-center">
                <a href="<?= Url::to(['/blog/index']); ?>" class="btn btn-stroke more-btn animate">Все посты</a>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- Last posts from blog section end -->