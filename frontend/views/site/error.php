<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>
<section class="page-not-found">
    <div class="container">
        <h1 class="lora">Страница не найдена</h1>
        <p>
            Неправильно набран адрес, или такой страницы больше нет.<br>
            Вернуться на <a href="<?= Url::to(['/']); ?>">главную страницу</a>.
        </p>
    </div>
</section>
