<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Консалтинг';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Page header section start -->
<section class="page-title" style="background-image: url(img/consulting-bg.jpg)">
    <div class="container">
        <h1 class="lora text-center">Консалтинг</h1>
    </div>
</section>
<!-- Page header section end -->

<!-- Partners section start -->
<section class="consulting">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                <div class="consulting-content">
                    <h2 class="lora">Заголовок #1</h2>
                    <h3 class="lora">Никаких замысловатых историй</h3>
                    <p>Подробное описание ресторана. Оно не должно быть скрыто, так как это основной текст, по которому поисковые системы будут ранжировать страницу. Подробное описание ресторана. Оно не должно быть скрыто, так как это основной текст, по которому поисковые системы будут ранжировать страницу.
                    </p>
                    <p>Подробное описание ресторана. Оно не должно быть скрыто, так как это основной текст, по которому поисковые системы будут ранжировать страницу.</p>
                    <img src="img/consulting-img.jpg" alt="">
                    <blockquote>Высокоинтеллектуальные беседы все еще не могу вести, но взаимопонимание с командой растет с каждым днем.</blockquote>
                    <p>Украинские продукты мы используем очень активно. Безусловно, есть позиции, которые требуют импорта: рыба, суши, американские стейки. Но овощи, курица, говядина, свинина у нас локальные. Мы даже лапшу делаем из украинской муки.</p>

                    <p>Мне кажется правильным использовать местные продукты хорошего качества, например, украинские фрукты. Люди привыкают к своей еде, чужая не всегда приживается.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Partners section end -->
