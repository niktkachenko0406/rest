<?php

/* @var $this yii\web\View */

$this->title = 'Контакты';
?>

<!-- Page header section start -->
<section class="page-title" style="background-image: url(/img/contacts-bg.jpg)">
    <div class="container">
        <h1 class="lora text-center">Контакты</h1>
    </div>
</section>
<!-- Page header section end -->

<!-- Partners section start -->
<section class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="contact-info">
                    <h3 class="lora">Звоните по любым вопросам</h3>
                    <p>
                        <b>в Киеве:</b> (044) 123 12 12<br>
                        <b>в Ровно:</b> +380 987-48-45
                    </p>
                    <p>Время работы: 10:00 - 22:00</p>
                    <p>
                        Пишите, если у Вас технический вопрос, жалоба или Вы хотите оставить отзыв: <br>
                        <a href="mailto:mail@restospilnota.ua">mail@restospilnota.ua</a>
                    </p>
                </div>
            </div>
            <div class="col-md-8">
                <div class="rest-content__map">
                    <div class="map-address">ул. Киевская 26 А</div>
                    <div id="rest-map" class="google-map"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Partners section end -->


<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDe4UajhwQfHG316ORG1ZxIJi48wMlZ9kc"></script>-->