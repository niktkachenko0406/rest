<?php
/**
 * @var News $news
 * @var Blog $blog
 */
use common\models\Blog;
use common\models\News;
use yii\helpers\Url;
$this->title = 'Результат поиска';
?>

<!-- Page header section start -->
<section class="page-title" style="background-image: url(/img/news-bg.jpg)">
    <div class="container">
        <h1 class="lora text-center">Поиск</h1>
    </div>
</section>
<!-- Page header section end -->
<section class="search-result">
    <div class="container">
        <div class="row">
            <div class="search-content col-md-12">
                <h3 class="lora text-center">Результат по запросу: <?= $search ?></h3>
                <?php if($allNews): ?>
                    <h2>Новости</h2>
                    <div class="grid news-list clearfix">
                        <?php foreach ($allNews as $news): ?>
                            <div class="grid-item grid-item-abs">
                                <a href="<?= Url::to(['news/view', 'uri' => $news->uri]); ?>" class="new-item">
                                    <div class="new-item__thumb">
                                        <img src="<?= $news->getImg(); ?>" alt="">
                                    </div>
                                    <div class="new-item__caption">
                                        <h3 class="new-item__title lora"><?= $news->news_title; ?></h3>
                                        <div class="new-item__date"><?= Yii::$app->formatter->asDate($news->created_at, 'php:d.m.Y'); ?></div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                        <div class="grid-sizer"></div>
                    </div>
                <?php endif; ?>
                <?php if($allBlog): ?>
                    <h2>Блог</h2>
                    <div class="grid news-list clearfix">
                        <?php foreach ($allBlog as $blog): ?>
                            <div class="grid-item grid-item-abs">
                                <a href="<?= Url::to(['blog/view', 'uri' => $blog->uri]); ?>" class="new-item">
                                    <div class="new-item__thumb">
                                        <img src="<?= $blog->getImg(); ?>" alt="">
                                    </div>
                                    <div class="new-item__caption">
                                        <h3 class="new-item__title lora"><?= $blog->blog_title; ?></h3>
                                        <div class="new-item__date"><?= Yii::$app->formatter->asDate($blog->created_at, 'php:d.m.Y'); ?></div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                        <div class="grid-sizer"></div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>


