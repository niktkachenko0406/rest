<?php
/** @var News $news */

use common\models\Blog;
use common\models\News;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Новости';
?>

<!-- Page header section start -->
<section class="page-title" style="background-image: url(/img/news-bg.jpg)">
    <div class="container">
        <h1 class="lora text-center">Новости</h1>
    </div>
</section>
<!-- Page header section end -->

<!-- News grid section start -->
<section class="news">
    <div class="container">
        <div class="grid news-list masonry-news clearfix">
            <?php foreach ($provider->getModels() as $news): ?>
                <div class="grid-item  grid-item-abs">
                    <a href="<?= Url::to(['view', 'uri' => $news->uri]); ?>" class="new-item">
                        <div class="new-item__thumb">
                            <img src="<?= $news->getImg(); ?>" alt="">
                        </div>
                        <div class="new-item__caption">
                            <h3 class="new-item__title lora"><?= $news->news_title; ?></h3>
                            <div class="new-item__date"><?= Yii::$app->formatter->asDate($news->created_at, 'php:d.m.Y'); ?></div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            <div class="grid-sizer"></div>
        </div>
        <nav class="post-navigation">
            <?= LinkPager::widget([
                'pagination' => $provider->pagination,
            ]);?>
        </nav>
    </div>
</section>
<!-- News grid section end -->