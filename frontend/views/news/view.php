<?php
/** @var News $news */
/** @var Comments $comment */

use common\models\Comments;
use common\models\News;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $news->news_title;
$userProfile = $user->userProfile;
?>


<!-- Page header section start -->
<section class="page-title" style="background-image: url(/img/new-bg.jpg)">
    <div class="container">
        <a href="<?= Url::to(['index']); ?>" class="page-title__back btn-stroke btn btn-sm animate pull-left">Назад</a>
        <h1 class="lora text-center">Новости</h1>
    </div>
</section>
<!-- Page header section end -->

<!-- New content section start -->
<section class="new-single">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="new-single__header">
                    <h2 class="new-single__title lora"><?= $news->news_title; ?></h2>
                    <div class="new-single__date"><?= Yii::$app->formatter->asDate($news->created_at, 'php:d.m.Y'); ?></div>
                    <div class="new-single__image">
                        <img src="<?= $news->getImg(); ?>" alt="">
                    </div>
                </div>
                <div class="new-single__content">
                    <?= $news->news_text; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="rest-content__reviews">
                <h3 class="lora">Коментарии <span class="badge"><?= count($news->comments); ?></span></h3>
                <div class="row">
                    <?php if(!empty($news->comments)): ?>
                        <?php foreach ($news->comments as $comment): ?>
                            <?php
                                $author        = $comment->user;
                                $authorProfile = $author->userProfile;
                            ?>
                            <div class="col-lg-6">
                                <div class="review-item">
                                    <div class="review-item__header">
                                        <div class="review-item__title">
                                            <img src="<?= $authorProfile ? $authorProfile->getAvatar() : '/img/avatar-default.jpg' ?>" alt="" class="review-item__avatar">
                                            <div class="review-item__name"><?= $authorProfile ? $authorProfile->getFullName() : $author->email ?></div>
                                        </div>
                                        <div class="review-item__text">
                                            <?= $comment->comment_text; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <?php if(Yii::$app->user->isGuest): ?>
                    <div class="review-item no-login text">Чтобы добавить коментарий,
                        <a href="#" data-toggle="modal" data-target="#login-modal">войдите</a> или
                        <a href="#" data-toggle="modal" data-target="#signin-modal">зарегестрируйтесь</a>.
                    </div>
                <?php else: ?>
                    <?php $form = \yii\bootstrap\ActiveForm::begin([
                        'id'                     => 'commentsForm',
                        'action'                 => Url::to(['/news/save-comment', 'uri' => $news->uri]),
                        'method'                 => 'post',
                        'enableClientValidation' => true,
                        'options' => [
                            'validateOnSubmit' => true,
                        ]
                    ]); ?>
                    <div class="review-item add-review">
                        <div class="review-item__header">
                            <div class="review-item__title">
                                <img src="<?= $userProfile ? $userProfile->getAvatar() : '/img/avatar-default.jpg' ?>" alt="" class="review-item__avatar">
                                <div class="review-item__name"><?= $userProfile ? $userProfile->getFullName() : $user->email ?></div>
                            </div>
                            <div class="review-item__text">
                                <?= $form->field($commentsModel, 'comment_text')->textarea([
                                    'class' => 'string-textarea',
                                    'placeholder' => 'Напишите коментарий...'
                                ])->label(false); ?>
                            </div>
                            <?= $form->field($commentsModel, 'entity_id')->hiddenInput([
                                'value' => $news->news_id
                            ])->label(false); ?>
                        </div>
                    </div>
                    <?= Html::button('Добавить коментарий', [
                    'class' => 'animate btn btn-light btn-lg btn-block',
                    'type'  => 'submit'
                ])?>
                    <?php \yii\bootstrap\ActiveForm::end(); ?><br>
                    <?php if($message = Yii::$app->session->getFlash('comment')): ?>
                        <div class="alert alert-success" role="alert"><?= $message ?></div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- New content section end -->

<!-- Other news section start -->
<section class="other-news">
    <div class="container">
        <h2 class="lora text-center">Другие новости</h2>
        <div class="news-list clearfix">
            <?php foreach ($lastNews as $item): ?>
                <div class="grid-item grid-item-static grid-item--width3">
                    <a href="<?= Url::to(['view', 'uri' => $item->uri]); ?>" class="new-item">
                        <div class="new-item__thumb">
                            <img src="<?= $item->getImg(); ?>" alt="">
                        </div>
                        <div class="new-item__caption">
                            <h3 class="new-item__title lora"><?= $item->news_title; ?></h3>
                            <div class="new-item__date"><?= Yii::$app->formatter->asDate($item->created_at, 'php:d.m.Y'); ?></div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- Other news section start -->