<?php

namespace frontend\models;

use common\models\User;
use common\models\UserProfile;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class OrderForm
 * @package frontend\models
 */
class OrderForm extends ActiveRecord {

    public $user_name;
    public $user_surname;
    public $user_email;

    public static function tableName()
    {
        return 'orders';
    }

    public function rules() {
        return [
            [['order_person', 'user_name', 'user_surname', 'user_email', 'order_phone', 'order_date'], 'required', 'message' => 'Поле не может быть пустым!'],
            [['user_name', 'user_surname', 'user_email', 'order_phone'], 'string'],
            [['order_person', 'user_id', 'restaurant_id'], 'integer'],
            [['order_date'], 'string'],
        ];
    }

    public function personsList() {
        return [
            1  => '1 персона',
            2  => '2 персоны',
            4  => '4 персоны',
            6  => '6 персон',
            10 => '10 персон',
        ];
    }

    public function saveOrder() {
        if(!$this->user_id) {
            if($user = User::findByUserEmail($this->user_email)) {
                \Yii::$app->user->login($user);

                $this->user_id = $user->id;

                if($this->save()) {
                    return true;
                }
            } else {
                $userModel   = new User();
                $userProfile = new UserProfile();

                $userModel->email     = $this->user_email;
                $userModel->user_type = User::USER_TYPE_USER;
                $password             = substr(md5(md5(microtime(true)) . md5(0, 100)), 0, 6);

                $userModel->setPassword($password);
                $userModel->generateAuthKey();

                if($userModel->save()) {
                    \Yii::$app->user->login($userModel);

                    $userProfile->profile_phone   = $this->order_phone;
                    $userProfile->profile_name    = $this->user_name;
                    $userProfile->profile_surname = $this->user_surname;
                    $userProfile->user_id         = $userModel->id;

                    $userProfile->save();
                }

                \Yii::$app->mailer->compose('register', ['user' => $userModel, 'password' => $password])
                    ->setFrom('lycifer31992@mail.ru')
                    ->setTo($userModel->email)
                    ->setSubject('Данные регистрации')
                    ->send();

                return true;
            }
        } else {
            return $this->save();
        }
    }
}