<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Class SignupForm
 * @package frontend\models
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $user_type;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required', 'message' => 'Поле не может быть пустым'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данный Email уже зарегистрирован'],

            ['password', 'required', 'message' => 'Поле не может быть пустым'],
            ['password', 'string', 'min' => 6, 'message' => 'Минимум 6 символов'],
            ['user_type', 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'    => 'Электронная почта *',
            'password' => 'Пароль *'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user            = new User();
        $user->email     = $this->email;
        $user->user_type = $this->user_type;

        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
