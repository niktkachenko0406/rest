/**
 * Main js code
 */


// Sticky menu-bar 
$(window).scroll(function(){
    if ($(this).scrollTop() > $('.top-bar').innerHeight()) {
        $('.main-menu').addClass("sticky");
    }
    else {
        $('.main-menu').removeClass("sticky");
    }    
});


jQuery(document).ready(function($) {

    // Sticky oreder box on single restaurant page
    if ($('div').hasClass('ordex-box-fix') && $(window).width() > 992) { 
        var orderBoxTop = $('.rest-header__order').offset().top;
        var orderBoxLeft = $('.rest-header__order').offset().left;
        var orderBoxMax = $('.similar-rest').offset().top - $('.abs-box').innerHeight();
        
        $(window).scroll(function(){
            if ($(this).scrollTop() > orderBoxTop - 80 && $(this).scrollTop() < orderBoxMax) {
                $('.rest-header__order').addClass("sticky")
                                        .removeClass("sticky-bottom")
                                        .css('left',orderBoxLeft + 'px');
            } else if ( $(this).scrollTop() > orderBoxTop - 75 && $(this).scrollTop() > orderBoxMax ) {
                $('.rest-header__order').removeClass("sticky")
                                        .addClass('sticky-bottom')
                                        .css('left','auto');   
            } else {
                $('.rest-header__order').removeClass("sticky").css('left','auto'); ;
            }
        });
    }

    if($_GET('rest')) {
        $('#js-rest-setting').click();
    }

    if($_GET('post')) {
        $('#js-rest-blog').click();
    }

    // Add branch color into underground select
    $('#filter-metro').on("chosen:ready", function() {
        if ($('#filter-metro option:selected').length == 0) {
            var selectedOpt = $('#filter-metro option').first();
        } else {
            var selectedOpt = $('#filter-metro option:selected');
        }
        var branchColorActive = selectedOpt.data('branch');
        $('#filter_metro_chosen .chosen-single span').html('<i class="m-branch ' + branchColorActive + '"></i>' + selectedOpt.text());
    });
    $('#filter-metro').on("chosen:showing_dropdown", function() {
        $('#filter_metro_chosen .chosen-results li').each(function(index, el) {
            var optionIndex = $(this).data('option-array-index');
            var branchColor = $('#filter-metro option:eq(' + optionIndex + ')').data('branch');
            $(this).prepend('<i class="m-branch ' + branchColor + '"></i>');
        });
    });
    $('#filter-metro').on("change", function() {
        var selectedOpt = $('#filter-metro option:selected');
        var branchColorActive = selectedOpt.data('branch');
        $('#filter_metro_chosen .chosen-single span').prepend('<i class="m-branch ' + branchColorActive + '"></i>');
    });

    // style for select
    $(".chosen-select").chosen({
        disable_search: true,
        inherit_select_classes: true
    });

    // Ranger slider for bill
    $("#bill-slider").slider({
        range: true,
        step: 1,
        values: [250, 500],
        min: 0,
        max: 600,
        create: function(event, ui) {
            var valMin = $(this).closest('.form-ranger').find('.min').val();
            var valMax = $(this).closest('.form-ranger').find('.max').val();
            $(this).find('.ui-corner-all:nth-child(2)').append('<span class="min-label">' + valMin + '</span>');
            $(this).find('.ui-corner-all:nth-child(3)').append('<span class="max-label">' + valMax + '</span>');
        },
        slide: function(event, ui) {
            $(this).closest('.form-ranger').find('.min').val($("#bill-slider").slider("values", 0));
            $(this).closest('.form-ranger').find('.max').val($("#bill-slider").slider("values", 1));
            $(this).find(".min-label").text($("#bill-slider").slider("values", 0));
            $(this).find(".max-label").text($("#bill-slider").slider("values", 1));
        }
    });


    // Datepicker in restaurant search
    $(".datepicker").datepicker({
        dateFormat: "dd MM",
        dayNamesMin: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Нд"],
        firstDay: 1,
        monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        minDate: new Date()
    });

    // Datepicker whith time in restaurant odrer box
    $('.datetimepicker').datetimepicker({
        dateFormat: "dd MM",
        dayNamesMin: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Нд"],
        firstDay: 1,
        monthNames: ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
        minDate: new Date(),
        currentText: 'Сегодня',
        closeText: 'ОК',
        timeFormat: ", HH:mm",
        showTime: false,
        controlType: {
            create: function(tp_inst, obj, unit, val, min, max, step){
                $('<input class="ui-timepicker-input" value="'+val+'" style="width:50%">')
                    .appendTo(obj)
                    .spinner({
                        min: min,
                        max: max,
                        step: step,
                        change: function(e,ui){ // key events
                            // don't call if api was used and not key press
                            if(e.originalEvent !== undefined)
                                tp_inst._onTimeChange();
                            tp_inst._onSelectHandler();
                        },
                        spin: function(e,ui){ // spin events
                            tp_inst.control.value(tp_inst, obj, unit, ui.value);
                            tp_inst._onTimeChange();
                            tp_inst._onSelectHandler();
                        }
                    });
                return obj;
            },
            options: function(tp_inst, obj, unit, opts, val){
                if(typeof(opts) == 'string' && val !== undefined)
                    return obj.find('.ui-timepicker-input').spinner(opts, val);
                return obj.find('.ui-timepicker-input').spinner(opts);
            },
            value: function(tp_inst, obj, unit, val){
                if(val !== undefined)
                    return obj.find('.ui-timepicker-input').spinner('value', val);
                return obj.find('.ui-timepicker-input').spinner('value');
            }
        },
        beforeShow: function() {
           $('#ui-datepicker-div').addClass($(this).data('loc'));
       }
    });

    // Google map init
    if ( $('div').hasClass('google-map') ) {
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            var lat = $('.js-rest-address').attr('data-lat') ? $('.js-rest-address').attr('data-lat') : 50.3794877;
            var lng = $('.js-rest-address').attr('data-lng') ? $('.js-rest-address').attr('data-lng') : 30.3737231;

            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(lat, lng),
                styles: [{
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#444444"
                    }]
                },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#ffffff"
                        }]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "poi.medical",
                        "elementType": "geometry.fill",
                        "stylers": [{
                            "visibility": "on"
                        },
                            {
                                "hue": "#cccccc"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [{
                            "visibility": "on"
                        },
                            {
                                "color": "#f6f6f6"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "simplified"
                        }]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.icon",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#46bcec"
                        },
                            {
                                "visibility": "on"
                            }
                        ]
                    }
                ]
            };
            var image = new google.maps.MarkerImage(
                '/img/pin.png',
                new google.maps.Size(60, 80),
                new google.maps.Point(0, 0),
                new google.maps.Point(42, 56)
            );
            var mapElement = document.getElementById('rest-map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: map,
                icon: image
            });
        }
    };

    //show all restaurants in map
    $('.js-show-map').click(function () {
        $.post('/restaurant/get-geo-positions', {data: $('.js-params').attr('data-params')}, function (data) {
            $('#js-rest-list-map').fadeToggle(100);
            setTimeout(function () {
                restMapInit(data);
            },101);
        }, 'json');
    });

    function restMapInit(locations) {
        // var locations = [
        //     ["Название ресторана", 50.468285, 30.450558, "/img/dish1.jpg", "Адрес ресторана"],
        //     ["Название ресторана", 50.462063, 30.451169, "/img/dish1.jpg", "Адрес ресторана"]
        // ];
        var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(50.468285, 30.450558),
            styles: [{
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#444444"
                }]
            },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [{
                        "color": "#ffffff"
                    }]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "poi.medical",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "visibility": "on"
                    },
                        {
                            "hue": "#cccccc"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [{
                        "visibility": "on"
                    },
                        {
                            "color": "#f6f6f6"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [{
                        "visibility": "simplified"
                    }]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [{
                        "color": "#46bcec"
                    },
                        {
                            "visibility": "on"
                        }
                    ]
                }
            ]
        };
        var map = new google.maps.Map(document.getElementById('js-rest-list-map'), mapOptions);

        var bounds     = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();
        var marker, i;
        var image = new google.maps.MarkerImage(
            '/img/pin.png',
            new google.maps.Size(60, 80),
            new google.maps.Point(0, 0),
            new google.maps.Point(42, 56)
        );

        $.each(locations, function (index, location) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(location.lat, location.lng),
                map: map,
                icon: image
            });

            bounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent('<img src="'+location.avatar+'" width="100" style="float: left; margin-right: 10px;"><b style="font-size: 16px;">' + location.name + '</b><br>' + location.address );
                    infowindow.open(map, marker);
                }
            })(marker, i));
        });

        map.fitBounds(bounds);
    };

    //Star rating
    function addStars(currentStar){
        currentStar.addClass('ion-ios-star').removeClass('ion-ios-star-outline');
        currentStar.prevAll('.star').addClass('ion-ios-star').removeClass('ion-ios-star-outline');
        currentStar.nextAll('.star').addClass('ion-ios-star-outline').removeClass('ion-ios-star');
    };

    $('.stars-rew span.star').mousemove(function(event) {
        addStars($(this));
    });
    $('.stars-rew span.star').mouseleave(function(event) {
        $(this).removeClass('ion-ios-star').addClass('ion-ios-star-outline');
        $(this).prevAll('.star').removeClass('ion-ios-star').addClass('ion-ios-star-outline');
        var starVal = $(this).parent().next('.rating-val').val();
        if (starVal > 0) {
            starVal = parseInt(starVal) - 1;
            var current = $(this).parent().find('.star:eq('+starVal+')');
            current.addClass('ion-ios-star').removeClass('ion-ios-star-outline');
            current.prevAll('.star').addClass('ion-ios-star').removeClass('ion-ios-star-outline');
        }
    });
    $('.stars-rew span.star').click(function() {
        $(this).parent().next('.rating-val').val(parseInt($(this).index() + 1));
        addStars($(this));
    });

    //Testimonial textfield size
    $('.string-textarea').focusin(function(event) {
        $(this).animate({
            height: '100px'
        }, 200);
    });
    $('.string-textarea').focusout(function(event) {
        $(this).css('height', 'auto');
    });

    
    //Show all list in sidebar
    $('.js-load-more').click(function(event) {
        event.preventDefault();
        if ($(this).hasClass('opened')) {
            $(this).removeClass('opened');
            $(this).text('Показать все');
            $(this).closest('.js-show-box').find('.event-to-show').slideUp('400');
        } else {
            $(this).addClass('opened');
            $(this).text('Скрыть');
            $(this).closest('.js-show-box').find('.event-to-show').slideDown('400');
        }
    });


    //Scroll to top
    $('.to-top').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    //Mobile menu
    $('.mob-menu').click(function(event) {
        $('.main-menu .navbar-nav').toggleClass('mob-opened');
    });

    //Add post
    $('.js-add-new').click(function(event) {
        event.preventDefault();
        $('.rest-news-list').addClass('hidden');
        $('.rest-create-new').removeClass('hidden');
    });

    $('.js-user-type').click(function (event) {
        $('.js-user-type-value').val($(this).attr('data-type'));
    });

    function $_GET(param) {
        var vars = {};
        window.location.href.replace( location.hash, '' ).replace(
            /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
            function( m, key, value ) { // callback
                vars[key] = value !== undefined ? value : '';
            }
        );

        if ( param ) {
            return vars[param] ? vars[param] : null;
        }
        return vars;
    }

    $('.filter-dropdown-toggle').on('click', function (event) {
        $('.dropdown').removeClass('open');
        $(this).parent().toggleClass('open');
    });
    $('.filter-link').on('click', function (event) {
        $(this).find('.filter-link__checkbox').toggleClass('active');
    });

    $('.js-subscriber').click(function (event) {
        event.preventDefault();
        var email = $('.js-subscriber-email').val();

        if(email) {
            $.post('/site/subscriber', {
                'email': email
            }, function (data) {
                if (data) {
                    $('.js-subscriber-email').val('');
                    alert('Вы подписались на рассылку');
                }
            }, 'json');
        }
    });

    $('.js-rest-order').click(function (event) {
        event.preventDefault();

        $.post('/restaurant/rest-order', {'restId': $(this).attr('data-rest-id'), params: $('.js-params').attr('data-params')}, function (data) {
            $('.js-order-form').empty().append(data.data);

            if(data.params) {
                $('.js-order-persons').val(data.params.person);
                $('.js-order-date').val(data.params.date);
            }

            initDatePicker();

            // $(".chosen-select").chosen({
            //     disable_search: true,
            //     inherit_select_classes: true
            // });
        });
    });

    function initDatePicker() {
        $('.datetimepicker').datetimepicker({
            dateFormat: "dd MM,",
            dayNamesMin: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Нд"],
            firstDay: 1,
            monthNames: ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
            minDate: new Date(),
            currentText: 'Сегодня',
            closeText: 'ОК',
            timeFormat: "HH:mm",
            showTime: false,
            controlType: {
                create: function(tp_inst, obj, unit, val, min, max, step){
                    $('<input class="ui-timepicker-input" value="'+val+'" style="width:50%">')
                        .appendTo(obj)
                        .spinner({
                            min: min,
                            max: max,
                            step: step,
                            change: function(e,ui){ // key events
                                // don't call if api was used and not key press
                                if(e.originalEvent !== undefined)
                                    tp_inst._onTimeChange();
                                tp_inst._onSelectHandler();
                            },
                            spin: function(e,ui){ // spin events
                                tp_inst.control.value(tp_inst, obj, unit, ui.value);
                                tp_inst._onTimeChange();
                                tp_inst._onSelectHandler();
                            }
                        });
                    return obj;
                },
                options: function(tp_inst, obj, unit, opts, val){
                    if(typeof(opts) == 'string' && val !== undefined)
                        return obj.find('.ui-timepicker-input').spinner(opts, val);
                    return obj.find('.ui-timepicker-input').spinner(opts);
                },
                value: function(tp_inst, obj, unit, val){
                    if(val !== undefined)
                        return obj.find('.ui-timepicker-input').spinner('value', val);
                    return obj.find('.ui-timepicker-input').spinner('value');
                }
            },
            beforeShow: function() {
                $('#ui-datepicker-div').addClass($(this).data('loc'));
            }
        });
    }
});

//Isotope grid
$(window).load( function(){
    $('.grid').isotope({
        itemSelector: '.grid-item',
        percentPosition: true,
        masonry: {
            columnWidth: '.grid-sizer'
        }
    });
});