$(document).ready(function() {
    $('.js-metro-checked').click(function (event) {
        event.preventDefault();

        var metroChecked = $(this).prev('.js-metro-val');

        if(metroChecked.attr('checked')) {
            metroChecked.removeProp('checked');
            metroChecked.removeAttr('checked');
        } else {
            metroChecked.prop('checked', 'checked');
            metroChecked.attr('checked', 'checked');
        }

        var checkedMetro = $('.js-filter-metro input:checkbox:checked').map(function() {return this.value;}).get();
        var params       = $.parseJSON($('.js-filter-metro').attr('data-params'));

        $.post('/restaurant/filter', {metro: checkedMetro.length > 0 ? checkedMetro : 0, params: params}, function (data) {

        }, 'json');
    });

    $('.js-type-checked').click(function (event) {
        event.preventDefault();

        var typeChecked = $(this).prev('.js-type-val');

        if(typeChecked.attr('checked')) {
            typeChecked.removeProp('checked');
            typeChecked.removeAttr('checked');
        } else {
            typeChecked.prop('checked', 'checked');
            typeChecked.attr('checked', 'checked');
        }

        var checkedType = $('.js-filter-type input:checkbox:checked').map(function() {return this.value;}).get();
        var params      = $.parseJSON($('.js-filter-type').attr('data-params'));

        $.post('/restaurant/filter', {type: checkedType.length > 0 ? checkedType : 0, params: params}, function (data) {

        }, 'json');
    });

    $('.js-kitchen-checked').click(function (event) {
        event.preventDefault();

        var kitchenChecked = $(this).prev('.js-kitchen-val');

        if(kitchenChecked.attr('checked')) {
            kitchenChecked.removeProp('checked');
            kitchenChecked.removeAttr('checked');
        } else {
            kitchenChecked.prop('checked', 'checked');
            kitchenChecked.attr('checked', 'checked');
        }

        var checkedKitchen = $('.js-filter-kitchen input:checkbox:checked').map(function() {return this.value;}).get();
        var params         = $.parseJSON($('.js-filter-kitchen').attr('data-params'));

        $.post('/restaurant/filter', {kitchen: checkedKitchen.length > 0 ? checkedKitchen : 0, params: params}, function (data) {

        }, 'json');
    });

    $('.js-feature-checked').click(function (event) {
        event.preventDefault();

        var featureChecked = $(this).prev('.js-feature-val');

        if(featureChecked.attr('checked')) {
            featureChecked.removeProp('checked');
            featureChecked.removeAttr('checked');
        } else {
           featureChecked.prop('checked', 'checked');
           featureChecked.attr('checked', 'checked');
        }

        var checkedFeature = $('.js-filter-feature input:checkbox:checked').map(function() {return this.value;}).get();
        var params         = $.parseJSON($('.js-filter-feature').attr('data-params'));

        $.post('/restaurant/filter', {feature: checkedFeature.length > 0 ? checkedFeature : 0, params: params}, function (data) {

        }, 'json');
    });

    $('.js-reason-checked').click(function (event) {
        event.preventDefault();

        var reasonChecked = $(this).prev('.js-reason-val');

        if(reasonChecked.attr('checked')) {
            reasonChecked.removeProp('checked');
            reasonChecked.removeAttr('checked');
        } else {
            reasonChecked.prop('checked', 'checked');
            reasonChecked.attr('checked', 'checked');
        }

        var checkedReasons = $('.js-filter-reason input:checkbox:checked').map(function() {return this.value;}).get();
        var params         = $.parseJSON($('.js-filter-reason').attr('data-params'));
        $.post('/restaurant/filter', {reason: checkedReasons.length > 0 ? checkedReasons : 0, params: params}, function (data) {

        }, 'json');
    });

    $('.js-favorites').click(function (event) {
        event.preventDefault();

        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).attr('data-status', 0);
        } else {
            $(this).addClass('active');
            $(this).attr('data-status', 1);
        }

        $.post('/restaurant/toggle-favorites', {
            'restId': $(this).attr('data-rest-id'),
            'status': $(this).attr('data-status')
        }, function (data) {

        }, 'json');
    });

    $('.js-search').change(function (event) {
        window.location.href = 'restaurant/index?person=' + $('.js-person').val() + '&date=' + $('.js-date').val() + '&search=' + $(this).val()
    });
});
