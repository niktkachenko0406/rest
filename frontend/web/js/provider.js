$(document).ready(function() {
    $('.js-filter-checked').click(function (event) {
        event.preventDefault();

        var filterChecked = $(this).prev('.js-filter-val');

        if(filterChecked.attr('checked')) {
            filterChecked.removeProp('checked');
            filterChecked.removeAttr('checked');
        } else {
            filterChecked.prop('checked', 'checked');
            filterChecked.attr('checked', 'checked');
        }

        var checkedFilter = $('#js-provider-filter-form').find('input:checkbox:checked').map(function() {return this.value;}).get();
        var params        = $.parseJSON($(this).parents('.js-filter-provider').attr('data-params'));

        $.post('/provider/filter', {filter: checkedFilter.length > 0 ? checkedFilter : 0, params: params}, function (data) {

        }, 'json');
    });
});
