$(document).ready(function() {

    $("#js-avatar").change(function(){
        readURL(this);
    });

    $('#js-rest-setting').click(function (event) {
        event.preventDefault();

        $.get('/restaurant/rest-setting', function (data) {
            $('.js-rest-setting-tab').html(data);
            initSelect();
            initRangeSchedule();
            uploadRestAvatar();
            uploadGallery();
            deleteImage();
            deleteAllImage();
            initMetroSelect();
            metroOptions();

        })
    });

    $('.js-add-post').click(function (event) {
        event.preventDefault();

        $.get('/blog/post-save', {restId: $(this).attr('data-rest-id')}, function (data) {
            $('.js-form-post-bloc').html(data).removeClass('hidden');
            blogPreview();
        })
    });

    $('.js-post-edit').click(function (event) {
        event.preventDefault();

        var blogId = $(this).attr('data-id');

        $.get('/blog/post-save', {restId: $(this).attr('data-rest-id'), id: blogId}, function (data) {
            $('.js-form-post-bloc').html(data).removeClass('hidden');
            $('.js-list-posts').addClass('hidden');
        })
    });

    $('.js-favorites-user-order').click(function (event) {
        event.preventDefault();

        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).attr('data-status', 0);
        } else {
            $(this).addClass('active');
            $(this).attr('data-status', 1);
        }

        $.post('/restaurant/toggle-favorites', {
            'restId': $(this).attr('data-rest-id'),
            'status': $(this).attr('data-status')
        }, function (data) {

        }, 'json');
    });


    $('.js-favorites-user-rest').click(function (event) {
        event.preventDefault();

        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).attr('data-status', 0);
            $(this).parents('.js-favorite-rest').remove();
        }

        $.post('/restaurant/toggle-favorites', {
            'restId': $(this).attr('data-rest-id'),
            'status': $(this).attr('data-status')
        }, function (data) {

        }, 'json');
    });

    function blogPreview() {
        $("#js-blog-img").change(function(){
            if (this.files && this.files[0]) {
                var reader    = new FileReader();
                reader.onload = function (e) {
                    $('#js-blog-preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        });
    }

    function metroOptions() {
        $('#js-district-id').change(function (event) {
            var districtId = $(this).val();

            $.get('/restaurant/get-metro',
                {districtId: districtId},
                function (response) {
                    if (response.success) {
                        $('.js-rest-metro').empty();
                        $('.js-metro-select').remove();
                        $.each(response.data, function (index, value) {
                            $('.js-rest-metro').append(
                                '<option data-branch="green" value="' + value.station_id + '">' + value.station_name + '</option>'
                            );
                        });
                        initMetroSelect();
                    }
                }
            );
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader    = new FileReader();
            reader.onload = function (e) {
                $('#js-profile-avatar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function uploadRestAvatar() {
        var uploader = new plupload.Uploader({
            browse_button: 'js-rest-avatar', // this can be an id of a DOM element or the DOM element itself
            url: '/restaurant/upload-avatar',
            multipart: true,
            multipart_params: {restId: $('#js-rest-avatar').attr('data-id')},
            file_data_name: 'rest_avatar'
        });

        uploader.init();

        uploader.bind('FilesAdded', function(up, files) {
            uploader.start();
        });

        uploader.bind('FileUploaded', function(up, file, result) {
            var response = $.parseJSON(result.response);

            $('#js-restaurant-avatar').attr('src', response.filePath);
        });
    }

    function uploadGallery() {
        var uploader = new plupload.Uploader({
            browse_button: 'js-upload-gallery', // this can be an id of a DOM element or the DOM element itself
            url: '/restaurant/upload-gallery',
            multipart: true,
            multipart_params: {restId: $('#js-rest-avatar').attr('data-id')},
            multi_selection: true,
            file_data_name: 'gallery_img'
        });

        uploader.init();

        uploader.bind('FilesAdded', function(up, files) {
            $.each(files, function (file) {
                $('.js-added-files').append('<li class="gall-upload-item js-preload"><span class="upload-state js-upload-progress">0%</span></li>');
            });

            uploader.start();
        });

        uploader.bind('UploadProgress', function(up, file) {
            $('.js-upload-progress').text(file.percent + '%');
        });

        uploader.bind('FileUploaded', function(up, file, result) {
            var response = $.parseJSON(result.response);

            $('.js-added-files .js-preload').first().remove();
            $('.js-added-files').append('' +
                '<li class="gall-upload-item">' +
                '<a href="#" class="upload-close ion-ios-close js-delete-image" data-file-id="' + response.fileId + '"></a>' +
                '<img src="' + response.filePath + '" alt="">' +
                '</li>' +
                '')
        });
    }

    function deleteImage() {
        $('.js-delete-image').click(function (event) {
            event.preventDefault();

            var fileId = $(this).attr('data-file-id');
            var that   = $(this);

            $.get('/restaurant/remove-image',
                {id: fileId},
                function (data) {
                    if(data.success) {
                        that.parents('li').remove();
                    }
                }, 'json'
            );
        });
    }

    function deleteAllImage() {
        $('.js-remove-image-all').click(function (event) {
            event.preventDefault();

            var restId = $('#js-rest-avatar').attr('data-id');
            var that   = $(this);

            $.get('/restaurant/remove-image-all',
                {restId: restId},
                function (data) {
                    if(data.success) {
                        $('.js-added-files').empty();
                    }
                }, 'json'
            );
        });
    }

    function initRangeSchedule() {
        $(".slider-time").slider({
            range: true,
            min: 0,
            max: 1440,
            step: 10,
            values: [420, 1320],
            create: function(event, ui) {
                var valMin = $(this).closest('.form-ranger').find('.min').val();
                var valMax = $(this).closest('.form-ranger').find('.max').val();

                $(this).find('.ui-corner-all:nth-child(2)').append('<span class="min-label">' + valMin + '</span>');
                $(this).find('.ui-corner-all:nth-child(3)').append('<span class="max-label">' + valMax + '</span>');
                if (!$(this).closest('.schedule-day').find('.checkbox').prop('checked')) {
                    $(this).slider( "disable");
                }
            },
            slide: function(e, ui) {
                var maxHours = Math.floor(ui.values[1] / 60);
                var maxMinutes = ui.values[1] - (maxHours * 60);
                if(maxMinutes.toString().length == 1) maxMinutes = '0' + maxMinutes;

                var minHours = Math.floor(ui.values[0] / 60);
                var minMinutes = ui.values[0] - (minHours * 60);
                if(minMinutes.toString().length == 1) minMinutes = '0' + minMinutes;

                $(this).closest('.form-ranger').find('.min').val(minHours + ':' + minMinutes);
                $(this).closest('.form-ranger').find('.max').val(maxHours + ':' + maxMinutes);

                $(this).find(".min-label").text(minHours + ':' + minMinutes);
                $(this).find(".max-label").text(maxHours + ':' + maxMinutes);
            }
        });

        $('.js-schedule-day').change(function(event) {
            if ($(this).prop('checked')) {
                $(this).closest('.schedule-day').find('.slider-time').slider( "enable");
            } else {
                $(this).closest('.schedule-day').find('.slider-time').slider( "disable");
            }
        });
    }

    function initSelect() {
        $(".chosen-select").chosen({
            disable_search: true,
            inherit_select_classes: true
        });

        // $('.js-rest-metro').on("chosen:showing_dropdown", function() {
        //     $('.js-rest-metro + .chosen-container .chosen-results li').each(function(index, el) {
        //         var optionIndex = $(this).data('option-array-index');
        //         var branchColor = $('select.js-rest-metro option:eq(' + optionIndex + ')').data('branch');
        //         $(this).prepend('<i class="m-branch ' + branchColor + '"></i>');
        //     });
        // });

        // $(".js-rest-metro").chosen({
        //     placeholder_text_multiple: 'Выберите метро'
        // });
    }

    function initMetroSelect() {
        var metroSelect = $('.js-rest-metro');
        metroSelect.hide();
        var newMetroSelect = document.createElement('div');
        var selectedWrap = document.createElement('a');
        var selectedList = document.createElement('ul');
        var optionList = document.createElement('ul');

        newMetroSelect.className = 'dropdown metro-select js-metro-select';
        selectedWrap.className = 'filter-dropdown-toggle dropdown-toggle selected-station';
        selectedList.className = 'list-unstyled js-selected-station';
        optionList.className = 'filter-dropdown dropdown-menu station-list';

        $('.js-rest-metro option').each(function (index, el) {
            var optionItem = document.createElement('li');
            var optionBranch = document.createElement('span');
            var optionText = document.createElement('b');
            var branchColor = $(el).data('branch');
            var branchValue = $(el).val();
            var branchName = $(el).text()
            optionBranch.className = 'm-branch ' + branchColor;
            optionItem.dataset.branch = branchColor;
            optionItem.dataset.value = branchValue;
            optionText.innerHTML = branchName;
            if ($(el).attr('selected')) {
                optionItem.className = 'js-chose-station active';
                var removeLi = document.createElement('span');
                removeLi.className = 'remove-item js-remove-station';
                var li = document.createElement('li');
                li.className = 'branch-' + branchColor;
                li.dataset.selectValue = branchValue;
                li.innerHTML = branchName;
                li.appendChild(removeLi);
                selectedList.append(li);
            } else {
                optionItem.className = 'js-chose-station';
            }
            optionItem.appendChild(optionBranch);
            optionItem.appendChild(optionText);
            optionList.appendChild(optionItem);
        });

        selectedWrap.appendChild(selectedList);
        newMetroSelect.appendChild(selectedWrap);
        newMetroSelect.appendChild(optionList);
        $(newMetroSelect).insertAfter(metroSelect);

        $('.filter-dropdown-toggle').on('click', function (event) {
            event.stopPropagation();
            $(this).parent().addClass('open');
        });

        $('.js-chose-station').on('click', function (event) {
            event.stopPropagation();
            var trueSelect = $('.js-rest-metro');
            var selectedList = $('.js-selected-station');
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                var branchColor = $(this).data('branch');
                var removeLi = document.createElement('span');
                removeLi.className = 'remove-item js-remove-station';
                var li = document.createElement('li');
                li.className = 'branch-' + branchColor;
                li.dataset.selectValue = $(this).data('value');
                li.innerHTML = $(this).find('b').text();
                li.appendChild(removeLi);
                selectedList.append(li);
                trueSelect.find('option[value='+$(this).data('value')+']').prop('selected', true);
            } else {
                selectedList.find('li[data-select-value='+$(this).data('value')+']').remove();
                trueSelect.find('option[value='+$(this).data('value')+']').prop('selected', false);
                $(this).removeClass('active');
            }
        });

        $('.js-selected-station').on('click', '.js-remove-station', function (event) {
            var item = $(this).parent().data('select-value');
            var trueSelect = $('.js-rest-metro');
            $(this).parent().remove();
            trueSelect.find('option[value='+item+']').prop('selected', false);
            $('.js-chose-station[data-value='+item+']').removeClass('active');
        });

        $(window).click(function() {
            $('.dropdown').removeClass('open');
        });

    }

});
