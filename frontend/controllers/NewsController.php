<?php

namespace frontend\controllers;

use common\models\Comments;
use common\models\News;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * Class NewsController
 * @author Maksim Nikitenko <lycifer3.mn@gmail.com>
 * @package frontend\controllers
 */
class NewsController extends BaseController {

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::getModels(),
            'pagination' => [
                'pageSize'       => \Yii::$app->params['perPage'],
                'forcePageParam' => false,
                'pageSizeParam'  => false,
                'validatePage'   => false,
            ],
        ]);

        return $this->render('index', [
            'provider' => $dataProvider
        ]);
    }

    public function actionView($uri)
    {
        $news          = News::findModelByUri($uri);
        $lastNews      = News::getLastNews();
        $commentsModel = new Comments();

        return $this->render('view', [
            'news'          => $news,
            'lastNews'      => $lastNews,
            'user'          => $this->user,
            'commentsModel' => $commentsModel
        ]);
    }

    /**
     * Save comment
     * @param $uri
     * @return Response
     */
    public function actionSaveComment($uri) {
        $request      = \Yii::$app->request;
        $commentModel = new Comments();

        if($request->isPost && $commentModel->load($request->post())) {
            $commentModel->user_id        = $this->user->id;
            $commentModel->comment_entity = Comments::ENTITY_NEWS;

            if($commentModel->save()) {
                \Yii::$app->session->setFlash('comment', 'Коментарий добавлен, появится на сайте после модерации');

                return $this->redirect(Url::to(['/news/view', 'uri' => $uri, '#' => 'commentsForm']));
            } else {
                \Yii::$app->session->setFlash('comment', 'Что то пошло не так попробуйте еще раз');

                return $this->redirect(Url::to(['/news/view', 'uri' => $uri, '#' => 'commentsForm']));
            }
        }
    }
}