<?php
 namespace frontend\controllers;

 use common\models\Providers;
 use common\models\ProvidersProducts;
 use yii\data\ActiveDataProvider;
 use yii\helpers\Url;
 use yii\helpers\Json;
 use common\models\User;
 use Yii;

 class ProviderController extends BaseController {

     public function actionIndex() {
         if(!Yii::$app->user->isGuest && Yii::$app->user->identity->user_type != User::USER_TYPE_USER) {
             $params = \Yii::$app->request->get();

             $dataProvider = new ActiveDataProvider([
                 'query'      => Providers::getModels($params),
                 'pagination' => [
                     'pageSize'       => \Yii::$app->params['perPage'],
                     'forcePageParam' => false,
                     'pageSizeParam'  => false,
                     'validatePage'   => false,
                 ],
             ]);

             return $this->render('index', [
                 'providers' => $dataProvider,
                 'params'    => $params
             ]);
         } else{
             return $this->goHome();
         }
     }

     public function actionView($uri) {
         if(!Yii::$app->user->isGuest && Yii::$app->user->identity->user_type != User::USER_TYPE_USER) {
             $provider    = Providers::findModelByUri($uri);
             $products    = ProvidersProducts::findProducts($provider->provider_id);
             $addressData = file_get_contents(
                 sprintf(
                     'https://maps.google.com/maps/api/geocode/json?address=%s&sensor=false&language=ru',
                     urlencode($provider->city->city_name . ', ' . $provider->provider_address)
                 )
             );

             $addressData = Json::decode($addressData);
             $coordinates = $addressData['results'][0]['geometry']['location'];

             return $this->render('view', [
                 'provider'    => $provider,
                 'products'    => $products,
                 'coordinates' => $coordinates
             ]);
         } else{
             return $this->goHome();
         }
     }

     public function actionFilter() {
        $request = \Yii::$app->request;

         if($request->isAjax && $request->isPost) {
             $post = $request->post();

             if($post['filter']) {
                 $filter = $post['filter'] ? implode(' ', $post['filter']) : null;
                 $url    = urldecode($filter);

                 return $this->redirect(Url::to(['/provider/index', 'filter' => $url]));
             }
         }
     }
 }