<?php

namespace frontend\controllers;

use common\lybrary\upload\Upload;
use common\models\User;
use common\models\UserProfile;
use common\models\Favorites;
use common\models\Orders;
use common\models\Restaurants;
use frontend\controllers\BaseController;
use frontend\models\PasswordChangeForm;
use yii\base\Exception;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;

class UserController extends BaseController {

    /**
     * User profile
     * @return string|Response
     * @throws Exception
     */
    public function actionIndex() {
        /** @var UserProfile $userProfile */
        $request        = \Yii::$app->request;
        $userProfile    = $this->user->userProfile ?: new UserProfile();
        $passwordChange = new PasswordChangeForm($this->user);
        $orderRests     = Orders::getOrderRestaurants($this->getAuthId());
        $favoriteRests  = [];
        $favorites      = [];

        if($request->isPost && $userProfile->load($request->post())) {
            $userProfile->user_id = $this->getAuthId();
            $file                 = UploadedFile::getInstance($userProfile, 'profile_avatar');
            $upload               = new Upload();

            if($file) {
                if($userProfile->profile_avatar) {
                    Upload::clearThumbnails('profile', $userProfile->profile_avatar, ['avatars']);
                }

                $upload->setFile($file)
                       ->setFileExtensions(['jpg', 'png', 'jpeg'])
                       ->setMaxSize(6)
                       ->setFileRename(true)
                       ->addThumbnails([[
                           'path'   => '/upload/profile/avatars',
                           'width'  => 300,
                           'height' => 300,
                           'type'   => 'crop'
                       ]])
                       ->upload();

                if (!$upload->isErrorExists()) {
                    $userProfile->profile_avatar = $upload->getFileName();
                }
            }

            $passwordChange->load($request->post());

            if(\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($passwordChange);
            }

            try {
                $passwordChange->changePassword();
            }catch (Exception $e) {
                throw new Exception($e->getMessage());
            }

            if($userProfile->save()) {
                return $this->redirect($this->getAuthType() == User::USER_TYPE_REST ? Url::to('/restaurant/profile') : Url::to('/user/index'));
            }
        }

        if($favorites = Favorites::getFavoritesRestaurants($this->getAuthId())) {
            $favoriteRests = Restaurants::getFavoriteRests($favorites);
        }

        return $this->render('profile', [
            'user'           => $this->user,
            'profile'        => $userProfile,
            'passwordChange' => $passwordChange,
            'favoriteRests'  => $favoriteRests,
            'favorites'      => $favorites,
            'orderRests'     => $orderRests
        ]);
    }
}