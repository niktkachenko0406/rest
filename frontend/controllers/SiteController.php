<?php
namespace frontend\controllers;

use common\models\Blog;
use common\models\News;
use common\models\Restaurants;
use common\models\Favorites;
use common\models\Subscribers;
use common\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $restaurants = Restaurants::getMainRestaurants();
        $favorites   = Favorites::getFavoritesRestaurants($this->getAuthId());
        $news        = News::getLastNews(6);
        $blogPosts   = Blog::getLastPosts(4);

        return $this->render('index', [
            'restaurants' => $restaurants,
            'news'        => $news,
            'blogPosts'   => $blogPosts,
            'favorites'   => $favorites
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {
            if(\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }

            if($model->login()) {
                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            return $this->goHome();
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model               = new SignupForm();
        $restModel           = new Restaurants();
        $restModel->scenario = Restaurants::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post())) {
            if(\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }

            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    if($user->user_type == User::USER_TYPE_REST) {
                        $restModel->createRest($user->id);
                    }

                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionConsulting()
    {
        if(!Yii::$app->user->isGuest && Yii::$app->user->identity->user_type != User::USER_TYPE_USER) {
            return $this->render('consulting');
        } else{
            return $this->goHome();
        }
    }

    /**
     * Added subscriber.
     *
     * @return mixed
     */
    public function actionSubscriber() {
        $model   = new Subscribers();
        $request = \Yii::$app->request;

        if ($request->isAjax && $request->isPost) {
            $post = $request->post();
            $model->email = $post['email'];

            return $model->save() ? true : false;
        }
    }

    /**
     * View search result
     */
    public function actionSearch() {
        $request = \Yii::$app->request;
        $get     = $request->get();

        if(!empty($search = trim(strip_tags($get['search'])))) {
            $news = News::getNewsForSearch($search);
            $blog = Blog::getPostsForSearch($search);

            return $this->render('search', [
                'allNews' => $news,
                'allBlog' => $blog,
                'search'  => $search
            ]);
        } else {
            return $this->redirect(\Yii::$app->request->referrer);
        }
    }
}
