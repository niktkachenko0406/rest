<?php

namespace frontend\controllers;

use common\lybrary\upload\Upload;
use common\models\Blog;
use common\models\Comments;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class BlogController
 * @author Maksim Nikitenko <lycifer3.mn@gmail.com>
 * @package frontend\controllers
 */
class BlogController extends BaseController {

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['post-save', 'delete'],
                'rules' => [
                    [
                        'actions' => ['post-save', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Blog::getModels(),
            'pagination' => [
                'pageSize' => \Yii::$app->params['perPage'],
            ],
        ]);

        return $this->render('index', [
            'provider' => $dataProvider
        ]);
    }

    public function actionView($uri)
    {
        $blog          = Blog::findModelByUri($uri);
        $lastPosts     = Blog::getLastPosts();
        $commentsModel = new Comments();

        return $this->render('view', [
            'blog'          => $blog,
            'lastPosts'     => $lastPosts,
            'user'          => $this->user,
            'commentsModel' => $commentsModel
        ]);
    }

    /**
     * Save comment
     * @param $uri
     * @return Response
     */
    public function actionSaveComment($uri) {
        $request      = \Yii::$app->request;
        $commentModel = new Comments();

        if($request->isPost && $commentModel->load($request->post())) {
            $commentModel->user_id        = $this->user->id;
            $commentModel->comment_entity = Comments::ENTITY_BLOG;

            if($commentModel->save()) {
                \Yii::$app->session->setFlash('comment', 'Коментарий добавлен, появится на сайте после модерации');

                return $this->redirect(Url::to(['/blog/view', 'uri' => $uri, '#' => 'commentsForm']));
            } else {
                \Yii::$app->session->setFlash('comment', 'Что то пошло не так попробуйте еще раз');

                return $this->redirect(Url::to(['/blog/view', 'uri' => $uri, '#' => 'commentsForm']));
            }
        }
    }

    /**
     * Create/update blog post
     * @return string
     */
    public function actionPostSave() {
        $request = \Yii::$app->request;
        $params  = $request->get();
        $model   = new Blog();

        if($params['id']) {
            $model = Blog::findModel($params['id']);
        }

        if($request->isAjax) {
            return $this->renderAjax('_blog-form', [
                'model'  => $model,
                'restId' => $params['restId']
            ]);
        }

        if($request->isPost && $model->load($request->post())) {
            $file   = UploadedFile::getInstance($model, 'blog_image');
            $upload = new Upload();

            if($file) {
                if($model->blog_image) {
                    Upload::clearThumbnails('blog', $model->blog_image);
                }

                $upload->setFile($file)
                       ->setFileExtensions(['jpg', 'png', 'jpeg'])
                       ->setMaxSize(6)
                       ->setFileRename(true)
                       ->setFileCompress(true)
                       ->addThumbnails([['path' => '/upload/blog']])
                       ->setQuality(60)
                       ->setMinCompressSize(0)
                       ->upload();

                if (!$upload->isErrorExists()) {
                    $model->blog_image = $upload->getFileName();
                }
            }

            if($model->save()) {
                return $this->redirect(Url::to(['/restaurant/profile', 'post' => true]));
            }
        }
    }

    /**
     * Delete blog post
     * @param $id
     * @return Response
     */
    public function actionDelete($id) {
        if($id) {
            $model = Blog::findModel($id);

            if($model->delete()) {
                return $this->redirect(Url::to(['/restaurant/profile', 'post' => true]));
            }
        }
    }
}