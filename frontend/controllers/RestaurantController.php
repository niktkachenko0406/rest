<?php
namespace frontend\controllers;

use Codeception\Module\Yii1;
use common\lybrary\upload\Upload;
use common\models\Comments;
use common\models\Districts;
use common\models\Favorites;
use common\models\Metro;
use common\models\RestaurantFeatures;
use common\models\RestaurantGallery;
use common\models\RestaurantKitchen;
use common\models\RestaurantMetro;
use common\models\RestaurantReasons;
use common\models\Restaurants;
use common\models\RestaurantTypes;
use common\models\UserProfile;
use frontend\controllers\BaseController;
use frontend\models\OrderForm;
use frontend\models\PasswordChangeForm;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\debug\models\timeline\DataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class RestaurantController
 * @author Maksim Nikitenko <lycifer3.mn@gmail.com>
 * @package frontend\controllers
 */
class RestaurantController extends BaseController {
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['profile'],
                'rules' => [
                    [
                        'actions' => ['profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['upload-avatar', 'upload-gallery'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Restaurants list
     * @return string
     */
    public function actionIndex($sort = null)
    {
        $params       = \Yii::$app->request->get();
        $dataProvider = new ActiveDataProvider([
            'query' => Restaurants::getModels($params),
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc'   => ['user.created_at' => SORT_DESC],
                        'desc'  => ['user.created_at' => SORT_ASC],
                        'label' => 'дате добавления',
                    ],
                    'price' => [
                        'asc'   => ['restaurant_average_check' => SORT_DESC],
                        'desc'  => ['restaurant_average_check' => SORT_ASC],
                        'label' => 'стоимости',
                    ],
                    'rating' => [
                        'asc'   => ['restaurant_rating' => SORT_DESC],
                        'desc'  => ['restaurant_rating' => SORT_ASC],
                        'label' => 'рейтингу',
                    ]
                ],
            ],
            'pagination' => [
                'pageSize'       => \Yii::$app->params['perPage'],
                'forcePageParam' => false,
                'pageSizeParam'  => false,
                'validatePage'   => false,
            ],
        ]);

        $favorites = Favorites::getFavoritesRestaurants($this->getAuthId());

        return $this->render('index', [
            'provider'  => $dataProvider,
            'sort'      => $sort,
            'params'    => $params,
            'favorites' => $favorites
        ]);
    }

    public function actionGetGeoPositions() {
        $request = \Yii::$app->request;

        if($request->isAjax && $request->isPost) {
            $post        = json_decode($request->post('data'), true);
            $restaurants = Restaurants::getModels($post, [
                'restaurants.restaurant_address',
                'restaurants.restaurant_name',
                'restaurants.restaurant_avatar',
                'cities.city_name'
            ])->asArray()->all();

            $locations = [];

            foreach ($restaurants as $restaurant) {
                $addressData   = file_get_contents(
                    sprintf(
                        'https://maps.google.com/maps/api/geocode/json?address=%s&sensor=false&language=ru',
                        urlencode($restaurant['city_name'] . ', ' . $restaurant['restaurant_address'])
                    )
                );

                $addressData = Json::decode($addressData);
                $coordinates = $addressData['results'][0]['geometry']['location'];

                $locations[] = [
                    'name'    => $restaurant['restaurant_name'],
                    'avatar'  => '/upload/restaurant/avatars/' . $restaurant['restaurant_avatar'],
                    'address' => $restaurant['restaurant_address'],
                    'lat'     => $coordinates['lat'],
                    'lng'     => $coordinates['lng'],
                ];
            }

            \Yii::$app->response->format = Response::FORMAT_JSON;

            return $locations;
        }
    }

    public function actionFilter() {
        $request = \Yii::$app->request;

        if($request->isAjax && $request->isPost) {
            $post   = $request->post();
            $params = [];

            if(isset($post['params'])) {
                $params = $post['params'];
            }

            if($post['metro']) {
                $url = $post['metro'] ? urldecode(implode(' ', $post['metro'])) : null;

                return $this->redirect(Url::to(array_merge($params, ['/restaurant/index', 'metro' => $url])));
            }

            if($post['type']) {
                $url = $post['type'] ? urldecode(implode(' ', $post['type'])) : null;

                return $this->redirect(Url::to(array_merge($params, ['/restaurant/index', 'type' => $url])));
            }

            if($post['kitchen']) {
                $url = $post['kitchen'] ? urldecode(implode(' ', $post['kitchen'])) : null;

                return $this->redirect(Url::to(array_merge($params, ['/restaurant/index', 'kitchen' => $url])));
            }

            if($post['feature']) {
                $url = $post['feature'] ? urldecode(implode(' ', $post['feature'])) : null;

                return $this->redirect(Url::to(array_merge($params, ['/restaurant/index', 'feature' => $url])));
            }

            if($post['reason']) {
                $url = $post['reason'] ? urldecode(implode(' ', $post['reason'])) : null;

                return $this->redirect(Url::to(array_merge($params, ['/restaurant/index', 'reason' => $url])));
            }
        }
    }

    /**
     * View restaurant profile
     * @param $uri
     * @return string
     */
    public function actionView($uri) {
        $commentsModel = new Comments();
        $orderModel    = new OrderForm();
        $restaurant    = Restaurants::findModel(null, $uri);
        $addressData   = file_get_contents(
            sprintf(
            'https://maps.google.com/maps/api/geocode/json?address=%s&sensor=false&language=ru',
                urlencode($restaurant->city->city_name . ', ' . $restaurant->restaurant_address)
            )
        );

        $addressData = Json::decode($addressData);
        $coordinates = $addressData['results'][0]['geometry']['location'];
        $favorites   = Favorites::getFavoritesRestaurants($this->getAuthId());
        $kitchens    = [];

        foreach($restaurant->kitchen as $kitchen) {
            $kitchens[] = $kitchen->uri;
        }

        $similarsRests = $restaurant::getSimilarRestaurants($restaurant->restaurant_id, $kitchens);

        return $this->render('view', [
            'restaurant'    => $restaurant,
            'coordinates'   => $coordinates,
            'user'          => $this->user,
            'commentsModel' => $commentsModel,
            'favorites'     => $favorites,
            'similars'      => $similarsRests,
            'orderModel'    => $orderModel
        ]);
    }

    /**
     * Save comment
     * @param $uri
     * @return Response
     */
    public function actionSaveComment($uri) {
        $request      = \Yii::$app->request;
        $commentModel = new Comments();

        if($request->isPost && $commentModel->load($request->post())) {
            $commentModel->user_id        = $this->user->id;
            $commentModel->comment_entity = Comments::ENTITY_REST;

            if($commentModel->save()) {
                \Yii::$app->session->setFlash('comment', 'Коментарий добавлен, появится на сайте после модерации');

                return $this->redirect(Url::to(['/restaurant/view', 'uri' => $uri, '#' => 'commentsForm']));
            } else {
                \Yii::$app->session->setFlash('comment', 'Что то пошло не так попробуйте еще раз');

                return $this->redirect(Url::to(['/restaurant/view', 'uri' => $uri, '#' => 'commentsForm']));
            }
        }
    }

    /**
     * Restaurant profile
     * @return string
     */
    public function actionProfile() {
        $rest          = Restaurants::findModel($this->getAuthId());
        $favorites     = [];
        $favoriteRests = [];

        if($favorites = Favorites::getFavoritesRestaurants($this->getAuthId())) {
            $favoriteRests = Restaurants::getFavoriteRests($favorites);
        }

        return $this->render('profile', [
            'rest'           => $rest,
            'profile'        => $this->user->userProfile ?: new UserProfile(),
            'passwordChange' => new PasswordChangeForm($this->user),
            'favoriteRests'  => $favoriteRests,
            'favorites'      => $favorites
        ]);
    }

    /**
     * Save/edit restaurant info
     * @return string
     */
    public function actionRestSetting() {
        /** @var Restaurants $model */
        $model   = Restaurants::findModel($this->getAuthId());
        $request = \Yii::$app->request;

        if($request->isAjax) {
            return $this->renderAjax('_restaurant-setting', [
                'model' => $model
            ]);
        }

        if($request->isPost) {
            $post       = $request->post();
            $types      = [];
            $kitchens   = [];
            $features   = [];
            $metroData  = [];
            $reasonData = [];

            if($model->load($post) && $model->validate()) {
                if($model->save()) {
                    if($post['types']) {
                        $i = 0;
                        foreach ($post['types'] as $type) {
                            $types[$i][] = $model->restaurant_id;
                            $types[$i][] = $type;
                            $i++;
                        }

                        RestaurantTypes::multiInsert($types, $model->restaurant_id);
                    }

                    if($post['kitchens']) {
                        $i = 0;
                        foreach ($post['kitchens'] as $kitchen) {
                            $kitchens[$i][] = $model->restaurant_id;
                            $kitchens[$i][] = $kitchen;
                            $i++;
                        }

                        RestaurantKitchen::multiInsert($kitchens,  $model->restaurant_id);
                    }

                    if($post['features']) {
                        $i = 0;
                        foreach ($post['features'] as $feature) {
                            $features[$i][] = $model->restaurant_id;
                            $features[$i][] = $feature;
                            $i++;
                        }

                        RestaurantFeatures::multiInsert($features,  $model->restaurant_id);
                    }

                    if($post['metroId']) {
                        $i = 0;
                        foreach ($post['metroId'] as $metro) {
                            $metroData[$i][] = $model->restaurant_id;
                            $metroData[$i][] = $metro;
                            $i++;
                        }

                        RestaurantMetro::multiInsert($metroData,  $model->restaurant_id);
                    }

                    if($post['reasons']) {
                        $i = 0;
                        foreach ($post['reasons'] as $reason) {
                            $reasonData[$i][] = $model->restaurant_id;
                            $reasonData[$i][] = $reason;
                            $i++;
                        }

                        RestaurantReasons::multiInsert($reasonData,  $model->restaurant_id);
                    }

                    return $this->redirect(Url::to(['/restaurant/profile', 'rest' => '1']));
                }
            }
        }
    }

    /**
     * Get districts by city id
     * @return array
     */
    public function actionGetDistricts()
    {
        $request = \Yii::$app->request;
        $data    = [];

        if($request->isAjax && $request->isPost) {
            $post      = $request->post();
            $districts = Districts::getDistricts($post['depdrop_parents'][0]);
            $i         = 0;

            foreach($districts as $district) {
                $data[$i]['id']   = $district->district_id;
                $data[$i]['name'] = $district->district_name;
                $i++;
            }

            \Yii::$app->response->format = Response::FORMAT_JSON;

            return ['output' => $data, 'selected' => ''];
        }
    }

    /**
     * Get list metro stations
     * @return array
     */
    public function actionGetMetro() {
        if(\Yii::$app->request->isAjax) {
            $params        = \Yii::$app->request->get();
            $metroStations = Metro::getMetroByDistrict($params['districtId']);

            \Yii::$app->response->format = Response::FORMAT_JSON;

            return ['success' => true, 'data' => $metroStations];
        }
    }

    /**
     * Upload restaurant avatar
     * @return array
     */
    public function actionUploadAvatar()
    {
        /** @var Restaurants $restaurant */

        $request = \Yii::$app->request;

        if($request->isPost) {
            $restaurant = Restaurants::findModel($this->getAuthId());
            $file       = UploadedFile::getInstanceByName('rest_avatar');
            $upload     = new Upload();

            if($file) {
                if($restaurant->restaurant_avatar) {
                    Upload::clearThumbnails('restaurant', $restaurant->restaurant_avatar, ['avatars']);
                }

                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->setFileCompress(true)
                    ->addThumbnails([['path' => '/upload/restaurant/avatars']])
                    ->setQuality(60)
                    ->setMinCompressSize(0)
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $restaurant->restaurant_avatar = $upload->getFileName();
                    $restaurant->save();

                    \Yii::$app->response->format = Response::FORMAT_JSON;

                    return ['filePath' => '/upload/restaurant/avatars/' . $upload->getFileName()];
                }
            }
        }
    }

    /**
     * Upload images to restaurant gallery
     * @return array
     */
    public function actionUploadGallery()
    {
        /** @var RestaurantGallery $galleryModel */

        $request = \Yii::$app->request;

        if($request->isPost) {
            $post         = $request->post();
            $galleryModel = new RestaurantGallery();
            $file         = UploadedFile::getInstanceByName('gallery_img');
            $upload       = new Upload();

            if($file) {
                $upload->setFile($file)
                    ->setFileExtensions(['jpg', 'png', 'jpeg'])
                    ->setMaxSize(6)
                    ->setFileRename(true)
                    ->addThumbnails([
                        [
                            'path'   => '/upload/restaurant/gallery/preview',
                            'width'  => 190,
                            'height' => 97,
                            'type'   => 'crop'
                        ],
                        [
                            'path'   => '/upload/restaurant/gallery/big',
                            'width'  => 800,
                            'height' => 600,
                            'type'   => 'adaptive'
                        ]
                    ])
                    ->upload();

                if (!$upload->isErrorExists()) {
                    $galleryModel->gallery_image = $upload->getFileName();
                    $galleryModel->restaurant_id = $post['restId'];
                    $id = $galleryModel->save();

                    \Yii::$app->response->format = Response::FORMAT_JSON;

                    return [
                        'filePath' => '/upload/restaurant/gallery/preview/' . $upload->getFileName(),
                        'fileId'   => $id
                    ];
                }
            }
        }
    }

    /**
     * Remove image gallery
     * @return array
     */
    public function actionRemoveImage()
    {
        $request = \Yii::$app->request;

        $image = RestaurantGallery::findModel($request->get('id'));

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if($image->delete()) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }

    /**
     * Remove image gallery all
     * @return array
     */
    public function actionRemoveImageAll()
    {
        $request = \Yii::$app->request;

        $deleted = RestaurantGallery::deleteAll([
            'restaurant_id' => $request->get('restId')
        ]);

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if($deleted) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }

    /**
     * Save order
     */
    public function actionOrder() {
        $request = \Yii::$app->request;

        if($request->isPost) {
            $orderModel = new OrderForm();
            $post       = $request->post();

            if($orderModel->load($post)) {
                $orderModel->saveOrder();

                return $this->redirect(\Yii::$app->request->referrer);
            }
        }
    }

    /**
     * Get order popup
     * @return string
     */
    public function actionRestOrder() {
        if(\Yii::$app->request->isAjax && \Yii::$app->request->isPost) {
            $restId     = \Yii::$app->request->post('restId');
            $params     = json_decode(\Yii::$app->request->post('params'), true);
            $restaurant = Restaurants::getRestaurant($restId);
            $orderModel = new OrderForm();

            if($this->user) {
                /** @var UserProfile $profile */
                $profile = isset($this->user->userProfile) ? $this->user->userProfile : null;

                $orderModel->user_name    = $profile ? $profile->profile_name : null;
                $orderModel->user_surname = $profile ? $profile->profile_surname : null;
                $orderModel->user_email   = $this->user->email;
                $orderModel->order_phone  = $profile ? $profile->profile_phone : null;
                $orderModel->user_id      = $this->user->id;
            }

            \Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'data' => $this->renderAjax('_order-form', [
                    'orderModel' => $orderModel,
                    'restaurant' => $restaurant,
                ]),
                'params' => $params
            ];
        }
    }

    /**
     * Toggle favorites
     * @return string
     */
    public function actionToggleFavorites() {
        if(\Yii::$app->request->isAjax) {
            if($this->user) {
                $restId         = \Yii::$app->request->post('restId');
                $status         = \Yii::$app->request->post('status');
                $favoritesModel = new Favorites();

                if(($favorites = Favorites::find()->where(['user_id' => $this->getAuthId(), 'restaurant_id' => $restId])->one()) && !$status) {
                    $favorites->delete();
                } elseif(!$favorites && $status) {
                    $favoritesModel->user_id       = $this->getAuthId();
                    $favoritesModel->restaurant_id = $restId;
                    $favoritesModel->save();
                }
            }
        }
    }
}