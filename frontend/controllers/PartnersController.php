<?php

namespace frontend\controllers;

use common\models\Partners;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use common\models\User;
use Yii;

/**
 * Class PartnersController
 * @author Nikolay Tkachenko <niktkachenko0406@gmail.com>
 * @package frontend\controllers
 */
class PartnersController extends BaseController {

    public function actionIndex()
    {
        if(!Yii::$app->user->isGuest && Yii::$app->user->identity->user_type != User::USER_TYPE_USER) {
            $dataProvider = new ActiveDataProvider([
                'query' => Partners::getModels(),
                'pagination' => [
                    'pageSize' => \Yii::$app->params['perPage'],
                ],
            ]);

            return $this->render('index', [
                'provider' => $dataProvider
            ]);
        } else{
            return $this->goHome();
        }
    }

    public function actionView($uri)
    {
        if(!Yii::$app->user->isGuest && Yii::$app->user->identity->user_type != User::USER_TYPE_USER) {
            $partner      = Partners::findModelByUri($uri);
            $lastPartners = Partners::getLastPosts();

            return $this->render('view', [
                'partner'      => $partner,
                'lastPartners' => $lastPartners,
                'user'         => $this->userProfile,
            ]);
        } else{
            return $this->goHome();
        }
    }
}