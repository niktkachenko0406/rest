<?php

namespace frontend\controllers;

use yii\web\Controller;

class BaseController extends Controller {

    public $user;
    public $userProfile;

    public function init()
    {
        $this->user        = \Yii::$app->user->identity;
        $this->userProfile = $this->user->userProfile;
    }

    /**
     * Return id authorisation user
     * @return mixed
     */
    public function getAuthId()
    {
        $identity = \Yii::$app->user->identity;
        return $identity ? $identity->getId() : null;
    }

    /**
     * Return user type
     * @return mixed
     */
    public function getAuthType()
    {
        return \Yii::$app->user->identity->getType();
    }
}