<?php
/**
 * @var UserProfile $profile
 */
use common\models\UserProfile;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<div id="profile-settings" class="tab-pane fade in active">
    <div class="row">
        <?php $form = ActiveForm::begin([
            'id'      => 'profileForm',
            'action'  => Url::to(['/user/index']),
            'method'  => 'post',
            'options' => ['enctype'=>'multipart/form-data']
        ]); ?>
        <div class="col-sm-4 col-md-4 col-lg-2 col-sm-push-8 col-md-push-6 col-lg-push-4 ">
            <div class="form-upload">
                <div class="upload-thumb upload-thumb-avatar">
                    <img id="js-profile-avatar" src="<?=$profile->getAvatar() ?: null ?>" alt="">
                </div>
                <div class="upload-btn animate">
                    Загрузить

                    <input id="js-avatar" type="file" name="UserProfile[profile_avatar]">
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-6 col-lg-4 col-sm-pull-4 col-md-pull-4 col-lg-pull-2 ">
            <?= $form->field($profile, 'profile_name')->textInput(); ?>
            <?= $form->field($profile, 'profile_surname')->textInput(); ?>
            <?= $form->field($profile, 'profile_phone')->textInput(); ?>

            <?= $form->field($passwordChange, 'currentPassword', ['enableAjaxValidation' => true])->passwordInput(['maxlength' => true]) ?>
            <?= $form->field($passwordChange, 'newPassword', ['enableAjaxValidation' => true])->passwordInput(['maxlength' => true]) ?>
            <?= $form->field($passwordChange, 'newPasswordRepeat', ['enableAjaxValidation' => true])->passwordInput(['maxlength' => true]) ?>
            <button type="submit" class="animate btn btn-light btn-lg submit-btn">Сохранить</button>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
