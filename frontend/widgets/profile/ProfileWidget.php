<?php
namespace frontend\widgets\profile;

use yii\base\Widget;

/**
 * Class ProfileWidget
 * @package frontend\widgets\profile
 */
class ProfileWidget extends Widget {

    public $profile;
    public $passwordChange;

    /**
     * init
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('profile-edit', [
            'profile'        => $this->profile,
            'passwordChange' => $this->passwordChange
        ]);
    }
}