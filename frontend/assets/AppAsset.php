<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';

    public $css = [
        'libs/bootstrap/css/bootstrap.min.css',
        'libs/choosen/chosen.min.css',
        'libs/jquery-ui/jquery-ui.min.css',
        'libs/timepicker-addon/jquery-ui-timepicker-addon.css',
        'libs/lightbox/css/lightbox.min.css',
        'css/main.css',
        'css/media.css',
    ];

    public $js = [
        'libs/jquery-ui/jquery-ui.min.js',
        'libs/timepicker-addon/jquery-ui-timepicker-addon.min.js',
        'libs/bootstrap/js/bootstrap.min.js',
        'libs/choosen/chosen.jquery.min.js',
        'libs/isotope/isotope.pkgd.min.js',
        'libs/lightbox/js/lightbox.min.js',
        'libs/plupload/plupload.full.min.js',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyD5j_9tf2KXN5OLi3t59pClmkeQAHPfWo0',
        'js/common.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
